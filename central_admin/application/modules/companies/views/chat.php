<script>
  $(document).ready(function() {
    $('.datatable-1').dataTable();
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
  } );
</script>

<div class="span9">
  <div class="content">





    <div class="module">
      <div class="module-head">
        <h3>chats del chofer</h3>
      </div>
      <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Celular usuario</th>
              <th>IMEI</th>
              <th>Mensaje</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach( $rows as $row):?>
              <?php $user = $this->Company->get_id_row('usuarios','imei',$row->uuid);?>
            <tr class="odd gradeX">
              <td><?php echo $row->fecha_creacion;?></td>
              <td><?php echo $user->celular;?></td>
              <td><?php echo $user->imei;?></td>
              <td><?php echo $row->mensaje;?></td>
              <td><a href="<?php echo base_url()?>index.php/companies/ver_chat/<?php echo $row->id_chofer.'/'.$row->uuid;?>" class="btn">ver chat</a></td>

            </tr>
          <?php endforeach;?>

          </tbody>
          <tfoot>
            <tr>
              <th>Fecha</th>
              <th>Celular usuario</th>
              <th>IMEI</th>
              <th>Mensaje</th>
              <th>Opciones</th>

            </tr>
          </tfoot>
        </table>
      </div>
    </div><!--/.module-->




  </div><!--/.content-->
</div><!--/.span9-->
