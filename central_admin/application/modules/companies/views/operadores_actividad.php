<script>
  $(document).ready(function() {
    $('.datatable-1').dataTable();
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
  } );
</script>

<div class="span9">
  <div class="content">


    <div class="module">
      <div class="module-head">
        <h3>Operadores</h3>
      </div>
      <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
          <thead>
            <tr>
              <th>Foto</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Fecha actualización</th>
              <th>Estatus</th>
              <th>Placas</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach( $rows as $row):?>
            <tr class="odd gradeX">
              <td>
                <img src="<?php echo base_url().'/'.$this->Company->get_foto($row->id_operador);?>" width="100px"/>
                </td>
              <td><?php echo $this->Company->get_name_operador($row->id_operador);?></td>
              <td><?php echo $this->Company->get_telefono($row->id_operador);?></td>
              <td><?php echo $row->fecha_creacion?></td>

              <td><!-- 1.-activo,0.-inactivo, 2.-historial, 3.-en servicio 4.-sesion cerrada-->
                <?php //echo $row->status;
                switch ($row->status) {
                      case 1:
                          echo '<div style="color:#27ae60;weight: bold;">Activo</div>';
                          break;
                      case 2:
                          echo '<div style="color:#c0392b;weight: bold;">Inactivo</div>';
                          break;
                      case 3:
                          echo '<div style="color:#8e44ad;weight: bold;">En servicio</div>';
                          break;
                      case 4:
                          echo '<div style="color:#f39c12;weight: bold;">Sesión cerrada</div>';
                          break;

                  }
                 ?>
              </td>
              <td class="center"><?php echo $this->Company->get_placas($row->id_operador);?> </td>
              <td class="center">
                  <a href="<?php echo base_url()?>index.php/companies/cerrar_chofer/<?php echo $row->id_operador;?>">Cerrar sesión</a>
              </td>
            </tr>
          <?php endforeach;?>

          </tbody>
          <tfoot>
            <tr>
              <th>Foto</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Fecha actualización</th>
              <th>Estatus</th>
              <th>Placas</th>
              <th>Opciones</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div><!--/.module-->




  </div><!--/.content-->
</div><!--/.span9-->
