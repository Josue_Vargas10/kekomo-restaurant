<script>
  $(document).ready(function() {
    $('.datatable-1').dataTable();
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
  } );
</script>

<div class="span9">
  <div class="content">

    <div class="module">
      <div class="module-head">
        <h3>Registro de usuarios</h3>
      </div>

    </div>



    <div class="module">
      <div class="module-head">
        <h3>Usuarios</h3>
      </div>
      <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
          <thead>
            <tr>
              <th>IMEI</th>
              <th>Celular</th>
              <th>Codigo registro</th>
              <th>Fecha creación</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach( $rows as $row):?>
            <tr class="odd gradeX">
              <td><?php echo $row->imei;?></td>
              <td><?php echo $row->celular;?></td>
              <td><?php echo $row->codigo?></td>
              <td class="center">
                <?php echo $row->fecha_creacion;?>
              </td>
              <td class="center">
                <!--a href="<?php echo base_url()?>index.php/companies/editar_operador/<?php echo $row->id;?>" class="btn">Editar</a>
                <a href="<?php echo base_url()?>index.php/companies/operador_auto/<?php echo $row->id;?>" class="btn">Asignar auto</a-->
              </td>
            </tr>
          <?php endforeach;?>

          </tbody>
          <tfoot>
            <tr>
              <th>IMEI</th>
              <th>Celular</th>
              <th>Codigo registro</th>
              <th>Fecha creación</th>
              <th>Opciones</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div><!--/.module-->




  </div><!--/.content-->
</div><!--/.span9-->
