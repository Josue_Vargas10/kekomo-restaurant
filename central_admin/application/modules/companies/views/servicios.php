<script>
  $(document).ready(function() {
    //$('.datatable-1').dataTable();


    /*$('.datatable-1').dataTable( {
    "order": []
      } );
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
  **/} );
</script>

<div class="span9">
  <div class="content">





    <div class="module">
      <div class="module-head">
        <h3>Servicios</h3>
      </div>
      <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0" class="table 	" width="100%">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Estatus</th>
              <th>precio</th>
              <th>Chofer</th>
              <th>Usuario</th>
              <th>Celular</th>

            </tr>
          </thead>
          <tbody>

            <?php foreach( $rows as $row):?>
            <tr class="odd gradeX">
              <td><?php echo $row->fecha_creacion;?></td>
              <td><?php
              switch ($row->status) {
                //1.-cotizacion,2.-activo,3.-proceso,4.-finalizado,5.-pagado,6.-cancelado,7.-asignado
                  case 1:
                      echo "Servicio cotizado";
                      break;
                  case 2:
                      echo "Servicio activo";
                      break;
                  case 3:
                      echo "Servicio en proceso";
                      break;
                  case 4:
                      echo "Servicio finalizado";
                      break;
                  case 5:
                      echo "Servicio pagado";
                      break;
                  case 6:
                      echo "Servicio cancelado";
                      break;
                  case 7:
                      echo "Servicio asignado al chofer";
                      break;

              }
              ?></td>
              <td>$<?php echo $row->precio?></td>
              <td class="center"><?php echo $this->Company->get_name_operador($row->id_operador);?> </td>
              <td><?php echo $this->Company->get_name_usuario_cel($row->celular);?>
                </td>
              <td><?php echo $row->celular?></td>

            </tr>
          <?php endforeach;?>

          </tbody>
          <tfoot>
            <tr>
              <th>Fecha</th>
              <th>Estatus</th>
              <th>precio</th>
              <th>Chofer</th>
              <th>Usuario</th>
              <th>Celular</th>


            </tr>
          </tfoot>
        </table>
      </div>
    </div><!--/.module-->




  </div><!--/.content-->
</div><!--/.span9-->
