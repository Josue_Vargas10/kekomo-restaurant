<script>
  $(document).ready(function() {
    $('.datatable-1').dataTable();
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
  } );
</script>

<div class="span9">
  <div class="content">

    <div class="module">
      <div class="module-head">
        <h3>Alta auto</h3>
      </div>
      <div class="module-body">



          <br />

          <form class="form-horizontal row-fluid" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>index.php/companies/save_auto">
            <div class="control-group">
              <label class="control-label" for="basicinput">Placas</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosPlacas]" placeholder="Placas" class="span8" required="">
                <!--span class="help-inline">Minimum 5 Characters</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Nick</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosNick]" placeholder="Nick" class="span8" required="">
                <!--span class="help-inline">Minimum 5 Characters</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Color</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosColor]" placeholder="color" class="span8" required="">
                <!--span class="help-inline">Minimum 5 Characters</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Descripción</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosDescripcion]" placeholder="Descripción" class="span8" required="">

              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Sitio</label>
              <div class="controls">
                  <input type="text" id="basicinput" name="save[autosSitio]" placeholder="contraseña" class="span8" required="">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Tipo</label>
              <div class="controls">
                <select class="" name="save[autosTipo]">
                  <option value="1">Auto</option>
                  <option value="2">Camioneta</option>
                </select>

              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Foto</label>
              <div class="controls">
                <input type="file" id="basicinput" name="image" placeholder="contraseña" class="span8" required="">
                <span class="help-inline">500px X 500px</span>
              </div>
            </div>


            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Guardar vehículo</button>
              </div>
            </div>
          </form>
      </div>
    </div>



    <div class="module">
      <div class="module-head">
        <h3>Autos</h3>
      </div>
      <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
          <thead>
            <tr>
              <th>Imagen</th>
              <th>Nick</th>
              <th>Placas</th>
              <th>Modelo</th>
              <th>Marca</th>
              <th>Color</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach( $rows as $row):?>
            <tr class="odd gradeX">
              <td>
                <img src="<?php echo base_url().'/'.$row->autosImagen;?>" width="100px"/>
                </td>
                <td><?php echo $row->autosNick;?></td>
              <td><?php echo $row->autosPlacas;?></td>
              <td><?php echo $row->autosDescripcion;?></td>
              <td><?php echo $row->autosSitio;?></td>
              <td class="center"> <?php echo $row->autosColor;?></td>
              <td class="center">
               <a href="<?php echo base_url()?>index.php/companies/editar_vehiculo/<?php echo $row->autosId;?>" class="btn">Editar</a>

              </td>
            </tr>
          <?php endforeach;?>
          </tbody>
          <tfoot>
            <tr>
              <th>Imagen</th>
              <th>Nick</th>
              <th>Placas</th>
              <th>Modelo</th>
              <th>Marca</th>
              <th>Color</th>
              <th>Opciones</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div><!--/.module-->




  </div><!--/.content-->
</div><!--/.span9-->
