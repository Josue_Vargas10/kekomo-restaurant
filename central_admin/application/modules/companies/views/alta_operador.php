<script>
  $(document).ready(function() {
    $('.datatable-1').dataTable();
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
  } );
</script>

<div class="span9">
  <div class="content">

    <div class="module">
      <div class="module-head">
        <h3>Alta operador</h3>
      </div>
      <div class="module-body">



          <br />

          <form class="form-horizontal row-fluid" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>index.php/companies/save_operador">
            <div class="control-group">
              <label class="control-label" for="basicinput">Nombre completo</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[operadorNombreCompleto]" placeholder="Nombre completo" class="span8" required="">
                <!--span class="help-inline">Minimum 5 Characters</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Teléfono</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[operadorTelefono]" placeholder="Teléfono" class="span8" required="">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Usuario</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[operadorUsuario]" placeholder="Usuario" class="span8" required="">
                <span class="help-inline">sin espacios</span>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">contraseña</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[operdorPassword]" placeholder="contraseña" class="span8" required="">
                <span class="help-inline">sin espacios</span>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Foto</label>
              <div class="controls">
                <input type="file" id="basicinput" name="image"  class="span8" required="">
                <span class="help-inline">500px X 500px</span>
              </div>
            </div>


            <div class="control-group">
              <label class="control-label" for="basicinput">Foto 2</label>
              <div class="controls">
                <input type="file" id="basicinput" name="image2"  class="span8" required="">
                <span class="help-inline">500px X 500px</span>
              </div>
            </div>


            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Guardar operador</button>
              </div>
            </div>
          </form>
      </div>
    </div>



    <div class="module">
      <div class="module-head">
        <h3>Operadores</h3>
      </div>
      <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
          <thead>
            <tr>
              <th>Foto</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Usuario</th>
              <th>Contraseña</th>
              <th>Placas</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach( $rows as $row):?>
            <tr class="odd gradeX">
              <td>
                <img src="<?php echo base_url().'/'.$row->OperadorImagen;?>" width="100px"/>
                </td>
              <td><?php echo $row->operadorNombreCompleto;?></td>
              <td><?php echo $row->operadorTelefono;?></td>
              <td><?php echo $row->operadorUsuario?></td>
              <td><?php echo $row->operdorPassword?></td>
              <td class="center"><?php echo $this->Company->get_placas($row->operadorId);?> </td>
              <td class="center">
                <a href="#" class="btn">Servicios</a>
                <a href="<?php echo base_url()?>index.php/companies/editar_operador/<?php echo $row->operadorId;?>" class="btn">Editar</a>
                <a href="<?php echo base_url()?>index.php/companies/operador_auto/<?php echo $row->operadorId;?>" class="btn">Asignar auto</a>
                <a href="<?php echo base_url()?>index.php/companies/eliminar_operador/<?php echo $row->operadorId;?>" class="btn btn-danger">Eliminar</a>
              </td>
            </tr>
          <?php endforeach;?>

          </tbody>
          <tfoot>
            <tr>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Usuario</th>
              <th>Placas</th>
              <th>Opciones</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div><!--/.module-->




  </div><!--/.content-->
</div><!--/.span9-->
