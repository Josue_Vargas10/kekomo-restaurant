<script>
  $(document).ready(function() {
    $('.datatable-1').dataTable();
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
  } );
</script>

<div class="span9">
  <div class="content">

    <div class="module">
      <div class="module-head">
        <h3>Editar auto</h3>
      </div>
      <div class="module-body">



          <br />

          <form class="form-horizontal row-fluid" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>index.php/companies/actualiza_vehiculo/<?php echo $row->autosId;?>">
            <div class="control-group">
              <label class="control-label" for="basicinput">Placas</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosPlacas]" value="<?php echo $row->autosPlacas;?>" placeholder="Placas" class="span8" required="">
                <!--span class="help-inline">Minimum 5 Characters</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Nick</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosNick]" value="<?php echo $row->autosNick;?>" placeholder="Nick" class="span8" required="">
                <!--span class="help-inline">Minimum 5 Characters</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Color</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosColor]" value="<?php echo $row->autosColor;?>" placeholder="color" class="span8" required="">
                <!--span class="help-inline">Minimum 5 Characters</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Descripción</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosDescripcion]" value="<?php echo $row->autosDescripcion;?>" placeholder="Marca" class="span8" required="">
                <!--span class="help-inline">sin espacios</span-->
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="basicinput">Sitio</label>
              <div class="controls">
                <input type="text" id="basicinput" name="save[autosSitio]" value="<?php echo $row->autosSitio;?>" placeholder="contraseña" class="span8" required="">
                <!--span class="help-inline">sin espacios</span-->
              </div>
            </div>


            <div class="control-group">
              <label class="control-label" for="basicinput">Tipo</label>
              <div class="controls">
                <select class="" name="save[autosTipo]">
                  <?php if($row->autosSitio == 1){?>
                    <option value="1" selected>Auto</option>
                  <?php }else{?>
                    <option value="1">Auto</option>
                  <?php }?>
                  <?php if($row->autosSitio == 1){?>
                    <option value="2" selected>Camioneta</option>
                  <?php }else{?>
                    <option value="2">Camioneta</option>
                  <?php }?>
                </select>

              </div>
            </div>

            <div class="control-group">

                <img src="<?php echo base_url().'/'.$row->autosImagen;?>" width="100px"/>
              <label class="control-label" for="basicinput">Foto</label>
              <div class="controls">
                <input type="file" id="basicinput" name="image" placeholder="contraseña" class="span8" >
                <span class="help-inline">500px X 500px</span>
              </div>
            </div>


            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Edita vehículo</button>
              </div>
            </div>
          </form>
      </div>
    </div>








  </div><!--/.content-->
</div><!--/.span9-->
