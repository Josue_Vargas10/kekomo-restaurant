<?php
/**

 **/
class Mapi extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function get_usuarios(){
      $this->db->select('usuarios.usuarioNombre, usuarios.usuarioId,
                          usuarios.usuarioCelular,servicios.servcioFechaCreacion,
                          servicios.servicioLatuitudInicio,servicios.servicioLongitudInicio,
                          servicios.servicioLatitudFin,servicios.servicioLongitudFin,servicios.servicioComentario');
      $this->db->from('servicios');
      $this->db->join('usuarios', 'usuarios.usuarioId = servicios.	servicioIdUsuario');
      $this->db->where('servicios.status',1);
      return $this->db->get()->result();
    }

    public function validar_telefono_imei($telefono,$imei){
      $this->db->where('celular',$telefono);
      //$this->db->where('imei',$imei);
      $query = $this->db->get('usuarios');
      if($query->num_rows()>0){
        return 1;
      }else{
        return 0;
      }
    }

	public function save_register($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function actualizar_tabla($tabla,$id_tabla,$data, $id){
        $this->db->update($tabla, $data, array($id_tabla=>$id));
    }

    public function select_row($table,$id_table, $id){
      $this->db->where($id_table,$id);
      return $this->db->get($table)->row();

    }

    public function login($email,$password){
      $this->db->where('email', $email);
      $this->db->where('password',$password);
      $query = $this->db->get('registro')->row();
      return $query;

    }

    public function mapa($id_usuario, $latitud, $longitud){
      $this->db->select('auto_operador.latitud, auto_operador.longitud, operador.nombre, operador.foto, operador.telefono, auto.placas, auto.foto, auto.color, auto.marca, auto.modelo');
      $this->db->from('auto_operador');
      $this->db->join('operador', 'operador.id = auto_operador.id_operador');
      $this->db->join('auto', 'auto.id = auto_operador.id_auto');
      return $this->db->get()->result();
    }

    /*==============================OPERADOR ================================*/
    public function login_operador($usuario,$password,$placas){
      $this->db->select('operador.id, auto.placas,operador.nombre,auto_operador.id as id_auto_operador');
      $this->db->from('operador');
      $this->db->join('auto_operador','auto_operador.id_operador = operador.id');
      $this->db->join('auto', 'auto.id = auto_operador.id_auto');
      $this->db->where('operador.usuario',$usuario);
      $this->db->where('operador.password',$password);
      $this->db->where('auto.placas',$placas);
      return $this->db->get()->row();

    }

    public function asignar_auto($id_servicio){
      $this->db->where('status',1);
      $query = $this->db->get('auto_operador')->row();
      $res = 0;
      if(is_object($query)){

        $data_auto_operador['status'] =3;
        $this->Mapi->actualizar_tabla('auto_operador','id',$data_auto_operador, $query->id);

        $data_servicios['status'] = 7;
        $data_servicios['id_operador']  = $query->id_operador;
        $this->Mapi->actualizar_tabla('servicios','id',$data_servicios, $id_servicio);

        $res = 1;
      }else{
        $res = 0;
      }

      return $res;
    }


    public function lista_chat_chofer($id_chofer){
      $this->db->where('id_chofer',$id_chofer);
      $this->db->group_by("uuid");
      $query = $this->db->get('chat');
      return $query->result();

    }

    /*====================================MAPA TEL=============================*/



    public function mapa_tel(){
      $this->db->select('auto_operador.latitud, auto_operador.longitud, operador.id,operador.nombre, operador.foto, operador.telefono, auto.placas, auto.foto as imagen, auto.color, auto.marca, auto.modelo');
      $this->db->from('auto_operador');
      $this->db->join('operador', 'operador.id = auto_operador.id_operador');
      $this->db->join('auto', 'auto.id = auto_operador.id_auto');
      $this->db->where('auto_operador.status',1);
      return $this->db->get()->result();
    }


    /*======================================CHAT=============================*/
    public function get_chat($id_chofer,$id_usuario){
      $this->db->where('id_chofer',$id_chofer);
      $this->db->where('uuid',$id_usuario);
      $query = $this->db->get('chat');
      return $query->result();
    }

	}
