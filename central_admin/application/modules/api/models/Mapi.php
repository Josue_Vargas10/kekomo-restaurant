<?php
/**

 **/
class Mapi extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function ver_pago($id_operador){
      $this->db->where('pagoIdOperador', $id_operador);
      $this->db->where('pagoEstatus',1);
      $query = $this->db->get('pagos')->row();
      return $query;

    }

    public function ver_calificacion_servicio($id_operador,$id_servicio){
      $this->db->where('calificacionIdOperador', $id_operador);
      $this->db->where('calificacionIdServicio',$id_servicio);
      $query = $this->db->get('calificacion_usuario')->row();
      return $query;

    }

    public function calificacion_texto(){

      $query = $this->db->get('calificacion_texto');
      return $query->result();
    }

    public function get_usuarios(){
      $this->db->select('usuarios.usuarioNombre, usuarios.usuarioId,
                          usuarios.usuarioCelular,servicios.servicioFechaCreacion,
                          servicios.servicioLatuitudInicio,servicios.servicioLongitudInicio,
                          servicios.servicioLatitudFin,servicios.servicioLongitudFin,servicios.servicioComentario,servicios.servicioId,
                          servicios.nombreSolicitante,servicios.celularSolicitante, servicios.observacionesSolicitante, servicios.domicilioSolicitante, servicios.base');
      $this->db->from('servicios');
      $this->db->join('usuarios', 'usuarios.usuarioId = servicios.servicioIdUsuario');
      $this->db->where('servicios.servicioStatus',1);
      $query_serv =  $this->db->get()->result();

      $response=array();
      foreach( $query_serv as $row){



			$res=array();
      if($row->base == 1){
        $res['usuarioNombre']=$row->nombreSolicitante;
      }else{
        $res['usuarioNombre']=$row->usuarioNombre;
      }

			$res['usuarioId']=$row->usuarioId;
			$res['usuarioCelular']=$row->usuarioCelular;
			$res['servicioFechaCreacion']=$row->servicioFechaCreacion;
      if($row->base == 1){
        	$res['servicioComentario']="";
      }else{
        $res['servicioComentario']=$row->servicioComentario;
      }

			$res['servicioLatitudFin']=$row->servicioLatitudFin;
			$res['servicioLongitudFin']=$row->servicioLongitudFin;
      $res['servicioLatuitudInicio']=$row->servicioLatuitudInicio;
      $res['servicioLongitudInicio']=$row->servicioLongitudInicio;
      $res['servicioId']=$row->servicioId;
      $res['nombreSolicitante']=$row->nombreSolicitante;
      $res['celularSolicitante']=$row->celularSolicitante;
      $res['observacionesSolicitante']=$row->observacionesSolicitante;
      $res['domicilioSolicitante']=$row->domicilioSolicitante;
      $res['base']=$row->base;
			array_push($response, $res);
		}
    return $response;
    }

    public function get_usuarios_prueba(){
      $this->db->select('usuarios.usuarioNombre, usuarios.usuarioId,
                          usuarios.usuarioCelular,servicios.servicioFechaCreacion,
                          servicios.servicioLatuitudInicio,servicios.servicioLongitudInicio,
                          servicios.servicioLatitudFin,servicios.servicioLongitudFin,servicios.servicioComentario,servicios.servicioId,
                          servicios.nombreSolicitante,servicios.celularSolicitante, servicios.observacionesSolicitante, servicios.domicilioSolicitante, servicios.base');
      $this->db->from('servicios');
      $this->db->join('usuarios', 'usuarios.usuarioId = servicios.servicioIdUsuario');
      $this->db->where('servicios.servicioIdUsuario',10);
      return $this->db->get()->result();
    }

    public function inserta_operador_servicio($id_servicio, $id_operador,$status){
      $this->db->where('idServicio',$id_servicio);
      $this->db->where('idOperador',$id_operador);
      $query =  $this->db->get('operador_servicios')->row();
      if(is_object($query)){
        $data['estatus'] = $status;
        $this->actualizar_tabla('operador_servicios','id',$data, $query->id);

      }else{
        $data['idServicio'] = $id_servicio;
        $data['idOperador'] = $id_operador;
        $data['estatus'] = $status;
        $this->save_register('operador_servicios', $data);

      }
      return 1;

    }

    public function get_datos_servicio($d_servicio){
      $this->db->select('servicios.servicioFechaCreacion,servicios.servicioComentario,
                          servicios.servicioLatuitudInicio,servicios.servicioLongitudInicio,
                          usuarios.usuarioId,usuarios.usuarioNombre,usuarios.usuarioCelular');
      $this->db->from('servicios');
      $this->db->join('usuarios', 'usuarios.usuarioId = servicios.servicioIdUsuario');
      $this->db->where('servicios.servicioId',$d_servicio);
      return $this->db->get()->row();
    }

    public function get_llamadas_operador($id_operador){
      $this->db->select('llamadas_operador.fecha_creacion,
                          usuarios.usuarioNombre,usuarios.usuarioCelular');
      $this->db->from('llamadas_operador');
      $this->db->join('usuarios', 'usuarios.usuarioId = llamadas_operador.idUsuario');
      $this->db->where('llamadas_operador.idOperador',$id_operador);
      return $this->db->limit(5)->get()->result();
    }

    public function mis_servicio_operador($id_operador){

      $this->db->select('usuarios.usuarioNombre,usuarios.usuarioId,usuarios.usuarioCelular,
      servicios.servicioFechaCreacion,servicios.servicioComentario,servicios.servicioLatitudFin,
      servicios.servicioLongitudFin,servicios.servicioLatuitudInicio,servicios.servicioLongitudInicio,
      servicios.servicioId,servicios.nombreSolicitante,servicios.celularSolicitante,servicios.observacionesSolicitante,
      servicios.domicilioSolicitante,servicios.base');
      $this->db->from('servicios');
      $this->db->order_by("servicios.servicioId", "DESC");
      $this->db->join('usuarios', 'usuarios.usuarioId = servicios.servicioIdUsuario');
      $this->db->where('servicios.servicioIdoperadorAuto',$id_operador);
      $query_serv =  $this->db->limit(5)->get()->result();

      $response=array();
      foreach( $query_serv as $row){



			$res=array();
      if($row->base == 1){
        $res['usuarioNombre']=$row->nombreSolicitante;
      }else{
        $res['usuarioNombre']=$row->usuarioNombre;
      }

			$res['usuarioId']=$row->usuarioId;
			$res['usuarioCelular']=$row->usuarioCelular;
			$res['servicioFechaCreacion']=$row->servicioFechaCreacion;
      if($row->base == 1){
        	$res['servicioComentario']="";
      }else{
        $res['servicioComentario']=$row->servicioComentario;
      }

			$res['servicioLatitudFin']=$row->servicioLatitudFin;
			$res['servicioLongitudFin']=$row->servicioLongitudFin;
      $res['servicioLatuitudInicio']=$row->servicioLatuitudInicio;
      $res['servicioLongitudInicio']=$row->servicioLongitudInicio;
      $res['servicioId']=$row->servicioId;
      $res['nombreSolicitante']=$row->nombreSolicitante;
      $res['celularSolicitante']=$row->celularSolicitante;
      $res['observacionesSolicitante']=$row->observacionesSolicitante;
      $res['domicilioSolicitante']=$row->domicilioSolicitante;
      $res['base']=$row->base;
			array_push($response, $res);
		}
    return $response;
    }

    public function validar_telefono_imei($telefono,$imei){
      $this->db->where('celular',$telefono);
      //$this->db->where('imei',$imei);
      $query = $this->db->get('usuarios');
      if($query->num_rows()>0){
        return 1;
      }else{
        return 0;
      }
    }

	public function save_register($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function actualizar_tabla($tabla,$id_tabla,$data, $id){
        $this->db->update($tabla, $data, array($id_tabla=>$id));
    }

    public function select_row($table,$id_table, $id){
      $this->db->where($id_table,$id);
      return $this->db->get($table)->row();

    }

    public function login($email,$password){
      $this->db->where('email', $email);
      $this->db->where('password',$password);
      $query = $this->db->get('registro')->row();
      return $query;

    }

    public function mapa($id_usuario, $latitud, $longitud){
      $this->db->select('auto_operador.latitud, auto_operador.longitud, operador.nombre, operador.foto, operador.telefono, auto.placas, auto.foto, auto.color, auto.marca, auto.modelo');
      $this->db->from('auto_operador');
      $this->db->join('operador', 'operador.id = auto_operador.id_operador');
      $this->db->join('auto', 'auto.id = auto_operador.id_auto');
      return $this->db->get()->result();
    }

    /*==============================OPERADOR ================================*/
    public function login_operador($usuario,$password,$placas){
      $this->db->select('operador.operadorId, autos.autosPlacas,operador.operadorNombreCompleto,operador_autos.OPId as id_auto_operador, autos.autosId');
      $this->db->from('operador');
      $this->db->join('operador_autos','operador_autos.OPIdOperador = operador.operadorId');
      $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto');
      $this->db->where('operador.operadorUsuario',$usuario);
      $this->db->where('operador.operdorPassword',$password);
      $this->db->where('autos.autosPlacas',$placas);
      return $this->db->get()->row();

    }

    public function selecionar_placa_operador($id_auto,$id_operador){
      $this->db->where('OPIdOperador',$id_operador);
      $this->db->where('OPIdAuto',$id_auto);
      return $this->db->get('operador_autos')->row();
    }

    public function asignar_auto($id_servicio){
      $this->db->where('status',1);
      $query = $this->db->get('auto_operador')->row();
      $res = 0;
      if(is_object($query)){

        $data_auto_operador['status'] =3;
        $this->Mapi->actualizar_tabla('auto_operador','id',$data_auto_operador, $query->id);

        $data_servicios['status'] = 7;
        $data_servicios['id_operador']  = $query->id_operador;
        $this->Mapi->actualizar_tabla('servicios','id',$data_servicios, $id_servicio);

        $res = 1;
      }else{
        $res = 0;
      }

      return $res;
    }


    public function lista_chat_chofer($id_chofer){
      $this->db->where('id_chofer',$id_chofer);
      $this->db->group_by("uuid");
      $query = $this->db->get('chat');
      return $query->result();

    }

    /*====================================MAPA TEL=============================*/







    public function mapa_tel(){
      $this->db->select('auto_operador.status,auto_operador.latitud, auto_operador.longitud, operador.id,operador.nombre, operador.foto, operador.telefono, auto.placas, auto.foto as imagen, auto.color, auto.marca, auto.modelo');
      $this->db->from('auto_operador');
      $this->db->join('operador', 'operador.id = auto_operador.id_operador');
      $this->db->join('auto', 'auto.id = auto_operador.id_auto');
      $this->db->where('auto_operador.status',1);
      return $this->db->get()->result();
    }


    /*======================================CHAT=============================*/
    public function get_chat($id_chofer,$id_usuario){
      $this->db->where('id_chofer',$id_chofer);
      $this->db->where('uuid',$id_usuario);
      $query = $this->db->get('chat');
      return $query->result();
    }

	}
