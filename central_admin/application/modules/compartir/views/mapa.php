<!DOCTYPE html>
<html>
  <head>
    <title>Geolocation</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>

    $( document ).ready(function() {
      setInterval(function(){
    		$.ajax({
    			url: 'http://uniendogeneraciones.com.mx/taxinuevo/index.php/compartir/coordenadas/<?php echo $usuario;?>',
    			type: 'POST',
    			async: true,
    			dataType: 'json',
    			timeout: 3000,
    			success: function(data){
          // alert(data.latitud);
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 15,
              center: {lat: parseFloat(data.latitud), lng: parseFloat(data.longitud)}
            });

            var image = 'http://uniendogeneraciones.com.mx/taxinuevo/taxi.png';
            var beachMarker = new google.maps.Marker({
              position: {lat: parseFloat(data.latitud), lng: parseFloat(data.longitud)},
              map: map,
              icon: image
            });

    			}
    		});
    	},15000);
 });

    function carga_mapa(){
      $.ajax({
        url: 'http://uniendogeneraciones.com.mx/taxinuevo/index.php/compartir/coordenadas/<?php echo $usuario;?>',
        type: 'GET',
        //data: { usuario: "<?php echo $usuario;?>"} ,
        contentType: 'application/json; charset=utf-8',
        dataType:'json',
        success: function (response) {
            //your success code

        },
        error: function () {
            //your error code
        }
    });
    }

    </script>
  </head>
  <body>
    <div id="map"></div>
    <script>

// This example adds a marker to indicate the position of Bondi Beach in Sydney,
// Australia.
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: {lat: 19.2484559, lng: -103.766423}
  });

  var image = 'http://uniendogeneraciones.com.mx/taxinuevo/taxi.png';
  var beachMarker = new google.maps.Marker({
    position: {lat: 19.2484559, lng: -103.766423},
    map: map,
    icon: image
  });
}

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-4KgdyE9C93Iu1mIokRpEjG9lEi2sS_M&signed_in=true&callback=initMap"
        async defer>
    </script>
  </body>
</html>
