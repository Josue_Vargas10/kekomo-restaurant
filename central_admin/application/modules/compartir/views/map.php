<!DOCTYPE html>
<html>
  <head>
    <title>Geolocation</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-4KgdyE9C93Iu1mIokRpEjG9lEi2sS_M&signed_in=true&callback=initMap"
        async defer>
    </script-->
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyB-4KgdyE9C93Iu1mIokRpEjG9lEi2sS_M"></script>

  </head>
  <body>
    <?php //echo $usuario;?>
    <div id="map"></div>
    <script>

    var myLatLng = new google.maps.LatLng( parseFloat("<?php echo $latitud;?>"), parseFloat("<?php echo $longitud;?>") ),
        myOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            },
        map = new google.maps.Map( document.getElementById( 'map' ), myOptions ),
        marker = new google.maps.Marker( {position: myLatLng, map: map} );

    marker.setMap( map );



    $( document ).ready(function() {



      setInterval(function(){
        $.ajax({
          url: 'http://transportesselectos.com/panel/index.php/compartir/coordenadas/<?php echo $usuario;?>',
          type: 'POST',
          async: true,
          dataType: 'json',
        //  timeout: 3000,
          success: function(data){
          // alert(data.latitud);

               marker.setPosition( new google.maps.LatLng( parseFloat(data.latitud), parseFloat(data.longitud) ) );
               map.panTo( new google.maps.LatLng( parseFloat(data.latitud), parseFloat(data.longitud) ) );
          }
        });
    	},8000);
 });

    function initialize() {

        var myLatLng = new google.maps.LatLng( parseFloat("<?php echo $latitud;?>"), parseFloat("<?php echo $longitud;?>") ),
            myOptions = {
                zoom: 15,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                },
            map = new google.maps.Map( document.getElementById( 'map' ), myOptions ),
            marker = new google.maps.Marker( {position: myLatLng, map: map} );

        marker.setMap( map );
        moveMarker( map, marker );

    }

    function moveMarker( map, marker ) {

        //delayed so you can see it move
        setTimeout( function(){

          $.ajax({
            url: 'http://uniendogeneraciones.com.mx/taxinuevo/index.php/compartir/coordenadas/<?php echo $usuario;?>',
            type: 'POST',
            async: true,
            dataType: 'json',
          //  timeout: 3000,
            success: function(data){
            // alert(data.latitud);

                 marker.setPosition( new google.maps.LatLng( parseFloat(data.latitud), parseFloat(data.longitud) ) );
                 map.panTo( new google.maps.LatLng( parseFloat(data.latitud), parseFloat(data.longitud) ) );
            }
          });



        }, 1500 );

    };




    </script>

  </body>
</html>
