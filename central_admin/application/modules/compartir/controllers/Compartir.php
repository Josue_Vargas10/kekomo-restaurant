<?php if(! defined('BASEPATH')) exit('No script access allowed');

class Compartir extends MX_Controller {

    /**
     * Construct where can declare all the files will
     * be used in all the class
     **/
    public function __construct(){
        parent::__construct();
        $this->load->model('General_model','',TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url'));

        date_default_timezone_set('America/Mexico_City');
    }

    public function index($id_usuario){
      $data['usuario'] = $id_usuario;
      $user = $this->General_model->get_row("usuario",$id_usuario,"coordenadas_usuario");
      $data['latitud'] = $user->latitud;
      $data['longitud'] = $user->longitud;

      $this->load->view('map',$data,FALSE);
    }

    public function coordenadas($id_usuario){
      $user = $this->General_model->get_row("usuario",$id_usuario,"coordenadas_usuario");
      $data['latitud'] = $user->latitud;
      $data['longitud'] = $user->longitud;
      echo json_encode($data);
    }
  }
