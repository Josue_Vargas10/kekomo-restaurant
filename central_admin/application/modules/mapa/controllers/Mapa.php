<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Mapa extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mmapa', '', TRUE);
        $this->load->model('Company', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'companies', 'url'));
    }

    public function login(){
      $this->load->view('mapa/login', '', FALSE);
    }

    public function entrar(){
      $usuario = $this->input->post("usuario");
      $password = $this->input->post('password');

      if(($usuario == "mapa") && ($password=="m3d5pa94")){
        $token = rand();
          //echo "mapa".$token;
          $array_session = array('id_mapa'=>$token);
          $this->session->set_userdata($array_session);
          if($this->session->userdata('id_mapa'))
          {
            redirect('mapa/mapa_activos');

          }

      }else{
        echo "contraseña o usuario incorrecto";
      }
    }


    public function mapa_activos(){
      if($this->session->userdata('id_mapa'))
      {
      $data['rows'] = $this->Mmapa->get_table('operador');
      $data['status'] = 1;
      $content = $this->load->view('mapa/mapa_open', $data, FALSE);
    }else{
      redirect('mapa/login');
    }

    }

    public function get_usuarios($status, $nick=null){
      if($this->session->userdata('id_mapa'))
           {
      $res = $this->Company->get_operadores_status($status,$nick);//$this->Company->get_operadores();
      echo json_encode($res);
    }
        else{
            redirect('companies');
        }
    }


}
