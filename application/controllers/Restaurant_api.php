<?php ob_start();if(!defined('BASEPATH')) exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;


require_once APPPATH.'libraries/restserver/RestController.php';

class Restaurant_api extends RestController{

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('servicios_model');
        $this->load->model('Principal');
        $this->load->model('Restaurantes_model');
        $this->load->helper(array('html', 'url'));
        $this->load->library('form_validation');
        date_default_timezone_set('America/Mexico_City');
    }
    /*
    * Valida el restaurante, el codigo QR y la fecha del pedido.
    * @method: GET

    * @url: api/restaurant/$1
    * @params: codigoQR
    */
/*     public function index_get($code = null, $tipo_compra)
	{		
        // $code = ['code' => $this->get( 'code' )];        

        // XSS Filtering
        // $data = $this->security->xss_clean($_GET);
        
        //definimos los campos a utilizar
        if($code == null ){
            $this->response([
                'status' => false,
                'message' => 'You must provide a code.',
            ], 400);
        }


        $campos = array('campo1' => 'cveingreso',
                        'campo2' => 'status',
                        'campo3' => 'fecha_creacion',
                        'campo4' => 'fecha_modificacion'
                    );

        //Definimos los valores de los campos                        
        $values = array('code' => $code,
                        'status' => 4,                        
                        // timeframe es el rango de tiempo que queremos usar
                        // Ejemplo 3, define un rango de 3hrs en el pasado hasta la hora actual
                        'timeframe' => 5
                         );         
                        


        // $tomorrow = date("Y-m-d H:i:s", mktime(date("H")+$values['timeframe'], date("i"), 
        //                                        date("s"), date("m"), date("d"), 
        //                                        date("Y")) );
        // $this->response($tomorrow, 200 );

        //Validamos los valores recibidos                            
        $this->form_validation->set_data(['code' => $code]);                                
        $this->form_validation->set_rules('code', 'Code', 'required', 
                                          array('required' => 'You must provide a %s.') );
                  
        
        // $this->response( $code, 200 );
        // $this->response( $campos, 200 );
        // $limit_past = date("Y-m-d H:i:s", 
        //                    mktime(date("H")-3, date("i"), date("s"), 
        //                           date("m"), date("d"),   date("Y") ) );
        // $this->response($limit_past, 200 );

        // Si no pasa la validacion                                                      
        if ($this->form_validation->run() == FALSE)
        {
            $this->response( [
                'status' => false,
                'message' => $this->form_validation->error_array()
            ], 400 );
        }
        else //si pasa la validacion
        {
            $order = $this->Mgeneral->get_orders($campos, $values, 'clientespedido');

            if($order){                                

                $loged = $this->session->has_userdata('uuid');
                $uuid = $this->session->userdata('uuid');
                
                // PARA COMPROBAR LO QUE CONTIENEN LAS VARIABLES DE ARRIBA 
                // $this->response([
                //     'status' => true,
                //     'message' => 'Resource found',
                //     'log' => $loged,
                //     'uuid' => $uuid,
                    
                // ], 200);



                // if(!$loged){
                    $uuid = $this->_uuid_key();
                    $this->session->set_userdata('uuid', $uuid);
                    $data= [
                        'uuid' => $uuid,
                        't_code' => $code,
                        'type_purchase' => $tipo_compra,
                    ];
    
                    $token = $this->_generate_token($data);

                    $id = $this->Mgeneral->save_register('guest_users', ['uuid' => $uuid, 'token' => $token]);


                    if($id){
                        $this->response([
                            'status' => true,
                            'message' => 'Resource found',
                            // 'token' => $token,
                            'id' => $uuid                           
                            
                        ], 200);
                    }
                // }

                $this->response([
                    'status' => false,
                    'message' => 'Wait a moment, you cant get another access.',
                ], 404);
                // $this->response( $order, 200 );
            }else{
                $this->response([
                    'status' => false,
                    'message' => 'Resource not found',
                ], 404);

            }           
        }
    } */

              
    public function get_orders_get(){

        // $this->response([
        //     'status' => 'Ok',
        //     'message' => $data,
        // ], 200);

        $this->response([
            'status' => 'Ok',
            'message' => $this->input->post(NULL, TRUE),
        ], 200);
    }
    
    public function add_order_post(){
        //$this->form_validation->set_data(['code' => $code]);                                
        $this->form_validation->set_rules('id_cliePed',             'Id del pedido', 'required');//el id con el que se guardó en la bd del restaurante
        $this->form_validation->set_rules('acc_control_key',        'Clave access control', 'required');
        $this->form_validation->set_rules('restaurante_cve',        'Clave del restaurante', 'required');
        $this->form_validation->set_rules('tel_restaurante',        'Telefono del restaurante', 'required');
        $this->form_validation->set_rules('domicilio_restaurante',  'Domicilio del restaurante', 'required');
        $this->form_validation->set_rules('tel_cliente',            'Telefono del cliente', 'required');
        $this->form_validation->set_rules('domicilio_cliente',      'Domicilio del cliente', 'required');

        $this->form_validation->set_message('required', 'El campo %s es requerido!');

        if ($this->form_validation->run() == FALSE)
        {
            $this->response( [
                'status' => false,
                'message' => $this->form_validation->error_array()
            ], 400 );
        }
        else //si pasa la validacion
        {
            $this->response( [
                'status' => true,
                'message' => "data received",
                'data'  => $this->input->post(NULL,TRUE)
            ], 200 );
        }
    }
}
