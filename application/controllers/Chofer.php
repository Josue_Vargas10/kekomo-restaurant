<?php
//session_start();
// Required if your environment does not handle autoloading

require_once(APPPATH.'vendor/autoload.php');
//require __DIR__ . '/vendor/autoload.php';
//require site_url('/vendor/autoload.php') ;
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

require APPPATH.'../vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

class Chofer extends CI_Controller {
    //const USER_ERROR_DIR = APPPATH.'logs/Site_User_errors.log';
	function __construct()
	{
		parent::__construct();

        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->helper('array');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('correo');

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library("pagination");
        $this->load->library('upload');
        $this->load->library('email');

        $this->load->model('usuarios_model');
        $this->load->model('servicios_model');
        $this->load->model('operadores_model');
        $this->load->model('principal');
        $this->output->set_template('template_custom');

        date_default_timezone_set('America/Mexico_City');
    }

    public function solicitud_viaje($idServicio, $idOperador)
    {

			$this->output->set_template('template_confirmacion');

			$this->blade->render('chofer/confirmacion');
        // $this->load->view('chofer/solicitud_viaje');
    }




}//class
?>
