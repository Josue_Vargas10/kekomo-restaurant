<?php ob_start(); if(!defined('BASEPATH')) exit('No direct script access allowed');
//session_start();
// Required if your environment does not handle autoloading
require_once(APPPATH.'vendor/autoload.php');
//require __DIR__ . '/vendor/autoload.php';
//require site_url('/vendor/autoload.php') ;
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;
require APPPATH.'vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
class Usuarios extends CI_Controller {
    //const USER_ERROR_DIR = APPPATH.'logs/Site_User_errors.log';
	function __construct()
	{
        parent::__construct();
        
        

        $this->load->model('usuarios_model');
        $this->load->model('servicios_model');
        $this->load->model('operadores_model');
        $this->load->model('Principal');
        $this->load->model('Restaurantes_model');

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library("pagination");
        $this->load->library('upload');
        $this->load->library('email');

        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->helper('array');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('correo');
        $this->load->helper('language');
        $this->output->set_template('template_custom');

        date_default_timezone_set('America/Mexico_City');
	}

    public function index($lang = 'spanish')
    {                
				$this->output->set_template('main_template');
                $this->lang->load('general', $lang);          
                $data['browser']=['name' => $this->_get_browser_name()];
                $data['agent'] = $_SERVER['HTTP_USER_AGENT'];
                $this->blade->render('usuarios/inicio',$data);

    }		
	public function restaurante($lang='spanish'){
        $this->output->set_template('main_template');
        $this->lang->load('general', $lang);
        $this->blade->render('usuarios/restaurant');
    }

    public function rest_validation($code){
        
        $this->output->unset_template();

        $code = $this->_clean_code($code) ;

        $result = $this->usuarios_model->rest_validation($code);
                        
        if($result == false){
            //Este Warning significa que el restaurate no esta activo o no existe
            echo json_encode(array('warning' => 404));
        }else{
            echo json_encode(array('sucess' => 200,
                                     'data' => $result,
                                     'test' => $code) );
        }                
    }

    private function _clean_code($code){
        define('NUM_CHAR', 2);    
        return substr($code, 0, NUM_CHAR); ;        
    }

    public function qr_scanner(){
        $this->output->set_template('main_template');
        $this->blade->render('usuarios/qr_scanner');
    }
    
    public function vehiculos_disponibles($auto_selected){
        if ($this->input->is_ajax_request())
        {
            $this->output->unset_template();

            $tipo_vehiculo = $this->db->get_where('tipo_vehiculo', array('id_tipo_auto' => $auto_selected))->result();
            echo json_encode($tipo_vehiculo);
        }
    }
    public function cancelar_servicio()
    {
        $idServicio = $this->input->post('idServicio');
        $result = $this->servicios_model->update_servicio_usuario($idServicio, 3);
       echo 1;exit();
    }

	public function indexBk()
	{
        $_SESSION['hdOpcion'] = 1;
        if(isset($this->session->userdata['loggedin'])){
            $result = $this->servicios_model->get_servicio_usuario($this->session->userdata['loggedin']['id']);

            if ($result)
            {
                //echo $result;
                $_SESSION['solicitado'] = 1;
                $result = $this->servicios_model->get_infoservicio_usuario($this->session->userdata['loggedin']['id']);

                $data['latitud'] = $result[0]->servicioLatuitudInicio;
                $data['longitud'] = $result[0]->servicioLongitudInicio;
                $data['encontrado'] = 1;
                $data['idServicio'] =  $result[0]->servicioId;
                $this->load->view('servicios/index', $data);
            }else
            {
                $_SESSION['solicitado']  = 0;
                $this->load->view('servicios/index');
            }
        }
        else
             $this->load->view('usuarios/login');
    }

    public function proceso_login_usuario($data = null) {

        $this->form_validation->set_rules('celular', 'Celular', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');

        if ($this->form_validation->run() == FALSE && !isset($data)) {
            $this->load->view('usuarios/login');
        } else {
            $datalog = array(
                'celular' => (!isset($data))?$this->input->post('celular'):$data['celular'],
                'password' =>  (!isset($data))?$this->input->post('password'):$data['password']
            );
            //if($datalog['celular']=="chofer" && $datalog['password']=="chofer"){
            //    $data['datos_servicio']=$this->servicios_model->get_servicios_pendientes();
            //    //$this->session->set_flashdata('viaje_pendiente', $data);
            //    $this->session->userdata['viaje_pendiente']=$data;
            //    redirect('chofer/solicitud_viaje', 'refresh');
            //}else{

            $result = $this->usuarios_model->login($datalog);

            if ($result == TRUE) {
                $userCel = $datalog['celular'];
                $result = $this->usuarios_model->leer_informacion_usuario($userCel);
                if ($result != false) {
                    $session_data = array(
                        'usuario' => $result[0]->usuarioNombre,
                        'telefono' => $result[0]->usuarioCelular,
                        'id' => $result[0]->usuarioId,
                        'estado' => $result[0]->estado,
                        'municipio' => $result[0]->municipio,
                        'tipoUsuario' => $result[0]->tipoUsuario
                    );
                    $this->session->set_userdata('loggedin', $session_data);


                    if($result[0]->tipoUsuario == 2)
                    {//bases
                        if($_SESSION['hdOpcion']==4)
                            $this->estadistico();
                        else
                            if($_SESSION['hdOpcion']==5)
                                $this->estadistico_bases();
                            else
                                redirect("usuarios/index_base");
                    }
                    else
                    {
                        if(isset($_SESSION['hdOpcion'])){
                            $hdOpcion=$_SESSION['hdOpcion'];
                        }else{$hdOpcion=1;}

                            if($hdOpcion == 0)
                                $this->finalizar();
                            else
                            {
                                if($hdOpcion == 1){
                                    //$this->index();
                                    //servicio_id 2->Repartidores
                                    //vehiculo_id 4->Motocicleta
                                    $this->session->userdata['tipo_servicio']['tipo_servicio']=2;
                                    $this->session->userdata['tipo_servicio']['tipo_vehiculo']=4;

                                    $this->mapa();
                                }else{
                                    if($hdOpcion== 2)
                                        $this->misservicios();
                                    else
                                    {
                                        if($hdOpcion== 3)
                                            $this->comentarios();
                                        else
                                            $this->index();
                                    }
                                }
                            }

                    }
                }
            } else {
                $data = array(
                    'error_message' => 'Celular o contraseña inválida'
                );

                $this->load->view('usuarios/login', $data);
            }

            //}//...if username!="chofer",""chofer
        }
    }

    public function nuevo_usuario_registro() {
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirmacion', 'Confirmacion', 'trim|required|matches[password]');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('codigo', 'Codigo', 'trim|required');
        //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');
        $this->form_validation->set_message('matches', 'Oops el campo %s no coincide con el de %s!!');
        //$this->form_validation->set_message('valid_email', 'Oops el correo electrónico es inválido!!');

        $data['usuario'] = $this->input->post('usuario');
        $data['password'] = $this->input->post('password');
        $data['confirmacion'] = $this->input->post('confirmacion');
        $data['codigo'] = $this->input->post('codigo');
        $data['telefono'] = $this->input->post('telefono');
        //print $_SESSION['domicilio']; die();
        //$data['email'] = $this->input->post('email');
        $data['email'] = "";
        //$data['tipoUsuario'] = 2;

        $pos = strpos($this->input->post('usuario'), ' ');
        //$pos = strpos($this->input->post('telefono'), ' ');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('usuarios/registro', $data);
        } else {
            if($pos)
            {
                $data['message_display'] = 'El campo Usuario no debe contener espacios es blanco';
                $this->load->view('usuarios/registro', $data);
            }else{
                if($this->input->post('codigo') != $_SESSION['codigo']){
                    $data['message_codigo'] = 'El codigo es incorrecto, favor de verificarlo con el SMS que se envió al telefóno';
                    $this->load->view('usuarios/registro', $data);
                }else{
                    $data = array(
                        'usuarioNombre'   =>  $this->input->post('usuario'),
                        'usuarioCelular'   =>  $this->input->post('telefono'),
                        'usuarioStatus' => 1,
                        'usuarioPassword' => $this->input->post('password'),
                        'email'   =>  "",
                        'tipoUsuario'   =>  1,
                        'descripcionBase'   =>  "usuario",
                        'estado'   =>  "COL",
                        'municipio'   =>  "007",
                    );
                    //$result = $this->usuarios_model->insert_usuario($data);
                    $result = $this->usuarios_model->update_insert_usuario($data);
                    if ($result == TRUE) {
                        $data['message_display'] = 'Usuario registrado exitosamente!';

                        if( isset($_SESSION['codigo']))
                            unset($_SESSION['codigo']);
                        //$this->load->view('usuarios/login', $data);
                        $dataLogin = array(
                            'celular'   =>  $this->input->post('telefono'),
                            'password' => $this->input->post('password')
                        );
                        $this->proceso_login_usuario($dataLogin);
                    } else {
                        $data['message_display'] = 'El usuario ya existe!';
                        $this->load->view('usuarios/registro', $data);
                    }
                }
            }
        }
    }

    public function contrasena() {
        $this->output->unset_template();

        $username = $this->input->post('texto');
        $tipo = 1;

        $result = $this->usuarios_model->contrasena($username,$tipo);
        if ($result != false) {
            $usuario = trim($result[0]->usuarioNombre);
            $password = trim($result[0]->usuarioPassword);
            $tel = trim($result[0]->usuarioCelular);

            $accountSid = 'ACe8ab4310e97e582af509109e0d358ae6';
            $authToken = '04278a55999b7f0967f24b1b822ff3b8';
            $twilioNumber = '+17027103658';

            $telefono = "+52".$tel;
            //$codigo = $this->randomNumber(4);
            $message = 'Tus datos se acceso a kekomo.net son Celular:'.$tel.' Contraseña:'.$password;
            //$message = "Tu codigo de acceso a kekomo.net es el siguiente: ".$codigo;

            $client = new Client($accountSid, $authToken);

            try {
                $client->messages->create(
                    $telefono,
                    [
                        "body" => $message,
                        "from" => $twilioNumber
                    ]
                );

                $data_codigo['return'] = 1;
                $data_codigo['codigo'] = $tel;
                echo json_encode($data_codigo);

            } catch (TwilioException $e) {
                $data_codigo['return'] = 0;
                $data_codigo['codigo'] = $e->getMessage();

                echo json_encode($data_codigo);

            }catch(Exception $ex){
                $data_codigo['return'] = 0;
                $data_codigo['codigo'] = $ex->getMessage();

                echo json_encode($data_codigo);
            }
        }else
        {
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = "No existe registro con los datos proporcionados";
            echo json_encode($data_codigo);
        }
    }

    public function mostrar_registro() {
        $this->load->view('usuarios/registro');
    }

    public function mostrar_registro_operador() {
        $this->load->view('usuarios/operador');
    }


    public function file_check($str,$f){
        $allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png');
        $mime = get_mime_by_extension($_FILES[$f]['name']);

        if(isset($_FILES[$f]['name']) && $_FILES[$f]['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Oops debes seleccionar solamente imagenes jpg/png!!');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Oops debes seleccionar una imagen!!');
            return false;
        }
    }

    public function nuevo_operador_registro() {
       // print_r($_FILES);
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confirmacion]');
        $this->form_validation->set_rules('confirmacion', 'Confirmacion', 'trim|required');

        $existeplaca = $this->input->post('hdExiste');

        if($existeplaca == "0")
        {
            $this->form_validation->set_rules('sitio', 'Sitio', 'trim|required');
            $this->form_validation->set_rules('numero', 'Número', 'trim|required');
            $this->form_validation->set_rules('placas', 'Placas', 'trim|required');
            $this->form_validation->set_rules('modelo', 'Modelo', 'trim|required');
            $this->form_validation->set_rules('cmbTipo', 'Tipo', 'trim|required');
            $this->form_validation->set_rules('color', 'Color', 'trim|required');
            $this->form_validation->set_rules('marca', 'Marca', 'trim|required');
            $this->form_validation->set_rules('linea', 'Linea', 'trim|required');
        }

        $a = "archivo";
        $this->form_validation->set_rules('archivo', '', 'callback_file_check['.$a.']');


        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');

        $this->form_validation->set_message('matches', 'Oops el campo %s no coincide con el de %s!!');

        $data['nombre'] = $this->input->post('nombre');
        $data['usuario'] = $this->input->post('usuario');
        $data['password'] = $this->input->post('password');
        $data['confirmacion'] = $this->input->post('confirmacion');
        $data['sitio'] = $this->input->post('sitio');
        $data['numero'] = $this->input->post('numero');
        $data['telefono'] = $this->input->post('telefono');
        $data['placas'] = $this->input->post('placas');
        $data['modelo'] = $this->input->post('modelo');
        $data['cmbTipo'] = $this->input->post('cmbTipo');
        $data['color'] = $this->input->post('color');
        $data['marca'] = $this->input->post('marca');
        $data['linea'] = $this->input->post('linea');
        $data['hdExiste'] = $this->input->post('hdExiste');


        $pos = strpos($this->input->post('usuario'), ' ');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('usuarios/operador', $data);
        } else {
            if($pos)
            {
                $data['message_display'] = 'El usuario no debe contener espacios es blanco';
                $this->load->view('usuarios/operador', $data);
            }else{

                $data = array(
                    'usuario' => $this->input->post('usuario')
                );

                $result = $this->operadores_model->existe_operador($data);
                if ($result == FALSE) {
                    $name = date('dmyHms').'_'.str_replace(" ", "", $_FILES['archivo']['name']);
                    //$name = date('dmy').'_'.$this->input->post('usuario').'.'.$_FILES['archivo']['type'];
                    $path_to_save = 'statics/fotos/';

                    if(!file_exists($path_to_save)){
                      mkdir($path_to_save, 0777, true);
                    }
                    move_uploaded_file($_FILES['archivo']['tmp_name'], 'central_admin/'.$path_to_save.$name);

                    $data = array(
                        'operadorNombreCompleto'   =>  $this->input->post('nombre'),
                        'operadorUsuario'   =>  $this->input->post('usuario'),
                        'operdorPassword' => $this->input->post('password'),
                        'OperadorImagen' =>  "",
                        'OperadorImagen2' =>  $path_to_save.$name,
                        'operadorTelefono' => $this->input->post('telefono'),
                        'imei' =>  "1",
                        'operadorStatus' => "1"
                    );

                    $result = $this->operadores_model->insert_operador($data);

                    if ($result == TRUE) {
                        $idOperador = $result;

                        $result = $this->operadores_model->existe_auto( $this->input->post('placas'));
                        if ($result == FALSE) {
                            $descrcipcion = $this->input->post('marca').' '. $this->input->post('linea').' '. $this->input->post('modelo');
                            $data = array(
                                'autosPlacas'   =>  $this->input->post('placas'),
                                'autosDescripcion'   =>  $descrcipcion,
                                'autosImagen' =>  '',
                                'autosNick' =>  $this->input->post('numero'),
                                'autosColor' => $this->input->post('color'),
                                'autosSitio' => $this->input->post('sitio'),
                                'autosTipo' =>  $this->input->post('cmbTipo'),
                                'autosStatus' => "1"
                            );

                            $result = $this->operadores_model->insert_auto($data);
                            if ($result == FALSE) {
                                $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                                $this->load->view('usuarios/operador', $data);
                            }else
                                $idAuto = $result;
                        }else
                            $idAuto = $result[0]->autosId;

                        $data = array(
                            'OPIdOperador'   =>  $idOperador,
                            'OPIdAuto'   =>  $idAuto,
                            'OPStatust' =>  "1",
                            'OPLatitud' =>  "19.25566608394677",
                            'OPLongitud' => "-103.7515141069153",
                            'token' => "",
                            'imei' =>  "1"
                        );

                        $result = $this->operadores_model->insert_operador_auto($data);
                        if ($result == FALSE) {
                            $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                            $this->load->view('usuarios/operadores', $data);
                        }
                        else{

                            $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' => 'contactocentraldebases@gmail.com',
                                'smtp_pass' => 'e}1^nT3f,5tY6~',
                                'mailtype'  => 'html',
                                'charset'   => 'utf-8'
                            );

                            /*$config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'servidor6501.sd.controladordns.com',
                                'smtp_port' => 465,
                                'smtp_user' => 'contacto@centraldebases.com',
                                'smtp_pass' => 'e}1^nT3f,5tY6~',
                                'mailtype'  => 'html',
                                'charset'   => 'utf-8'
                            );*/

                            $this->load->library('email', $config);
                            $this->email->set_newline("\r\n");

                            $this->email->from('contacto@centraldebases.com', 'Central de Bases');
                            $this->email->to('vargaslagos@gmail.com');
                            //$this->email->to('pedrazarey@gmail.com');

                            $this->email->subject('Nuevo operador registrado');

                            if($this->input->post('cmbTipo') == 1)
                                $tipo = "AUTOMÓVIL";
                            else
                                $tipo = "CAMIONETA";


                            $mensaje =  "Nombre: " . $this->input->post('nombre') ."\r\n " .
                                        "Teléfono: " . $this->input->post('telefono') ." \r\n " .
                                        "Usuario: " . $this->input->post('usuario') ." \r\n " .
                                        "Sitio: " . $this->input->post('sitio')." ".$this->input->post('numero') ." \r\n " .
                                        "Placas: " . $this->input->post('placas') ."\r\n ".
                                        "Modelo: " . $this->input->post('modelo') ." \r\n ".
                                        "Tipo: " . $tipo . " ".
                                        "Color: " . $this->input->post('color') ." \r\n ".
                                        "Marca: " . $this->input->post('marca') ." \r\n ".
                                        "Línea: " . $this->input->post('linea') ;

                            $this->email->message(nl2br($mensaje));

                           // $this->email->message('Hola, esta es una prueba');

                            //$this->email->send();

                            /*if (!$this->email->send())
                            {
                              $this->email->print_debugger(array('headers'));
                            }
                            else
                            {*/
                                $data['message_display'] = 'Operador registrado exitosamente!';

                                $this->load->view('usuarios/operador', $data);
                            //}
                        }

                    } else {
                        $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                        $this->load->view('usuarios/operador', $data);
                    }
                }
                else
                {
                    $data['message_display'] = 'El usuario ya existe!';
                    $this->load->view('usuarios/operador', $data);
                }
            }
        }
    }

    public function nuevovehiculo() {
        $this->load->view('usuarios/nuevovehiculo');
    }

    public function nuevo_auto_registro() {
       // print_r($_FILES);
        $this->form_validation->set_rules('sitio', 'Sitio', 'trim|required');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required');
        $this->form_validation->set_rules('placas', 'Placas', 'trim|required');
        $this->form_validation->set_rules('modelo', 'Modelo', 'trim|required');
        $this->form_validation->set_rules('cmbTipo', 'Tipo', 'trim|required');
        $this->form_validation->set_rules('color', 'Color', 'trim|required');
        $this->form_validation->set_rules('marca', 'Marca', 'trim|required');
        $this->form_validation->set_rules('linea', 'Linea', 'trim|required');
        $this->form_validation->set_rules('cmbVehiculo', 'tipo de vehiculo', 'trim|required');

        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');

        $data['sitio'] = $this->input->post('sitio');
        $data['numero'] = $this->input->post('numero');
        $data['telefono'] = $this->input->post('telefono');
        $data['placas'] = $this->input->post('placas');
        $data['modelo'] = $this->input->post('modelo');
        $data['cmbTipo'] = $this->input->post('cmbTipo');
        $data['color'] = $this->input->post('color');
        $data['marca'] = $this->input->post('marca');
        $data['linea'] = $this->input->post('linea');
        $data['cmbVehiculo'] = $this->input->post('cmbVehiculo');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('usuarios/nuevovehiculo', $data);
        } else {
            $result = $this->operadores_model->existe_auto( $this->input->post('placas'));
            if ($result == FALSE) {
                $descripcion = $this->input->post('marca').' '. $this->input->post('linea').' '. $this->input->post('modelo');
                $data = array(
                    'autosPlacas'   =>  $this->input->post('placas'),
                    'autosDescripcion'   =>  $descripcion,
                    'autosImagen' =>  '',
                    'autosNick' =>  $this->input->post('numero'),
                    'autosColor' => $this->input->post('color'),
                    'autosSitio' => $this->input->post('sitio'),
                    'autosTipo' =>  $this->input->post('cmbTipo'),
                    'tipo_auto' =>  $this->input->post('cmbvehiculo'),

                );

                $result = $this->operadores_model->insert_auto($data);
                if ($result == FALSE) {
                    $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                    $this->load->view('usuarios/registrovehiculo', $data);
                }else
                {
                    $data['message_display'] = 'Vehiculo registrado exitosamente!';
                    $this->load->view('usuarios/registrovehiculo', $data);
                }
            }
        }
    }


    public function login($servicio_kekomo="")
    {
        $_SESSION['hdOpcion'] = 1;
        if($servicio_kekomo!=""){
            $this->session->userdata['tipo_servicio']['servicio_kekomo']=$servicio_kekomo;

        }
        $this->load->view('usuarios/login');
    }

    public function logout() {
        if(isset($this->session->userdata['loggedin'])){
            if($this->session->userdata['loggedin']['tipoUsuario'] != 2)
            {
                $sess_array = array(
                    'usuario' => ''
                );

                if( isset($_SESSION['telefono']))
                    unset($_SESSION['telefono']);
                if( isset($_SESSION['codigo']))
                    unset($_SESSION['codigo']);
                if( isset($_SESSION['latitud']))
                    unset($_SESSION['latitud']);
                if( isset($_SESSION['longitud']))
                    unset($_SESSION['longitud']);
                if( isset($_SESSION['domicilio']))
                    unset($_SESSION['domicilio']);
                if( isset($_SESSION['hdOpcion']))
                    unset($_SESSION['hdOpcion']);
                if( isset($_SESSION['solicitado']))
                    unset($_SESSION['solicitado']);
                $result = $this->servicios_model->update_servicio_usuario(0,3);
                $this->session->unset_userdata('loggedin', $sess_array);
                $this->index();
            }
            else
            {
                $this->session->unset_userdata('loggedin', $sess_array);
                $this->index_base();
            }
        }

    }

    public function existe()
    {
        $usuarioNombre = $this->input->post('usuario');
        $this->output->unset_template();
        try{
            $pos = strpos($usuarioNombre, ' ');

            if($pos)
            {
                $data_codigo['return'] = 3;
                $data_codigo['codigo'] = "El usuario no debe contener espacios en blanco";
                echo json_encode($data_codigo);
            }else{
                $data = array(
                    'usuario' => $usuarioNombre
                    //'telefono' => $usuarioCelular
                );
                $result = $this->usuarios_model->existe($data);

                if ($result == false) {
                    $data_codigo['return'] = 0;
                    echo json_encode($data_codigo);
                }else{
                    $data_codigo['return'] = 1;
                    echo json_encode($data_codigo);
                }
            }

        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
            echo json_encode($data_codigo);
        }
    }

    public function existeNumero()
    {
        $usuarioCelular = $this->input->post('numero');
        $this->output->unset_template();
        try{

            $data = array(
                //'usuario' => $usuarioNombre
                'telefono' => $usuarioCelular
            );
            $result = $this->usuarios_model->existeNumero($data);

            if ($result == false) {
                $data_codigo['return'] = 0;
                echo json_encode($data_codigo);
            }else{
                $data_codigo['return'] = 1;
                echo json_encode($data_codigo);
            }
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
            echo json_encode($data_codigo);
        }
    }

    public function existe_operador($usuarioNombre)
    {
        $this->output->unset_template();
        try{
            $data = array(
                'usuario' => $usuarioNombre
            );
            $result = $this->operadores_model->existe_operador($data);

            if ($result == false) {
                $data_codigo['return'] = 0;
                echo json_encode($data_codigo);
            }else{
                $data_codigo['return'] = 1;
                echo json_encode($data_codigo);
            }

        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
            echo json_encode($data_codigo);
        }
    }

    public function solicitar_codigo($tel)
    {
        $this->output->unset_template();

        $limit_intentos=3;
        
        $usuarioNombre=$this->input->post('usuario');
        $intentos=$this->input->post('num_req');//almacena el numero de intentos de solicitud de codigo

        $c['telefono']=$tel;
        $existeNumero=$this->usuarios_model->existeNumero($c,"2");

        if( isset($_SESSION['codigo']) && $existeNumero)
        {
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $_SESSION['codigo'];
            $data_codigo['msg']    = "Ya se ha enviado un código a este número";
            //echo json_encode($data_codigo);
        }
        else{
            if($intentos<$limit_intentos){

                $codigo = $this->randomNumber(4);

                $data = array(
                    'usuarioNombre'   =>  $usuarioNombre,
                    'usuarioCelular'   =>  $this->input->post('telefono'),
                    'usuarioStatus' => 2,
                    'usuarioPassword' => $this->input->post('password'),
                    'codigo' => $codigo
                );

                $result = $this->usuarios_model->insert_usuario($data);

                /*$data_codigo['return'] = 1;
                $data_codigo['codigo'] = $codigo;
                $_SESSION['codigo'] = $codigo;
                $_SESSION['telefono'] = $tel;

                echo json_encode($data_codigo);*/

                /*try {

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => 'http://uniendogeneraciones.com.mx/app_mensajes/index.php/api/enviar_notificacion?codigo='.$codigo.'&numero='.$tel,
                        CURLOPT_USERAGENT => 'cURL Request'
                    ));
                    $resp = curl_exec($curl);
                    curl_close($curl);

                    $data_codigo['return'] = 1;
                    $data_codigo['codigo'] = $codigo;
                    $_SESSION['codigo'] = $codigo;
                    $_SESSION['telefono'] = $tel;
                    echo json_encode($data_codigo);

                }catch(Exception $ex){
                    $data_codigo['return'] = 0;
                    $data_codigo['codigo'] = $ex->getMessage();
                    //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR);

                    unset($_SESSION['codigo']);
                    unset($_SESSION['telefono']);
                    echo json_encode($data_codigo);
                }*/

                $accountSid = 'ACe8ab4310e97e582af509109e0d358ae6';
                $authToken = '04278a55999b7f0967f24b1b822ff3b8';
                $twilioNumber = '+17027103658';

                $telefono = "+52".$tel;
                //$codigo = $this->randomNumber(4);
                $message = "Tu codigo de acceso a kekomo.net es el siguiente: ".$codigo;

                $client = new Client($accountSid, $authToken);

                try {
                    $client->messages->create(
                        $telefono,
                        [
                            "body" => $message,
                            "from" => $twilioNumber
                        ]
                    );

                    $data_codigo['return']  = 1;
                    $data_codigo['codigo']  = $codigo;
                    $data_codigo['num_req'] =$intentos+1;
                    $data_codigo['msg']     = "código enviado";
                    $_SESSION['codigo']     = $codigo;
                    $_SESSION['telefono']   = $tel;
                    $_SESSION['num_req']    =$intentos+1;

                    //echo json_encode($data_codigo);

                } catch (TwilioException $e) {
                    $data_codigo['return'] = 0;
                    $data_codigo['codigo'] = $e->getMessage();
                    unset($_SESSION['codigo']);
                    unset($_SESSION['telefono']);
                    $data_codigo['msg']    = $data_codigo['codigo'];
                    //echo json_encode($data_codigo);

                }catch(Exception $ex){
                    $data_codigo['return'] = 0;
                    $data_codigo['codigo'] = $ex->getMessage();
                    $data_codigo['msg']    = $data_codigo['codigo'];
                    unset($_SESSION['codigo']);
                    unset($_SESSION['telefono']);

                    //echo json_encode($data_codigo);
                }
            }else{
                //se ha superado el limite de intentos para solicitar codigo
                $data_codigo['return'] = 3;
                $data_codigo['codigo'] = (isset($_SESSION['codigo']))?$_SESSION['codigo']:"0000";
                $data_codigo['num_req']=$intentos+1;
                $data_codigo['msg']    = "Se ha superado el limite de intentos para solicitar código";
                $_SESSION['num_req']=$intentos+1;

                //echo json_encode($data_codigo);
            }//...else intentos
         }//...else SESSION['codigo']
        
         echo json_encode($data_codigo);
    }

    public function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function servicios()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        /*$arrayData['telefono'] = $this->input->post('telefono');
        $arrayData['codigo'] = $this->input->post('codigo');*/

        //$this->load->view('servicios/index');//, $arrayData);
        $this->load->view('servicios/index');
        //$this->load->view('servicios/index', $telefono);
    }

    public function activarServicio($idServicio)
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->activar_servicio_usuario($idServicio);
            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "";

            echo json_encode($data_codigo);
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();

            echo json_encode($data_codigo);
        }
    }

    public function operador_Servicio()
    {
        $this->output->unset_template();

        $idServicio =  $this->input->post('idServicio');
        $idOperador =  $this->input->post('idOperador');
        $estatus =  $this->input->post('estatus');

        try {
            $data = array(
                'idServicio'   =>  $idServicio,
                'idOperador' => $idOperador,
                'estatus' => $estatus
            );

            $result = $this->servicios_model->operador_servicio($data);

            $data_codigo['return'] = 1;

            echo json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();

            echo json_encode($data_codigo);
        }
    }


    public function grabarComentario()
    {

        $this->form_validation->set_rules('comentario', 'Comentario', 'trim|required');
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('servicios/comentarios');
        }
        else {

            $idusuario = $this->session->userdata['loggedin']['id'];
            $comentario =  $this->input->post('comentario');
            $post_data = array(
                'buzonIdUsuario'   =>  $idusuario,
                'buzonComentario' => $comentario
            );

            $servicio = $this->servicios_model->grabar_comentario($post_data);
            $this->index();
        }
    }

    public function misservicios()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $_SESSION['hdOpcion'] = 2;
        if (!isset($this->session->userdata['loggedin']))
            $this->load->view('usuarios/login');
         else
         {
            //obtiene los servicios finalizados
            $result = $this->servicios_model->get_todos_servicios($this->session->userdata['loggedin']['id']);
            //print_r($result);
            $data['finalizados'] =$result;

            $this->load->view('servicios/misservicios', $data);
        }
    }

    public function pedirtaxi()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $this->load->view('servicios/pedirtaxi');
    }

    public function comentarios()
    {
        $this->load->helper('array');
        $this->load->helper('url');


         $_SESSION['hdOpcion'] = 3;
        if (!isset($this->session->userdata['loggedin']))
            $this->load->view('usuarios/login');
         else
         {
            $this->load->view('servicios/comentarios');
        }
    }

    public function listado($idservicio)
    {
        $data['servicio'] =$idservicio;
        $this->load->view('servicios/finalizar', $data);
    }

    public function operadores($idServicio)
    {

        $this->output->unset_template();

        //$result = $this->servicios_model->get_operadores_activos();
        $result = $this->servicios_model->get_operadores_activos_no_notificados($idServicio,1);
        //print_r($result);die();
        echo json_encode($result);
    }

    public function get_operadores()
    {

        $this->output->unset_template();

        //$result = $this->servicios_model->get_operadores_activos();
        $result = $this->operadores_model->get_operadores();
        //print_r($result);die();
        echo json_encode($result);
    }

    public function pruebas()
    {
        $this->output->unset_template();


        print APPPATH.'vendor/autoload.php';
        print APPPATH.'../vendor/autoload.php';

    }

    public function getPlatform($user_agent) {
       $plataformas = array(
          'Windows 10' => 'Windows NT 10.0+',
          'Windows 8.1' => 'Windows NT 6.3+',
          'Windows 8' => 'Windows NT 6.2+',
          'Windows 7' => 'Windows NT 6.1+',
          'Windows Vista' => 'Windows NT 6.0+',
          'Windows XP' => 'Windows NT 5.1+',
          'Windows 2003' => 'Windows NT 5.2+',
          'Windows' => 'Windows otros',
          'iPhone' => 'iPhone',
          'iPad' => 'iPad',
          'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
          'Mac otros' => 'Macintosh',
          'Android' => 'Android',
          'BlackBerry' => 'BlackBerry',
          'Linux' => 'Linux',
       );
       foreach($plataformas as $plataforma=>$pattern){
          if (eregi($pattern, $user_agent))
             return $plataforma;
       }
       return 'Otras';
    }

    public function status_servicio($idServicio)
    {
        $this->output->unset_template();
        $result = $this->servicios_model->get_status_servicio($idServicio);
        if ($result == TRUE)
            echo json_encode($result);
        else
             echo json_encode(0);
    }

    public function cancelar()
    {
        $idServicio = $this->input->post('hdServicioCancelar');

        try {
            $result = $this->servicios_model->update_servicio_usuario($idServicio, 3);
            $this->index();
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();

            $this->load->view('servicios/index', $data_codigo);
        }
    }

    public function check_servicio_temporal(){
        $this->output->unset_template();

        echo json_encode($this->session->userdata);

    }


    public function update_session_servicio_temporal(){
        $this->output->unset_template();
        $idusuario=$this->session->userdata['loggedin']['id'];

        $postForm=$this->input;

        $flag=$postForm->post('flag');//

        if($flag=="origin" || $flag=="destination"){
            $tipo=$postForm->post('tipo');//origin o destination
            $lat=$postForm->post('lat');
            $lng=$postForm->post('lng');
            $formatted_address=$postForm->post('formatted_address');
            $calle_numero=$postForm->post('calle_numero');
            $place_name=$postForm->post('place_name');
            $place_id=$postForm->post('place_id');

            $newSession = array(
                'temp_serv_idUsuario'   =>  $idusuario,
                'temp_serv_tipo' => $tipo,
                'temp_serv_lat' => $lat,
                'temp_serv_lng' => $lng,
                'temp_serv_address' => $formatted_address,
                'temp_serv_calle_numero' => $calle_numero,
                'temp_serv_place_name' => $place_name,
                'temp_serv_place_id' => $place_id
            );

            if($flag=="origin"){
                $this->session->userdata['tripdata']['origin']=$newSession;
            }
            if($flag=="destination"){
                //$this->session->set_userdata("destination",$post_data);
                $this->session->userdata['tripdata']['destination']=$newSession;

            }
        }

        if($flag=="info_trayecto"){
            $duracion=$postForm->post('duracion');
            $distancia=$postForm->post('distancia');
            $post_data = array(
                'duracion'   =>  $duracion,
                'distancia' => $distancia
            );

            $newSession=$post_data;
            //$this->session->set_userdata("info_trayecto",$newSession);
            $this->session->userdata['tripdata']['info_trayecto']=$newSession;
            $whatsapp_message="https://api.whatsapp.com/send?phone=521".$this->session->loggedin['telefono']."&text=Hola!%20Hice%20un%20pedido";
            $this->session->userdata['loggedin']['msg_whatsapp']=$whatsapp_message;
            $arr=array("msg_whatsapp"=>$whatsapp_message);
            $this->session->set_userdata('msg_whatsapp',$arr);
        }

        if($flag=="tipo_servicio"){
            $tipoServicio=$postForm->post('tipo_servicio');
            $tipoVehiculo=$postForm->post('tipo_vehiculo');

            $newSession["tipo_servicio"]=$tipoServicio;
            $newSession["tipo_vehiculo"]=$tipoVehiculo;
            //$this->session->set_userdata("tipo_servicio",$newSession);
            $this->session->set_userdata('tipo_servicio',$newSession);

        }


        echo json_encode($this->session->userdata);

    }//...update_session_servicio_temporal
    public function finalizar()
    {

        $data['latitud'] = $this->input->post('hdLatitud');
        $data['longitud'] = $this->input->post('hdLongitud');
        $data['domicilio'] = $this->input->post('hdDomicilio');
        $_SESSION['hdOpcion'] = 0;
        if (!isset($this->session->userdata['loggedin'])) {
            $this->load->view('usuarios/login',$data);
        } else{

            $result = $this->servicios_model->get_servicio_usuario($this->session->userdata['loggedin']['id']);
            //echo $result; die();
            if ($result == TRUE)
            {
                //echo $result;
                $data['encontrado'] = 1;
                $_SESSION['solicitado'] = 1;
                $result = $this->servicios_model->get_infoservicio_usuario($this->session->userdata['loggedin']['id']);
                $data['latitud'] = $result[0]->servicioLatuitudInicio;
                $data['longitud'] = $result[0]->servicioLongitudInicio;
                $data['idServicio'] =  $result[0]->servicioId;
                $data['mensaje'] = "Ya tiene un servicio . Por favor cancele para poder solicitar uno nuevo.";
                $this->load->view('servicios/index', $data);
            }else
            {
                $_SESSION['solicitado'] = 0;
                $telefono = ($this->session->userdata['loggedin']['telefono']);
                $idusuario = ($this->session->userdata['loggedin']['id']);
                //$codigo = $this->input->post('hdCodigo');

                $latitud = $this->input->post('hdLatitud');
                $longitud = $this->input->post('hdLongitud');



                $comentario = $this->input->post('hdDomicilio');

                $post_data = array(
                    'servicioIdoperadorAuto'   =>  null,
                    'servicioIdUsuario'   =>  $idusuario,
                    'servicioStatus' => 1,
                    'servicioFechaCreacion' =>  NULL,
                    'servicioLatuitudInicio' => $latitud,
                    'servicioLongitudInicio' => $longitud,
                    'servicioLatitudFin' => null,
                    'servicioLongitudFin' => null,

                    'servicioComentario' => $comentario,
                    'OneSignalId' => ""
                );
                $servicio = $this->servicios_model->insert_servicio($post_data);
                //print_r($servicio);
                if($servicio != 0)
                {
                    //notifico libres dentro de 5kms
                    $operadoreslibres = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 1);
                    if(count($operadoreslibres)>0)
                    {
                        foreach ($operadoreslibres as $operador) {
                            if($operador['kilometros']<=2.5)
                                $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio);
                        }
                    }

                    //notifico ocupados dentro de 10kms
                    $operadoresocupados = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 0);

                    if(count($operadoresocupados)>0)
                    {
                        foreach ($operadoresocupados as $operador) {
                            if($operador['kilometros']<=5)
                                $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio);
                        }
                    }

                    if(count($operadoreslibres)>0 or count($operadoresocupados)>0)
                    {
                        $data['mensaje'] = "Hemos recibido tu solicitud. Se ha notificado al operador más cercano. Espera a que se ponga en contacto contigo";
                    }
                    else
                    {
                        $data['mensaje'] = "Hemos recibido tu solicitud. Espera a que Central de Bases se comunique contigo o selecciona en el siguiente mapa el vehículo más cercano.";
                    }

                    $this->output->set_template('template');

                    $data['latitud'] = $latitud;
                    $data['longitud'] = $longitud;
                    $data['idServicio'] =  $servicio;
                    $data['encontrado'] = 1;

                    $_SESSION['solicitado'] = 1;
                    $this->load->view('servicios/index', $data);
                    //$this->load->view('servicios/success', $data);
                }
                else
                    echo "Ha ocurrido un error, por favor intentelo más tarde";
            }
        }
    }

    public function obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, $libres)
    {
        $latitud1 = floatval($latitud);
        $longitud1 = floatval($longitud);

        $choferes = $this->servicios_model->get_operadores_activos_no_notificados($servicio, $libres);
        //print_r($choferes);
        $response=array();
        foreach($choferes as $row):
            $res=array();
            $latitud2 = floatval($row->OPLatitud);
            $longitud2 = floatval($row->OPLongitud);
            $distancia = (3958*3.1415926*sqrt(($latitud2-$latitud1)*($latitud2-$latitud1) + cos($latitud2/57.29578)*cos($latitud1/57.29578)*($longitud2-$longitud1)*($longitud2-$longitud1))/180)*1.609344;
            $kilometros = $distancia;
            $res['kilometros']=$distancia;
            $res['id_chofer']=$row->OPIdOperador;
            array_push($response, $res);
        endforeach;

        $n = count($response);
        for($i=1;$i<$n;$i++)
        {
            for($j=0;$j<$n-$i;$j++)
            {
                if($response[$j]['kilometros']>$response[$j+1]['kilometros'])
                {

                    $k1=$response[$j+1]['id_chofer'];
                    $k2=$response[$j+1]['kilometros'];

                    $response[$j+1]['id_chofer']=$response[$j]['id_chofer'];
                    $response[$j+1]['kilometros']=$response[$j]['kilometros'];

                    $response[$j]['id_chofer']=$k1;
                    $response[$j]['kilometros']=$k2;

                }
            }
        }
        return $response;
    }

    public function enviar_notificacion_noreturn($operador, $servicio)
    {
        $this->output->unset_template();
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1,
                CURLOPT_URL => 'http://globodi.com/centraldebases.com/central_admin/index.php/api/solicitar_operador_uno',
                CURLOPT_USERAGENT => 'cURL Request',
                CURLOPT_POSTFIELDS => 'id_operador='.$operador.'&id_servicio='.$servicio.'&todos=1'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);


            $data = array(
                'idServicio'   =>  $servicio,
                'idOperador' => $operador,
                'estatus' => 6
            );

            $result = $this->servicios_model->operador_servicio($data);

            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "El chofer ha sido notificado. Espera a que Central de Bases te contacte";
            return json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 0;
            $data_codigo['codigo'] = $ex->getMessage();
            //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR);
            return json_encode($data_codigo);
        }
    }

    public function enviar_notificacion($operador, $servicio, $actualizar)
    {
        $this->output->unset_template();
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1,
                CURLOPT_URL => 'http://globodi.com/centraldebases.com/central_admin/index.php/api/solicitar_operador_cero',
                CURLOPT_USERAGENT => 'cURL Request',
                CURLOPT_POSTFIELDS => 'id_operador='.$operador.'&id_servicio='.$servicio.'&todos=0'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);


            $data = array(
                'idServicio'   =>  $servicio,
                'idOperador' => $operador,
                'estatus' => 5
            );

            if($actualizar == "1")
            {
                $result = $this->servicios_model->update_servicio_usuario($servicio, 5, $operador);

                $result = $this->servicios_model->operador_servicio($data);
            }

            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "El chofer ha sido notificado. Espera a que Central de Bases te contacte";
            echo json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 0;
            $data_codigo['codigo'] = $ex->getMessage();
            //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR);
            echo json_encode($data_codigo);
        }
    }

    public function enviar_notificacion_noreturn_prueba($operador, $servicio)
    {
        $this->output->unset_template();
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1,
                CURLOPT_URL => 'https://www.centraldebases.com/central_admin/index.php/api/solicitar_operador_uno',
                CURLOPT_USERAGENT => 'cURL Request',
                CURLOPT_POSTFIELDS => 'id_operador='.$operador.'&id_servicio='.$servicio.'&todos=1'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);


            $data = array(
                'idServicio'   =>  $servicio,
                'idOperador' => $operador,
                'estatus' => 5
            );

            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "El chofer ha sido notificado. Espera a que Central de Bases te contacte";
            return json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 0;
            $data_codigo['codigo'] = $ex->getMessage();
            //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR);
            return json_encode($data_codigo);
        }
    }

    public function obtenerServicios()
    {
        $this->output->unset_template();
        $result = $this->servicios_model->get_todos_servicios($this->session->userdata['loggedin']['id']);
            //print_r($result);
        $data['servicios'] =$result;
        echo json_encode($data);
    }
    public function obtenerServiciosFrecuentes($groupbycampo)
    {
        $this->output->unset_template();
        if($groupbycampo == 'servicioOrigenPlaceId'){
            $result = $this->servicios_model->get_servicios_frecuentes_orig($this->session->userdata['loggedin']['id'],$groupbycampo);
        }else{
            $result = $this->servicios_model->get_servicios_frecuentes_dest($this->session->userdata['loggedin']['id'],$groupbycampo);
        }
       

            //print_r($result);
        $data['servicios'] =$result;
        echo json_encode($data);
    }
    public function grabarRating()
    {
        $idServicio = $this->input->post('hdIdServicio');
        $rating =  $this->input->post('rating');
        $comentario =  $this->input->post('comentario');

        $post_data = array(
            'idServicio'   =>  $idServicio,
            'rating' => $rating,
            'comentario' => $comentario
        );

        //print_r($post_data);die();

        $servicio = $this->servicios_model->update_rating($post_data);
        redirect('usuarios/misservicios');
    }

    public function nuevo_operador_vehiculo()
    {
        $this->load->view('usuarios/registrovehiculo');
    }

    public function buscarparcial()
    {
        $this->output->unset_template();

        $texto = $_POST['texto'];

        ///////// PAGINACION /////////
          $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $this->operadores_model->buscarparcial($texto, 1);
          $config["per_page"] = 5;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination" id="opera" name="opera">';
          $config["full_tag_close"] = '</ul>';
          $config["first_link"] = "Primero";
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_link"] = "Último";
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'>";
          $config["cur_tag_close"] = "</li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;

          $page = $this->uri->segment(3);
          $start = ($page - 1) * $config["per_page"];

        $result= $this->operadores_model->buscarparcial($texto,0,$config["per_page"], $start);
        $this->pagination->initialize($config);

        if ($result == false) {
            $strTableH = "<table class='table table-hover' id='tblOperadores' name='tblOperadores'><thead>
                    <tr>
                      <th scope='col' colspan='5' class='text-center'> NO SE ENCONTRARON COINCIDENCIAS</th>
                    </tr>
                  </thead></table>";

        }else {
             $strTableH = "<table class='table table-hover' id='tblOperadores' name='tblOperadores'><thead><tr><th scope='col'>Id</th><th scope='col'>Nombre</th><th scope='col'>Seleccionar</th></tr></thead><tbody>";

             foreach($result as $row) {

                $strTableH = $strTableH."<tr>
                  <th scope='row'>".$row->operadorId."</th>
                  <td>".$row->operadorNombreCompleto."</td>
                  <td><input type='radio' name='rbseleccionar' id='rbseleccionar' onclick='selectoperador(\"".$row->operadorId."\")'></td></tr>";
            }
            $strTableH = $strTableH."</tbody></table>";
        }

        $str_links = $this->pagination->create_links();

         $output = array(
           'pagination_link'  => $str_links,
           'country_table'   => $strTableH,
           'texto' => $texto
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function buscarparcialveh()
    {
        $this->output->unset_template();

        $texto = $_POST['texto'];

        $result= $this->operadores_model->buscarparcialveh($texto);

        if ($result == false) {
            $strTableH = "<table class='table table-hover' id='tblVehiculos' name='tblVehiculos'><thead>
                    <tr>
                      <th scope='col' colspan='4' class='text-center'> NO SE ENCONTRARON COINCIDENCIAS</th>
                      <th scope='col' class='text-center'> <a href='javascript:nuevovehiculo()'><b>AGREGAR</b></a></th>
                    </tr>
                  </thead></table>";

        }else {
             $strTableH = "<table class='table table-hover' id='tblVehiculos' name='tblVehiculos'><thead><tr><th scope='col'>Placa</th><th scope='col'>Descripción</th><th scope='col'>Seleccionar</th></tr></thead><tbody>";

             foreach($result as $row) {

                $strTableH = $strTableH."<tr>
                  <th scope='row'>".$row->autosPlacas."</th>
                  <td>".$row->autosDescripcion."</td>
                  <td><input type='radio' name='rbseleccionarVeh' id='rbseleccionarVeh' onclick='selectvehiculo(\"".$row->autosId."\")'></td></tr>";
            }
            $strTableH = $strTableH."</tbody></table>";
        }

         $output = array(
           'country_table_veh'   => $strTableH,
           'texto' => $texto
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function grabarAsignacion()
    {
        $idOperador = $this->input->post('hdIdOperador');
        $idAuto =  $this->input->post('hdIdVehiculo');

        $data = array(
            'OPIdOperador'   =>  $idOperador,
            'OPIdAuto'   =>  $idAuto,
            'OPStatust' =>  "1",
            'OPLatitud' =>  "19.25566608394677",
            'OPLongitud' => "-103.7515141069153",
            'token' => "",
            'imei' =>  "1"
        );

        $result = $this->operadores_model->insert_operador_auto($data);
        if ($result == FALSE) {
            $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
            $this->load->view('usuarios/registrovehiculo', $data);
        }
        else{
            $data['message_display'] = 'Operador asignado exitosamente!';
            $this->load->view('usuarios/registrovehiculo', $data);
        }
    }


		public function rechazar_servicio(){
				$this->output->unset_template();
				$data = array(
					'table_name' => 'servicios', // pass the real table name
					'id' => $this->input->post('idServicio'),
					'status' => 4,
					'idOperador' => NULL
				);


				$this->load->model('Servicios_model');
				if($this->Servicios_model->aceptar_servicio($data)) // call the method from the model
		    {
					header('Content-Type: application/json');
					echo json_encode( array( 'success' => True));
		    }
		    else
		    {
					header('Content-Type: application/json');
					return json_encode( array( 'success' => False));
		    }
		}

		public function aceptar_servicio(){
				$this->output->unset_template();
				$data = array(
					'table_name' => 'servicios', // pass the real table name
					'id' => $this->input->post('idServicio'),
					'status' => 2,
					'idOperador' => $this->input->post('idOperador')
				);


				$this->load->model('Servicios_model');
				if($this->Servicios_model->aceptar_servicio($data)) // call the method from the model
				{
					header('Content-Type: application/json');
					echo json_encode( array( 'success' => True));
				}
				else
				{
					header('Content-Type: application/json');
					return json_encode( array( 'success' => False));
				}
		}

		public function iniciar_servicio(){
			$this->output->unset_template();
			$data = array(
				'table_name' => 'servicios', // pass the real table name
				'id' => $this->input->post('idServicio'),
				'status' => 5
			);
			$this->load->model('Servicios_model');
			if($this->Servicios_model->iniciar_servicio($data)) // call the method from the model
			{
					header('Content-Type: application/json');
					echo json_encode( array( 'success' => True));
			}
			else
			{
					header('Content-Type: application/json');
					return json_encode( array( 'success' => False));
			}
		}

		public function finalizar_servicio(){
			$this->output->unset_template();
			$data = array(
				'table_name' => 'servicios', // pass the real table name
				'id' => $this->input->post('idServicio'),
				'status' => 6,
				'servicioFechaFinalizacion' =>  date("Y-m-d H:i:s")

			);
			$this->load->model('Servicios_model');
			if($this->Servicios_model->finalizar_servicio($data)) // call the method from the model
			{
				header('Content-Type: application/json');
				echo json_encode( array( 'success' => True));
			}
			else
			{
				header('Content-Type: application/json');
				return json_encode( array( 'success' => False));
			}
		}


    public function cancelar_servicios()
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->cancelar_servicios_pendientes();
            //echo json_encode(1);
            //print_r($result);
        }catch(Exception $ex){
            echo $ex;
        }
    }

    public function liberar_servicios()
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->liberar_servicios();
            //echo json_encode(1);
            print_r($result);
        }catch(Exception $ex){
            echo $ex;
        }
    }

    public function existe_vehiculo($placa)
    {
        $this->output->unset_template();
        $result = $this->operadores_model->existe_auto($placa);

        if ($result)
        {
            $data['encontrado'] = 1;
            $data['vehiculo'] = $result;
        }
        else
            $data['encontrado'] = 0;

        echo json_encode($data);
    }

    public function index_base()
    {
        $this->load->view('servicios/index_base');
    }

    public function obtieneCatEstados()
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_estados();
        $data['estados'] = $result;

        echo json_encode($data);
    }

    public function obtieneCatMunicipios($entidad)
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_municipios($entidad);
        $data['municipios'] = $result;

        echo json_encode($data);
    }

    public function obtieneCatLocalidades($entidad, $municipio)
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_localidades($entidad, $municipio);
        $data['localidades'] = $result;

        echo json_encode($data);
    }

    public function solicitar_base()
    {
        //print_r($_POST);die();

        $data['latitud'] = $this->input->post('hdLatitud');
        $data['longitud'] = $this->input->post('hdLongitud');
        $data['domicilio'] = $this->input->post('hdDomicilio');
        $data['hdNombre'] = $this->input->post('hdNombre');
        $data['hdTelefono'] = $this->input->post('hdTelefono');
        $data['hdObservaciones'] = $this->input->post('hdObservaciones');
        $data['domicilioUsuario'] = $this->input->post('hdDomicilioUsuario');

        if (!isset($this->session->userdata['loggedin'])) {
            $this->load->view('usuarios/login',$data);
        }
        else
        {
            $latitud = $this->input->post('hdLatitud');
            $longitud = $this->input->post('hdLongitud');
            $nombre = $this->input->post('hdNombre');
            $telefono = $this->input->post('hdTelefono');
            $idusuario = ($this->session->userdata['loggedin']['id']);
            $comentario = $this->input->post('hdDomicilio');
            $observaciones = $this->input->post('hdObservaciones');
            $domicilioUsuario = $this->input->post('hdDomicilioUsuario');

            $post_data = array(
                'servicioIdoperadorAuto'   =>  null,
                'servicioIdUsuario'   =>  $idusuario,
                'servicioStatus' => 1,
                'servicioFechaCreacion' =>  NULL,
                'servicioLatuitudInicio' => $latitud,
                'servicioLongitudInicio' => $longitud,
                'servicioLatitudFin' => null,
                'servicioLongitudFin' => null,
                'servicioComentario' => $comentario,
                'OneSignalId' => "",
                'nombreSolicitante' => $nombre,
                'celularSolicitante' => $telefono,
                'observacionesSolicitante' => $observaciones,
                'domicilioSolicitante' => ucwords(strtolower($domicilioUsuario)),
                'base' => 1
            );
            /* $servicio = $this->servicios_model->insert_servicio($post_data); */
            //print_r($servicio);
            if($servicio != 0)
            {

                $this->notificar_operadores($latitud, $longitud, $servicio);

                $this->output->set_template('template');

                redirect('usuarios/index_base');
                //$this->load->view('servicios/success', $data);
            }
            else
                echo "Ha ocurrido un error, por favor intentelo más tarde";
        }
    }

    public function notificar_operadores($latitud, $longitud, $servicio)
    {
        //notifico libres dentro de 5kms
        $operadoreslibres = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 1);
        if(count($operadoreslibres)>0)
        {
            foreach ($operadoreslibres as $operador) {
                if($operador['kilometros']<=2.5)
                    $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio);
            }
        }

        //notifico ocupados dentro de 10kms
        $operadoresocupados = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 0);

        if(count($operadoresocupados)>0)
        {
            foreach ($operadoresocupados as $operador) {
                if($operador['kilometros']<=5)
                    $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio);
            }
        }
    }

    public function obtieneCatColonias($entidad, $municipio)
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_colonias($entidad, $municipio);
        $data['colonias'] = $result;

        echo json_encode($data);
    }

    public function servicios_base()
    {
        if (!isset($this->session->userdata['loggedin']))
            $this->load->view('usuarios/login');
         else
            $this->load->view('servicios/historial_bases');

    }

    public function serviciosparcial()
    {
        $this->output->unset_template();
        $idUsuario = $this->session->userdata['loggedin']['id'];

        ///////// PAGINACION /////////
          $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $this->servicios_model->serviciosparcial(1,null,null,null,$idUsuario);
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination">';
          $config["full_tag_close"] = '</ul>';
          $config["first_link"] = "Primero";
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_link"] = "Último";
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'>";
          $config["cur_tag_close"] = "</li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;

          $page = $this->uri->segment(3);
          $start = ($page - 1) * $config["per_page"];
          //print $start;

        $result= $this->servicios_model->serviciosparcial(0,null,null,null,$idUsuario,$config["per_page"], $start);
        $this->pagination->initialize($config);

        if ($result == false) {
            $strTableH = "<table class='table table-hover table_servicios' id='tblServicios' name='tblServicios'><thead>
                    <tr>
                      <th scope='col' colspan='8' class='text-center'> NO SE ENCONTRARON SERVICIOS</th>
                    </tr>
                  </thead></table>";

        }else {
             $strTableH = "<table class='table table-hover table_servicios table-responsive' id='tblServicios' name='tblServicios'><thead><tr>
             <th scope='col' style='width:5%'>Id</th>
             <th scope='col' style='width:5%'>Estatus</th>
             <th scope='col' style='width:10%'>Fecha</th>
             <th scope='col' style='width:15%'>Cliente</th>
             <th scope='col' style='width:5%'>Celular</th>
             <th scope='col' style='width:20%'>Domicilio aproximado</th>
             <th scope='col' style='width:20%'>Domicilio capturado</th>
             <th scope='col' style='width:10%'>Operador</th>
             <th scope='col' style='width:10%'>Sitio</th>
             <th scope='col' style='width:5%'></th></tr></thead><tbody>";

             foreach($result as $row) {
                switch ($row->servicioStatus) {
                    case 1:
                        $strEstatus = "Pendiente";
                        $strclass = "thNormal";
                        $activar = 0;
                        break;
                    case 2:
                       $strEstatus = "Aceptado";
                       $strclass = "thVerde";
                       $activar = 0;
                        break;
                    case 3:
                        $strEstatus = "Cancelado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                    case 4:
                        $strEstatus = "Rechazado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                }
                $date = new DateTime($row->servicioFechaCreacion);
                $operador = "";
                $sitio = "";

                if(!is_null($row->servicioIdoperadorAuto))
                {
                    $datos = $this->operadores_model->datosOperadorAuto($row->servicioIdoperadorAuto);
                    $operador = $datos[0]->OperadorNombreCompleto;
                    $sitio = $datos[0]->autosSitio." ".$datos[0]->autosNick;
                }

                $strTableH = $strTableH."<tr class='".$strclass."'>
                  <th scope='row' class='text-justify'>".$row->servicioId."</a></th>
                  <td class='text-justify'>".$strEstatus."</a></td>
                  <td class='text-justify'>".$date->format('d/m/Y H:i:s')."</td>
                  <td class='text-justify'>".$row->nombreSolicitante."</td>
                  <td class='text-justify'>".$row->celularSolicitante."</td>
                  <td class='text-justify'>".$row->servicioComentario."</td>
                  <td class='text-justify'>".$row->domicilioSolicitante."</td>
                  <td class='text-justify'>".$operador."</td>
                  <td class='text-justify'>".$sitio."</td>";

                if($activar == 1 && $row->activar)
                    $strTableH = $strTableH."<td><button data-toggle='tooltip' data-placement='bottom' title='ACTIVAR' onclick='activar(\"".$row->servicioId."\")' >ACTIVAR</button></td>";
                else
                    $strTableH = $strTableH."<td></td>";

            }
            $strTableH = $strTableH."</tr></tbody></table>";
        }

        $str_links = $this->pagination->create_links();

         $output = array(
           'pagination_link'  => $str_links,
           'servicios_table'   => $strTableH
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function serviciosparcialfechas()
    {
        $this->output->unset_template();
        $fechaDesde = $_POST['fechaDesde'];
        $fechaHasta = $_POST['fechaHasta'];
        $estatus = $_POST['estatus'];
        $idUsuario = $_POST['usuario'];

        $date_ini = DateTime::createFromFormat('d/m/Y', $fechaDesde);
        $date_fin = DateTime::createFromFormat('d/m/Y', $fechaHasta);
        $sfechaDesde = $date_ini->format('Y-m-d').' 00:00:00';
        $sfechaHasta = $date_fin->format('Y-m-d').' 23:59:59';

        $numrows = $this->servicios_model->serviciosparcial(1, $sfechaDesde, $sfechaHasta, $estatus, $idUsuario);
        ///////// PAGINACION /////////
         $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $numrows;
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination">';
          $config["full_tag_close"] = '</ul>';
          $config["first_link"] = "Primero";
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_link"] = "Último";
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'>";
          $config["cur_tag_close"] = "</li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;

          $page = $this->uri->segment(3);
          $start = ($page - 1) * $config["per_page"];

        $result= $this->servicios_model->serviciosparcial(0, $sfechaDesde, $sfechaHasta, $estatus, $idUsuario, $config["per_page"], $start);
        $this->pagination->initialize($config);


        if ($result == false) {
            $strTableH = "<table class='table table-hover table_servicios table-responsive' id='tblServicios' name='tblServicios'><thead>
                    <tr>
                      <th scope='col' colspan='7' class='text-center'> NO SE ENCONTRARON SERVICIOS</th>
                    </tr>
                  </thead></table>";

        }else {
             $strTableH = "<table class='table table-hover table_servicios' id='tblServicios' name='tblServicios'><thead><tr>
             <th scope='col' style='width:15%'>Fecha</th>
             <th scope='col' style='width:5%'>Estatus</th>
             <th scope='col' style='width:20%'>Cliente</th>
             <th scope='col' style='width:25%'>Domicilio capturado</th>
             <th scope='col' style='width:20%'>Operador</th>
             <th scope='col' style='width:15%'>Sitio</th>
             </tr></thead>";

             foreach($result as $row) {
                switch ($row->servicioStatus) {
                    case 1:
                        $strEstatus = "Pendiente";
                        $strclass = "thNormal";
                        $activar = 0;
                        break;
                    case 2:
                       $strEstatus = "Aceptado";
                       $strclass = "thVerde";
                       $activar = 0;
                        break;
                    case 3:
                        $strEstatus = "Cancelado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                    case 4:
                        $strEstatus = "Rechazado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                }

                $date = new DateTime($row->servicioFechaCreacion);
                $operador = "";
                $sitio = "";
                $descripcion = "";

                if(!is_null($row->servicioIdoperadorAuto))
                {
                    $datos = $this->operadores_model->datosOperadorAuto($row->servicioIdoperadorAuto);
                    $operador = $datos[0]->OperadorNombreCompleto;
                    $sitio = $datos[0]->autosSitio." ".$datos[0]->autosNick;
                    $descripcion = $datos[0]->autosDescripcion;
                }

                $strTableH = $strTableH."<tr class='".$strclass."'>
                  <td>".$date->format('d/m/Y H:i:s')."</td>
                  <td>".$strEstatus."</td>
                  <td>".$row->nombreSolicitante."</td>
                  <td align='left'>".$row->domicilioSolicitante."</td>
                  <td>".$operador."</td>
                  <td>".$sitio."</td>";

            }
            $strTableH = $strTableH."</table>";
        }

        $str_links = $this->pagination->create_links();

         $output = array(
           'pagination_link'  => $str_links,
           'servicios_table'   => $strTableH,
           'total' => $numrows
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function activarServicioBase($idServicio)
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->get_status_servicio($idServicio);

            if ($result == TRUE)
            {
                $latitud = $result[0]->servicioLatuitudInicio;
                $longitud = $result[0]->servicioLongitudInicio;
                $result = $this->servicios_model->activar_servicio_usuario($idServicio);

                $this->notificar_operadores($latitud, $longitud, $idServicio);

                $data_codigo['return'] = 1;
                $data_codigo['codigo'] = "";

                echo json_encode($data_codigo);
            }
            else
            {
                $data_codigo['return'] = 2;
                $data_codigo['codigo'] = $ex->getMessage();

                echo json_encode($data_codigo);
            }
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();

            echo json_encode($data_codigo);
        }
    }

    public function estadistico_bases()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $_SESSION['hdOpcion'] = 5;
        if (!isset($this->session->userdata['loggedin']))
            $this->load->view('usuarios/login');
        else
            $this->load->view('servicios/estadistico_bases');
    }

    public function estadistico()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $_SESSION['hdOpcion'] = 4;
        if (!isset($this->session->userdata['loggedin']))
            $this->load->view('usuarios/login');
        else
            $this->load->view('servicios/estadistico');
    }

    public function informacion_usuario()
    {
        $this->output->unset_template();
        $usuario = $_POST['usuario'];

        $result = $this->usuarios_model->informacion_usuario($usuario);

        if ($result == false) {
            $strTableH = "<table class='table table-hover' id='tblUsuarios' name='tblUsuarios'><thead>
                    <tr>
                      <th scope='col' colspan='5' class='text-center'> NO SE ENCONTRARON USUARIOS</th>
                    </tr>
                  </thead></table>";

        }else {
             $strTableH = "<table class='table table-hover table-responsive' id='tblUsuarios' name='tblUsuarios'><thead><tr><th scope='col'>Id</th><th scope='col'>Usuario</th><th scope='col'>Descripción</th><th scope='col'>Estado</th><th scope='col'>Zona</th><th scope='col'>Seleccionar</th></tr></thead><tbody>";

             foreach($result as $row) {
                $zona = "";
                $estado = "";
                if(!is_null($row->municipio))
                {
                    $municipios = $this->usuarios_model->obtiene_zona($row->estado, $row->municipio);
                    foreach($municipios as $municipio) {
                        $zona = $zona.$municipio->municipio.", ";
                        $estado = $municipio->estado;
                    }
                }

                $strTableH = $strTableH."<tr>
                  <th scope='row'>".$row->usuarioId."</th>
                  <td>".$row->usuarioNombre."</td>
                  <td>".$row->descripcionBase."</td>
                  <td>".$estado."</td>
                  <td>".$zona."</td>
                  <td><input type='radio' name='rbseleccionar' id='rbseleccionar' onclick='selectusuario(\"".$row->usuarioId."\")'></td></tr>";
            }
            $strTableH = $strTableH."</tbody></table>";
        }

         $output = array(
           'usuarios_table'   => $strTableH
          );
          echo json_encode($output);
    }

    public function impreso()
    {

        $fechaDesde = $_POST['hdfechaDesde'];
        $fechaHasta = $_POST['hdfechaHasta'];
        $estatus = $_POST['hdestatus'];
        $idUsuario = $_POST['hdusuario'];
        $base = $_POST['hdbase'];
        //print_r($_POST);

        if($base == "1")
        {
            $data['usuario'] = $base;
        }

        $date_ini = DateTime::createFromFormat('d/m/Y', $fechaDesde);
        $date_fin = DateTime::createFromFormat('d/m/Y', $fechaHasta);
        $sfechaDesde = $date_ini->format('Y-m-d').' 00:00:00';
        $sfechaHasta = $date_fin->format('Y-m-d').' 23:59:59';

        $result= $this->servicios_model->serviciosparcialimpreso($sfechaDesde, $sfechaHasta, $estatus, $idUsuario);

        $data['servicios'] = $result;
        $data['fechaDesde'] = $fechaDesde;
        $data['fechaHasta'] = $fechaHasta;
        $data['usuario'] = $base;

        $html2pdf = new Html2Pdf('L','A4','es');

        $html = $this->load->view('servicios/impreso', $data, true);
        $html2pdf->writeHTML($html);
        ob_end_clean();
        $html2pdf->output();
    }

    public function vermapa()
    {
        $this->load->view('servicios/mapa');
    }

    //PITA
    public function mis_correos($id='')
    {
        if($this->input->post()){
            $this->form_validation->set_rules('correo1', 'correo1', 'trim|valid_email|required');
            $this->form_validation->set_rules('correo2', 'correo2', 'trim|valid_email|required');
            $this->form_validation->set_rules('correo3', 'correo3', 'trim|valid_email|required');

       if ($this->form_validation->run()){
        $datos = array('correo1' => $this->input->post('correo1'),
                        'correo2' => $this->input->post('correo2'),
                        'correo3' => $this->input->post('correo3'),
                        'idusuario'=>$this->session->userdata['loggedin']['id'],

            );

            //Saber si ya se guardó
            $existe = $this->principal->existeRegistro('correos_user',array('idusuario'=>$this->session->userdata['loggedin']['id']));

            if($existe){
                $this->principal->update('correos_user',array('idusuario'=>$this->session->userdata['loggedin']['id']),$datos);
            }else{
                $this->principal->save('correos_user',$datos);
            }

          echo 1;exit();
          }else{
             $errors = array(
                  'correo1' => form_error('correo1'),
                  'correo2' => form_error('correo2'),
                  'correo3' => form_error('correo3'),
             );
            echo json_encode($errors); exit();
          }
      }
        $id = $this->session->userdata['loggedin']['id'];
        $info= $this->principal->getById('correos_user',array('idusuario'=>$id));

        $data['input_correo1'] = form_input('correo1',set_value('correo1',exist_obj($info,'correo1')),'class="input input100" rows="5" id="correo1" placeholder="Escríbe el correo 1" ');
        $data['input_correo2'] = form_input('correo2',set_value('correo1',exist_obj($info,'correo2')),'class="input input100" rows="5" id="correo2" placeholder="Escríbe el correo 2" ');
        $data['input_correo3'] = form_input('correo3',set_value('correo3',exist_obj($info,'correo3')),'class="input input100" rows="5" id="correo3" placeholder="Escríbe el correo 3" ');

         $_SESSION['hdOpcion'] = 3;
        if (!isset($this->session->userdata['loggedin']))
            $this->load->view('usuarios/login');
         else
         {
            $this->blade->set_data($data)->render('usuarios/mis_correos');
        }
    }
    //Enviar correos
    public function sendMails(){
        $id = $this->session->userdata['loggedin']['id'];
        $correos = $this->principal->getById('correos_user',array('idusuario'=>$id));
        $vista = $this->blade->render('usuarios/correos',array(),true);
        enviar_correo($correos->correo1,'¡NECESITO TU AYUDA!',$vista);
        enviar_correo($correos->correo2,'¡NECESITO TU AYUDA!',$vista);
        enviar_correo($correos->correo3,'¡NECESITO TU AYUDA!',$vista);
    }
    //MAPA
    public function mapa(){
        $_SESSION['hdOpcion'] = 1;
        if(isset($this->session->userdata['loggedin'])){
            $this->blade->render('usuarios/mapa');
        }
        else
             $this->load->view('usuarios/login');

    }
    public function getPrecioServicio(){
        $latitud = $_POST['origin_lat'];
        $latitud_destino = $_POST['destination_lat'];
        $longitud = $_POST['origin_lng'];
        $longitud_destino = $_POST['destination_lng'];
        $distancia = $_POST['distancia'];
        $duracion = $_POST['duracion'];

       // print_r($_POST);die();
        $response = $this->getPriceAPI();

        if($response->error==1){
            echo json_encode(array('exito'=>false));
        }else{

            $result_array_precio=$response->ticket;
            $result_array_precio["exito"]=true;

            if($response->ticket["tipo_ticket"]=="Repartidores"){
                $result_array_precio["costo_viaje"]=$response->ticket['costo_viaje'];
                $result_array_precio["km_extra_repart"]=$response->ticket['km_extra_repart'];
                $result_array_precio["precio_x_km_extra"]=$response->ticket['precio_x_km_extra'];
                $result_array_precio["costo_x_kms_extra"]=$response->ticket['costo_x_kms_extra'];
            }
             echo json_encode($result_array_precio);
        }
        //exit();

    }//...getPRecioServicio


    public function detalleViaje(){

        $data['datos'] = $_POST;
        echo $this->blade->render('usuarios/detalle_viaje',$data);
        exit();
    }

    public function detalleViaje_tel_contacto(){

        $data['datos'] = $_POST;
        echo $this->blade->render('usuarios/detalle_viaje_tel_contacto',$data);
        exit();
    }

    public function ticket(){

        $data['datos'] = $_POST;
        echo $this->blade->render('usuarios/detalle_viaje_ticket',$data);
        exit();
    }

    public function guardarViaje(){
        //print_r($_POST);die();
        $latitud = $_POST['origin_lat'];
        $latitud_destino = $_POST['destination_lat'];
        $longitud = $_POST['origin_lng'];
        $longitud_destino = $_POST['destination_lng'];
        $comentario = $_POST['observaciones'];
        $distancia = $_POST['distancia'];
        $duracion = $_POST['duracion'];
        $tel_contacto = $_POST['tel_contacto_modal'];

        $response = $this->getPriceAPI();

        $precio = $response->ticket['tarifamin'];


        /*$this->session['tripdata']["origin"] = array(
            'temp_serv_idUsuario'   =>
            'temp_serv_tipo' =>
            'temp_serv_lat' =>
            'temp_serv_lng' =>
            'temp_serv_address' =>
            'temp_serv_calle_numero' =>
            'temp_serv_place_id' =>
        ); */

        $Origin=$this->session->tripdata['origin'];
        $Destination=$this->session->tripdata['destination'];

        $Precio_min=$this->session->ticket['tarifamin'];
        $Precio_max=$this->session->ticket['tarifamax'];
        $Distancia_aprox=$this->session->ticket['km'];
        $Duracion_aprox=$this->session->ticket['min'];

        $array_servicios = array (
          'servicioIdUsuario' =>$this->session->userdata['loggedin']['id'],
          'servicioStatus' =>1,
          'servicioTelefonoContacto' =>$tel_contacto,
          'servicioFechaCreacion' => date('Y-m-d H:i:s'),
          'servicioLatuitudInicio' =>$Origin["temp_serv_lat"],
          'servicioLongitudInicio' =>$Origin["temp_serv_lng"],
          'servicioLatitudFin' =>$Destination["temp_serv_lat"],
          'servicioLongitudFin' =>$Destination["temp_serv_lng"],
          'servicioOrigenText' => (isset($Origin["temp_serv_address"]))?$Origin["temp_serv_address"]:$Origin["temp_serv_calle_numero"],
          'servicioDestinoText' => $Destination["temp_serv_place_name"],
          'servicioOrigenCalleNumero' => $Origin["temp_serv_calle_numero"],
          'servicioDestinoCalleNumero' => $Destination["temp_serv_calle_numero"],
          'servicioOrigenPlaceId' => $Origin["temp_serv_place_id"],
          'servicioDestinoPlaceId' => $Destination["temp_serv_place_id"],
          'servicioComentario' =>'',
          'servicioFechaFinalizacion' => null,
          'OneSignalId' => '',
          'rating' => 0,
          'ratingComentario' => '',
          'nombreSolicitante' => $this->session->userdata['loggedin']['usuario'],
          'celularSolicitante' => '',
          'observacionesSolicitante' => $comentario,
          'domicilioSolicitante' =>'',
          'base' =>0,
          'tipo_pago' =>0,
          'tipo_auto'=>$this->session->userdata['tipo_servicio']['tipo_servicio'],//tipo_de_servicio
          'precio'=> 0,
          'precio_min_aprox' =>$Precio_min,
          'precio_max_aprox' =>$Precio_max,
          'distancia_aprox' =>$Distancia_aprox,
          'duracion_aprox' =>$Duracion_aprox
        );
        $this->db->insert('servicios',$array_servicios);
        $id = $this->db->insert_id();

/*         $array_pedido_kekomo= array(
            'id_ped_kekomo' =>$Precio_min,
        );

        $this->db->insert('pedido',$array_pedido_kekomo); */


        /* $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://jaimee.com.mx/central_admin/index.php/api/servicio_enviado/".$id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cookie: ci_session=f28ab676da5a297f1f96a99bb854c228c632cf41"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl); */

        $this->session->set_userdata('idservicio',$id);

        echo 1;exit();
    }//...guardarViaje

    function getPriceAPI(){
        $this->output->unset_template();
        /* $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://grupoave.net/panel/index.php/api_operador/calcula_tarifa",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "latitud=".$latitud."&longitud=".$longitud."&latitud_destino=".$latitud_destino."&longitud_destino=".$longitud_destino,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded"
          ),
        ));
        $response = json_decode(curl_exec($curl));
        curl_close($curl); */

				/*         $KmViaje=floatval(
                        str_replace(" km", "", $_SESSION['info_trayecto']['distancia']
                        )
                    );
        $TiempoViaje=floatval(
                            str_replace(" min", "", $_SESSION['info_trayecto']['duracion']
                            )
                        ); */
        $KmViaje=floatval(
                    str_replace(",", ".",
                        str_replace(" km", "", $this->session->userdata['tripdata']['info_trayecto']['distancia']
                        )
                    )
                );
        $TiempoViaje=floatval(
                        str_replace(" min", "", $this->session->userdata['tripdata']['info_trayecto']['duracion']
                        )
                    );


        $hoy=date("N");                                             //week day (monday=1 ,sunday=7)
        $hora=date("H:i:s");
        $tipoServicio=$this->session->userdata['tipo_servicio']['tipo_servicio'];
        $tipoVehiculo=$this->session->userdata['tipo_servicio']['tipo_vehiculo'];

        //establece tipo de ticket de acuerdo al servicio, (flag)
        $txt_serv_vehic= $this->servicios_model->get_txt_serv_vehic($tipoServicio,$tipoVehiculo);
        $tipo_ticket_text       =$txt_serv_vehic->tipo_servicio_text;
        $tipo_auto_text         =$txt_serv_vehic->tipo_vehiculo_text;
        
        $restaurante_place_id   =$this->session->userdata['tripdata']['destination']['temp_serv_place_id'];
        if(isset($restaurante_place_id) && $restaurante_place_id != ""){
            $row_restaurante        = $this->Restaurantes_model->get_restaurante_by_place_id($restaurante_place_id);
            $cuota_restaurante      = round($row_restaurante->cuota_restaurante,3,PHP_ROUND_HALF_EVEN);
        }else{
            $cuota_resturante="";
        }
        
        if($tipoServicio=='1' || $tipoServicio=='2'){

            //iniciar variables repartidor
            $costo_viaje=0;
            $exceso_km=0;
            $precio_km_extra=0;
            $costo_extra_viaje=0;

            $parametros= $this->servicios_model->get_parametros($tipoServicio,$tipoVehiculo);      //model get (radartax_central.parametros)

            $porc_movilidad         =round($parametros->porcentaje_movilidad/100,3,PHP_ROUND_HALF_EVEN);
            $porc_IVA               =round($parametros->porcentaje_IVA/100,3,PHP_ROUND_HALF_EVEN);
            $porc_ISR               =round($parametros->porcentaje_ISR/100,3,PHP_ROUND_HALF_EVEN);
            $porc_solicitud         =round($parametros->porcentaje_solicitud/100,3,PHP_ROUND_HALF_EVEN);
            $porc_prec_max          =round($parametros->porcentaje_precio_maximo/100,3,PHP_ROUND_HALF_EVEN);
            $porc_SUV               =round($parametros->porcentaje_SUV/100,3,PHP_ROUND_HALF_EVEN);
            $precio_max_solicitud   =$parametros->precio_max_solicitud;

            $tarifabase=$parametros->tarifa_base;                       //$parametros.tarifa_base

            $tarifadinamica= $this->servicios_model->get_tarifa_dinamica($hoy,$hora); //model get (radartax_central.tarifadinamica join radartax_central.tasa)

            $tasa=$tarifadinamica->tarifa;                 //$tarifadinamica.tarifa (%)

            $tarifa_minima=$parametros->tarifa_minima_servicio; //$parametros.tarifa_minima_servicio ($)

            //Taxi Privado
            if($tipoServicio=='1'){
                $tarifa=     $this->servicios_model->get_tarifa($KmViaje,"kilometros","");  //model get (radartax_central.kilometros)
                $tarifaKM=$tarifa->precio_x_km;                                             //$tarifa.precio_x_kkm
                $tarifaMIN=$tarifa->precio_x_minuto;                                        //$tarifa.precio_x_minuto

                $total_precio_km    = $KmViaje*$tarifaKM;
                $total_precio_min   = $TiempoViaje*$tarifaMIN;
            }

            //Repartidor
            if($tipoServicio=='2'){
                $tarifa=     $this->servicios_model->get_tarifa($KmViaje,"kilometros_repartidores",$tipoVehiculo);  //model get (radartax_central.kilometros_repartidores)

                $tasa=1;                               //tarifa dinámica
                $costo_viaje=$tarifa->precio_viaje;   //$tarifa.precio_x_kkm

                $tarifaKM=$costo_viaje;

                $total_precio_km    = $tarifaKM;
                $total_precio_min   = 0;

                $limite_km_repartidor   = $tarifa->a_km;

                //si la distancia supera el limite de kilometros de la tarifa normal
                if($KmViaje>$limite_km_repartidor){

                    $exceso_km=$KmViaje - $limite_km_repartidor;
                    $precio_km_extra    = $parametros->precio_km_extra;

                    $costo_extra_viaje  = $exceso_km * $precio_km_extra;

                    $total_precio_km    = $tarifaKM + $costo_extra_viaje;
                    $total_precio_min   = 0;
                }
            }


            //CÁlCULO
                $subtotal1     = ($total_precio_km)    +   ($total_precio_min)   +   $tarifabase;

                $tasadinamica  = $subtotal1*$tasa;//tasa de tarifa dinamica

                $subtotal2     = $tasadinamica;
                $primerprecio  = $subtotal2;                   //subtotal antes de  parametros

                $costo_SUV = $subtotal2*($porc_SUV);

                $subtotal2 += $costo_SUV;           //añadir porcentaje si es camioneta SUV

                //$costo_solicitud    = $subtotal2*($porc_solicitud);
                $costo_solicitud=$cuota_restaurante;
                if($costo_solicitud>$precio_max_solicitud){$costo_solicitud=$precio_max_solicitud;}

                $subtotal2_con_solicitud = $subtotal2+$costo_solicitud;

                $subtotal3=$subtotal2_con_solicitud;

                if($subtotal3 < $tarifa_minima){$subtotal2=$tarifa_minima;}

                $costo_movilidad  = $subtotal3*($porc_movilidad);
                $costo_IVA        = $subtotal3*($porc_IVA);
                $costo_ISR        = $subtotal3*($porc_ISR);

                $subtotal3 += $costo_IVA;
                $subtotal3 += $costo_ISR;
                $subtotal3 += $costo_movilidad;

                $tarifamin = $subtotal3;
                $tarifamax = $tarifamin+$tarifamin*($porc_prec_max);

                //ticket array (cantidades)
                $ticket=array(
                    'fecha'=>$hoy,
                    'hora'=>$hora,
                    'tipo_ticket'=>$tipo_ticket_text,
                    'tipo_vehiculo'=>$tipo_auto_text,
                    'costo_viaje'=>$costo_viaje,
                    'km_extra_repart'=>round($exceso_km,2,PHP_ROUND_HALF_EVEN),
                    'precio_x_km_extra'=>$precio_km_extra,
                    'costo_x_kms_extra'=>round($costo_extra_viaje,2,PHP_ROUND_HALF_EVEN),
                    'km'=>$KmViaje,
                    'min'=>$TiempoViaje,
                    'total_precio_km'=>round($total_precio_km,2,PHP_ROUND_HALF_EVEN),
                    'total_precio_min'=>round($total_precio_min,2,PHP_ROUND_HALF_EVEN),
                    'tarifabase'=>round($tarifabase,2,PHP_ROUND_HALF_EVEN),
                    'tasadinamica'=>round($tasadinamica,2,PHP_ROUND_HALF_EVEN),
                    'tasadinamica'=>$tasa,
                    'subtotal1'=>round($subtotal1,2,PHP_ROUND_HALF_EVEN),
                    'subtotal2'=>round($subtotal2,2,PHP_ROUND_HALF_EVEN),
                    'subtotal2_con_solicitud'=>round($subtotal2_con_solicitud,2,PHP_ROUND_HALF_EVEN),
                    'subtotal3'=>round($subtotal3,2,PHP_ROUND_HALF_EVEN),
                    'primerprecio'=>round($primerprecio,2,PHP_ROUND_HALF_EVEN),
                    'costo_SUV'=>$costo_SUV,
                    'tarifa_minima'=>round($tarifa_minima,2,PHP_ROUND_HALF_EVEN),
                    'costo_movilidad'=>round($costo_movilidad,2,PHP_ROUND_HALF_EVEN),
                    'costo_IVA'=>round($costo_IVA,2,PHP_ROUND_HALF_EVEN),
                    'costo_ISR'=>round($costo_ISR,2,PHP_ROUND_HALF_EVEN),
                    'costo_solicitud'=>round($costo_solicitud,2,PHP_ROUND_HALF_EVEN),
                    'tarifamin'=>round($tarifamin,2,PHP_ROUND_HALF_EVEN),
                    'tarifamax'=>round($tarifamax,2,PHP_ROUND_HALF_EVEN),
                    'authTicket'=>false
                );

                //respaldo ticket array (porcentajes)
                /* $ticket=array(
                    'fecha'=>$hoy,
                    'hora'=>$hora,
                    'tipo_ticket'=>$tipo_ticket_text,
                    'tipo_vehiculo'=>$tipo_auto_text,
                    'km_extra_repart'=>$exceso_km,
                    'precio_x_km_extra'=>$precio_km_extra,
                    'costo_x_kms_extra'=>$costo_extra_viaje,
                    'km'=>$KmViaje,
                    'min'=>$TiempoViaje,
                    'total_precio_km'=>round($total_precio_km,2,PHP_ROUND_HALF_EVEN),
                    'total_precio_min'=>round($total_precio_min,2,PHP_ROUND_HALF_EVEN),
                    'tarifabase'=>round($tarifabase,2,PHP_ROUND_HALF_EVEN),
                    'tasadinamica'=>round($tasadinamica,2,PHP_ROUND_HALF_EVEN),
                    'subtotal1'=>round($subtotal1,2,PHP_ROUND_HALF_EVEN),
                    'subtotal2'=>round($subtotal2,2,PHP_ROUND_HALF_EVEN),
                    'subtotal2_con_solicitud'=>round($subtotal2_con_solicitud,2,PHP_ROUND_HALF_EVEN),
                    'subtotal3'=>round($subtotal3,2,PHP_ROUND_HALF_EVEN),
                    'primerprecio'=>round($primerprecio,2,PHP_ROUND_HALF_EVEN),
                    'costo_SUV'=>$porc_SUV,
                    'tarifa_minima'=>round($tarifa_minima,2,PHP_ROUND_HALF_EVEN),
                    'costo_movilidad'=>round($porc_movilidad,2,PHP_ROUND_HALF_EVEN),
                    'costo_IVA'=>round($porc_IVA,2,PHP_ROUND_HALF_EVEN),
                    'costo_ISR'=>round($porc_ISR,2,PHP_ROUND_HALF_EVEN),
                    'costo_solicitud'=>round($porc_solicitud,2,PHP_ROUND_HALF_EVEN),
                    'tarifamin'=>round($tarifamin,2,PHP_ROUND_HALF_EVEN),
                    'tarifamax'=>round($tarifamax,2,PHP_ROUND_HALF_EVEN),
                    'authTicket'=>false
                ); */
        }
        //Carga
        else{//if(!$tipoServicio=='1' || $tipoServicio=='2')
            /*si el tipo de servicio es carga los precios se setean a 0,
             no se realiza el calculo ni la consulta a parámetros y tarifamin = "Por cotizar"

            */
            $ticket=array(
                'fecha'=>$hoy,
                'hora'=>$hora,
                'tipo_ticket'=>$tipo_ticket_text,
                'tipo_vehiculo'=>$tipo_auto_text,
                'costo_viaje'=>0,
                'km_extra_repart'=>0,
                'precio_x_km_extra'=>0,
                'costo_x_kms_extra'=>0,
                'km'=>$KmViaje,
                'min'=>0,
                'total_precio_km'=>0,
                'total_precio_min'=>0,
                'tarifabase'=>0,
                'tasadinamica'=>0,
                'tasadinamica'=>0,
                'subtotal1'=>0,
                'subtotal2'=>0,
                'subtotal2_con_solicitud'=>0,
                'subtotal3'=>0,
                'primerprecio'=>0,
                'costo_SUV'=>0,
                'tarifa_minima'=>0,
                'costo_movilidad'=>0,
                'costo_IVA'=>0,
                'costo_ISR'=>0,
                'costo_solicitud'=>0,
                'tarifamin'=>0,
                'tarifamax'=>0,
                'authTicket'=>false
            );
        }
        //id's de usuarios habilitados para visualizar el botón "ver ticket en detalle viaje"
            $usuarioVerTicket=array(6,7,9,10);
            if(in_array($this->session->userdata['loggedin']['id'],$usuarioVerTicket)){
               $ticket['authTicket']=true;
            }
        $response = new stdClass;

        $response->ticket=$ticket;
        $response->error=0;
        $this->session->userdata['ticket']=$ticket;
        return $response;
    }//...getPriceApi


    //Función para la espera de chofer
    public function espera_chofer(){
        $data['datos_chofer'] = $this->usuarios_model->getDatosChofer();
        $data['existe_chofer'] = false;
        $data['url_gps'] = '';
        $data['total_servicio'] = '';
        $data['info_share'] = '';
        if(isset($data['datos_chofer'])){
            if($data['datos_chofer']->operadorId!=''){
                $data['existe_chofer'] = true;

                $data['info_share'] = 'Hola soy '.$this->session->userdata['loggedin']['usuario'].', quiero compartir los datos de mi viaje: Nombre Chofer: '.$data['datos_chofer']->operadorNombreCompleto.', teléfono de chofer: '.$data['datos_chofer']->operadorTelefono;
                $info_auto = $this->usuarios_model->getInfoAuto();
                if($info_auto!=''){
                    $data['url_gps'] = $info_auto->url_gps;
                    $data['total_servicio'] =  number_format(($info_auto->precio_min + $info_auto->precio_max) / 2, 2, '.', '');
                }

            }
        }
        //debug_var($data);die();
        $this->blade->render('usuarios/v_espera_chofer',$data);
    }
    //Validar si ya existe el chofer
    public function validarChofer(){
        $datos_chofer = $this->usuarios_model->getDatosChofer();
        $url_gps = '';
        $total_servicio = '';
        //1.-pendiente 3.-cancelado 2.-aceptado 4.-rechazado 5.-notificado
        if($datos_chofer->servicioStatus==2 || $datos_chofer->servicioStatus==4 || $datos_chofer->servicioStatus==3){
             $this->session->set_userdata('tipo_auto','');//tipo_de_servicio
        }
        if($datos_chofer->servicioStatus==2){
            $info_auto = $this->usuarios_model->getInfoAuto();
            $url_gps = $info_auto->url_gps;
            $total_servicio =  round(($info_auto->precio_min + $info_auto->precio_max) / 2, 0, PHP_ROUND_HALF_UP);
        }
        echo json_encode(array('chofer'=>$datos_chofer,'estatus'=>$datos_chofer->servicioStatus,'url_gps'=>$url_gps,'total_servicio'=>$total_servicio));
        exit();
    }
    //Información de la camioneta
    public function tipo_auto(){
        $this->session->set_userdata('tipo_auto',$this->input->post('tipo_auto'));//tipo_de_servicio
        echo 1;exit();
        exit();
    }
    //Información de la camioneta
    public function info_camioneta(){
        $this->session->set_userdata('tipo_auto',$this->input->post('tipo_auto'));
        echo $this->blade->render('usuarios/info_camioneta');
        exit();
    }
    public function mimapa(){
        $this->blade->render('usuarios/mi_mapa');
    }
    public function redes(){
        $this->blade->render('usuarios/redes');
    }
		// public function confirmar_servicio(){
		// $this->output->set_template('template_confirmacion');
		//
	  //   $this->blade->render('chofer/confirmacion');
	  // }
		public function solicitud_viaje($idServicio, $idOperador)
		{
				$template = 'chofer/confirmacion';
				$this->output->set_template('template_confirmacion');

				$data = array(
						'servicio' => $this->servicios_model->get_servicio($idServicio),
						'operador' => $this->operadores_model->get_operador($idOperador)
				);

				if($data['servicio'] == null || $data['operador'] == null){
					$data['servicio'] = array(false);
					$data['operador'] = array(false);

					$this->session->set_userdata($data);

				}else {

					$this->session->set_userdata($data);
					if($data['servicio'][0]->servicioStatus == 2){
							$template = 'chofer/panel_principal';
					}
				}
				$this->blade->render($template);
				// $this->load->view('chofer/solicitud_viaje');
		}

		public function panel_chofer(){
				$this->output->set_template('template_confirmacion');
				$this->blade->render('chofer/panel_principal');
		}

    public function kekomo(){
			$this->output->set_template('main_template');
			$this->blade->render('usuarios/inicio');
    }
    
    public function update_session_servicio_kekomo($value){
        $this->output->unset_template();
       
        echo $this->session->userdata['tipo_servicio']['servicio_kekomo']=$value;
    }

    public function agregar_restaurante(){
        $this->output->unset_template();
        $data=$this->input->post();
        //echo print_r($this->input->post());
        echo $this->Principal->save("restaurantes",$data);
    }

    public function obtenerRestaurantes($lat_cliente,$lng_cliente,$radio_km){
        $this->output->unset_template();
        $lat_cliente  = $this->session->tripdata['origin']['temp_serv_lat'];
        $lng_cliente  = $this->session->tripdata['origin']['temp_serv_lng'];

        echo json_encode($this->Restaurantes_model->obtenerRestaurantes($lat_cliente,$lng_cliente,$radio_km));
    }

    public function modal_add_restaurante(){
        //$this->output->unset_template();
        $data['restaurante'] = $_POST;
        echo $this->blade->render('usuarios/agregar_restaurante',$data);
        exit();
    }
    private function _get_browser_name(){
        // return $_SERVER['HTTP_USER_AGENT'];


        // Make case insensitive.
        $t = strtolower($_SERVER['HTTP_USER_AGENT']);

        // If the string *starts* with the string, strpos returns 0 (i.e., FALSE). Do a ghetto hack and start with a space.
        // "[strpos()] may return Boolean FALSE, but may also return a non-Boolean value which evaluates to FALSE."
        //    http://php.net/manual/en/function.strpos.php
        $t = " " . $t;

        // Humans / Regular Users      
        if     (strpos($t, 'opera'     ) || strpos($t, 'opr/')     ) return 'Opera'            ;
        elseif (strpos($t, 'edge'      )                           ) return 'Edge'             ;
        elseif (strpos($t, 'firefox'   )                           ) return 'Firefox'          ;        
        elseif (strpos($t, 'chrome'    )                           ) return 'Chrome'           ;
        elseif (strpos($t, 'safari'    )                           ) return 'Safari'           ;        
        elseif (strpos($t, 'msie'      ) || strpos($t, 'trident/7')) return 'Internet Explorer';
        else    'Error';
    }

    

}
?>
