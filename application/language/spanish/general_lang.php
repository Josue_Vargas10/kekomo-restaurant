
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['language'] = 'english';
$lang['current_lang'] = 'spanish';
$lang['main_label'] = 'Tu servicio será:';
$lang['button_1'] = 'En el restaurante';
$lang['button_2'] = 'A domicilio';
$lang['button_3'] = 'Por recoger';
$lang['button_4'] = 'Servicio en tu auto';

// Scanner QR in restaurant
$lang['label_1'] = "Escribe el código de tu mesa:";
$lang['label_2'] = "Ó";
$lang['link_1'] = "Escanea el código QR";
$lang['footer'] = "*Este código será proporcionado por el mesero.";

//ASIDE MENU
$lang['about'] = 'Sobre KeKomo';
$lang['billing'] = 'Facturación';
$lang['partners'] = 'Socios';
$lang['register'] = 'Afiliate';
$lang['home'] = 'Inicio';

// MODAL RESTAURANT
$lang['modal_title'] = 'Hubo un error en tu solicitud.';
$lang['modal_text'] = 'Código incorrecto, por favor verificalo bien.';
$lang['modal_text2'] = 'Recurso no encontrado, verifica tu código.';
