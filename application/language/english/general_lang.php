<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['language']= 'español';
$lang['current_lang'] = 'english';
$lang['main_label'] = 'Your service will be:';
$lang['button_1'] = 'Restaurant';
$lang['button_2'] = 'Delivery';
$lang['button_3'] = 'Pick&Go';
$lang['button_4'] = 'Drive thru';


// Scanner QR in restaurant
$lang['label_1'] = "Type your table QR code:";
$lang['label_2'] = "Or";
$lang['link_1'] = "Scan your QR code";
$lang['footer'] = "*This code will be provided by the waiter.";

//ASIDE MENU
$lang['about'] = 'About KeKomo';
$lang['billing'] = 'Billing';
$lang['partners'] = 'Partners';
$lang['register'] = 'Join us';
$lang['home'] = 'Home';

// MODAL RESTAURANT
$lang['modal_title'] = 'There was a problem...';
$lang['modal_text'] = 'Wrong code, please verified it.';
$lang['modal_text2'] = 'Resource not found, verify your code.';