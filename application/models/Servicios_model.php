<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Servicios_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	function insert_servicio($post_data) {
	    $this->db->insert('servicios',$post_data);
	    return $this->db->insert_id();
	}

	function grabar_comentario($post_data) {
	    $this->db->insert('buzon_usuario',$post_data);
	    return $this->db->insert_id();
	}

	function operador_servicio($post_data) {
		$existe = $this->get_operador_servicio($post_data);

		if($existe)
		{
			$updateData=array("estatus"=>$post_data['estatus']);
			$condition = "idServicio = '".$post_data['idServicio']."' AND idOperador = '".$post_data['idOperador']."'";
			$this->db->where($condition);
			$this->db->update("operador_servicios",$updateData);
		}
		else
		{
		    $this->db->insert('operador_servicios',$post_data);
		    return $this->db->insert_id();
		}
	}

	function get_operador_servicio($post_data)
	{
		$condition = "idServicio = '".$post_data['idServicio']."' AND idOperador = '".$post_data['idOperador']."'";
		$this->db->select('*');
		$this->db->from('operador_servicios');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows()>0) {
			return true;
		} else {
			return false;
		}
	}

	function get_servicio($id){

		$status_array = [1,2];
		$this->db->select('*');
		$this->db->from('servicios');
		$this->db->where('servicioId', $id);
		$this->db->where_in('servicioStatus', $status_array);
		// $this->db->where("(status=1 OR status=2 )");
		$this->db->limit(1);
		$query = $this->db->get();
		//echo $condition;die();

		if ($query->num_rows() == 1) {
			return $query->result();
		}
	}


	function get_servicios_pendientes()
	{
		$condition = "servicioStatus = 1";
		$this->db->select('*');
		$this->db->from('servicios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		//echo $condition;die();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

	function get_servicio_usuario($idusuario)
	{
		$condition = "servicioIdUsuario = '" . $idusuario . "' AND servicioStatus in (1,5)";
		$this->db->select('*');
		$this->db->from('servicios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		//echo $condition;die();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	function get_infoservicio_usuario($idusuario)
	{
		$condition = "servicioFechaFinalizacion is null and servicioIdUsuario = '"
						. $idusuario .
						"' AND servicioStatus in (1,5)";
		$this->db->select('*');
		$this->db->from('servicios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 1)
			return $query->result();
	}


	function update_servicio_usuario($idServicio, $estatus, $idOperador = null)
	{

		if ($idServicio == 0)
		{
			$condition = "servicioIdUsuario = '".$this->session->userdata['loggedin']['id']."' and servicioStatus = 1";
		}else
		{
			$condition = "servicioId = '".$idServicio."'";
		}

		if(!is_null($idOperador))
			$updateData=array("servicioStatus"=> $estatus, "servicioIdoperadorAuto" => $idOperador);
		else
			$updateData=array("servicioStatus"=> $estatus);

		$this->db->where($condition);
		$this->db->update("servicios",$updateData);

	}

	function finalizar_servicio($data){
		extract($data);

		$this->db->set('servicioFechaFinalizacion', $servicioFechaFinalizacion);
		$this->db->set('servicioStatus', $status);
		$this->db->where('servicioId', $id);
	  $this->db->update($table_name);
	  return true;
	}

	function iniciar_servicio($data){
		extract($data);
		// $this->db->set('servicioFechaFinalizacion', $servicioFechaFinalizacion);
		$this->db->set('servicioStatus', $status);
		$this->db->where('servicioId', $id);
	  $this->db->update($table_name);
	  return true;
	}

	function aceptar_servicio($data){
		extract($data);

		$this->db->set('servicioIdoperadorAuto', $idOperador);
		$this->db->set('servicioStatus', $status);
		$this->db->where('servicioId', $id);
	  $this->db->update($table_name);
	  return true;
	}

	function rechazar_servicio($data){
		extract($data);

		$this->db->set('servicioIdoperadorAuto', $idOperador);
		$this->db->set('servicioStatus', $status);
		$this->db->where('servicioId', $id);
		$this->db->update($table_name);
		return true;
	}

	function activar_servicio_usuario($idServicio)
	{
		$updateData=array("servicioStatus"=>"1", "servicioIdoperadorAuto" => null);
		$condition = "servicioId = '".$idServicio."'";
		$this->db->where($condition);
		$this->db->update("servicios",$updateData);
	}

	function get_todos_servicios($idusuario)
	{
		$this->db->select('servicios.servicioId, servicios.servicioStatus, servicios.servicioFechaCreacion, operador_autos.OPIdOperador, operador_autos.OPIdAuto, operador.OperadorNombreCompleto, operador.OperadorImagen, autos.autosPlacas, autos.autosDescripcion, autos.autosImagen, servicios.rating, operador.operadorTelefono, autos.autosColor, autos.autosSitio, autos.autosNick, operador.OperadorImagen2, autos.autosTipo');
	    $this->db->from('servicios');
	    $this->db->join('operador_autos', 'operador_autos.OPIdOperador = servicios.servicioIdoperadorAuto');
	    $this->db->join('operador', 'operador.operadorId = operador_autos.OPIdOperador');
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto');
	    $this->db->where("servicios.servicioStatus = 2 and servicioIdUsuario = '" . $idusuario . "'");
		$this->db->order_by('servicioFechaCreacion', 'DESC');
		$this->db->limit(10, 0);
	    $query1 = $this->db->get();


	    return $query1->result();

	}
	function get_servicios_frecuentes_orig($idusuario,$groupbycampo)
	{
		$this->db->select('servicios.*');
		$this->db->from('servicios');		
		$this->db->where("servicioStatus = 1 and servicioIdUsuario = '" . $idusuario . "'");
		$this->db->group_by($groupbycampo);
		$this->db->order_by('servicioFechaCreacion', 'DESC');
		$this->db->limit(10, 0);
	    $query1 = $this->db->get();


	    return $query1->result();

	}
	function get_servicios_frecuentes_dest($idusuario,$groupbycampo)
	{
		$this->db->select('servicios.*, restaurantes.restaurantes_id, restaurantes.restaurantes_url, restaurantes.widgetId, restaurantes.restaurantes_palabras_clave,
		restaurantes.sipId,restaurantes.domElement');
		$this->db->from('servicios');
		$this->db->join("restaurantes","servicios.servicioDestinoPlaceId = restaurantes.servicioDestinoPlaceId");
		$this->db->where("servicioStatus = 1 and servicioIdUsuario = '" . $idusuario . "'");
		$this->db->group_by($groupbycampo);
		$this->db->order_by('servicioFechaCreacion', 'DESC');
		$this->db->limit(10, 0);
	    $query1 = $this->db->get();


	    return $query1->result();

	}
	public function record_count($idusuario) {
        $this->db->select('servicios.servicioId, servicios.servicioStatus, servicios.servicioFechaCreacion, operador_autos.OPIdOperador, operador_autos.OPIdAuto, operador.OperadorNombreCompleto, operador.OperadorImagen, autos.autosPlacas, autos.autosDescripcion, autos.autosImagen');
	    $this->db->from('servicios');
	    $this->db->join('operador_autos', 'operador_autos.OPid = servicios.servicioIdoperadorAuto');
	    $this->db->join('operador', 'operador.operadorId = operador_autos.OPIdOperador');
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto');
	    $this->db->where("servicios.servicioStatus = 2 and servicioIdUsuario = '" . $idusuario . "'");
	    $query = $this->db->get();

	    return $query->num_rows();

    }

    public function fetch_servicios($limit, $start, $idusuario) {
        $this->db->select('servicios.servicioId, servicios.servicioStatus, servicios.servicioFechaCreacion, operador_autos.OPIdOperador, operador_autos.OPIdAuto, operador.OperadorNombreCompleto, operador.OperadorImagen, autos.autosPlacas, autos.autosDescripcion, autos.autosImagen');
	    $this->db->from('servicios');
	    $this->db->join('operador_autos', 'operador_autos.OPid = servicios.servicioIdoperadorAuto');
	    $this->db->join('operador', 'operador.operadorId = operador_autos.OPIdOperador');
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto');
	    $this->db->where("servicios.servicioStatus = 2 and servicioIdUsuario = '" . $idusuario . "'");
		$this->db->order_by('servicioFechaCreacion', 'DESC');
		$this->db->limit($limit, $start);
	    $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

	function get_operadores_activos()
	{
		$this->db->select('operador_autos.OPLatitud, operador_autos.OPLongitud, operador_autos.OPIdOperador, operador_autos.OPIdAuto, operador.OperadorNombreCompleto, operador.OperadorImagen, operador.operadorTelefono, autos.autosPlacas, autos.autosDescripcion, autos.autosImagen, autos.autosSitio, autos.autosNick, autos.autosColor, operador.OperadorImagen2, autos.autosTipo');
	    $this->db->from('operador_autos');
	    $this->db->join('operador', 'operador.operadorId = operador_autos.OPIdOperador');
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto');
	    $this->db->where('operador_autos.OPStatust = 1 and operador_autos.OPLatitud != ""');
	    $query1 = $this->db->get();

	    return $query1->result();
	}

	function get_operadores_activos_no_notificados($idServicio, $libres)
	{
		$this->db->select('operador_autos.OPLatitud, operador_autos.OPLongitud, operador_autos.OPIdOperador, operador_autos.OPIdAuto, operador.OperadorNombreCompleto, operador.OperadorImagen, operador.operadorTelefono, autos.autosPlacas, autos.autosDescripcion, autos.autosImagen, autos.autosSitio, autos.autosNick, autos.autosColor, operador.OperadorImagen2, autos.autosTipo');
	    $this->db->from('operador_autos');
	    $this->db->join('operador', 'operador.operadorId = operador_autos.OPIdOperador');
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto');

	    $this->db->where('operador_autos.OPStatust = '.$libres.' and operador_autos.OPLatitud != "" and operador_autos.OPIdOperador not in (select idOperador from operador_servicios where idServicio = '.$idServicio.' and estatus <> "6")');

	    $query1 = $this->db->get();

	    return $query1->result();
	}

	function get_status_servicio2($idServicio){

		$this->db->select('servicios.servicioId, servicios.servicioStatus, servicios.servicioFechaCreacion, operador_autos.OPIdOperador, operador_autos.OPIdAuto, operador.OperadorNombreCompleto, operador.OperadorImagen, autos.autosPlacas, autos.autosDescripcion, autos.autosImagen');
	    $this->db->from('servicios');
	    $this->db->join('operador_autos', 'operador_autos.OPid = servicios.servicioIdoperadorAuto');
	    $this->db->join('operador', 'operador.operadorId = operador_autos.OPIdOperador');
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto');
	    $this->db->where("servicios.servicioStatus in (2,3,4) and servicios.servicioId = '".$idServicio."'");
	    $query1 = $this->db->get();

	    if ($query1->num_rows() != 0) {
			return $query1->result();
		} else {
			return false;
		}
	}

	function get_status_servicio($idServicio){

		$this->db->select('*');
	    $this->db->from('servicios');
	    $this->db->where("servicios.servicioId = '".$idServicio."'");
	    $query1 = $this->db->get();

	    if ($query1->num_rows() != 0) {
			return $query1->result();
		} else {
			return false;
		}
	}

	function update_rating($data)
	{
		$updateData=array("rating"=>$data['rating'], "ratingComentario"=>$data['comentario']);
		$condition = "servicioId = ".$data['idServicio'];
		$this->db->where($condition);
		$this->db->update("servicios",$updateData);
	}

	function cancelar_servicios_pendientes()
	{
		/*$condition = "servicioStatus = 1 and servicioFechaCreacion < (CURRENT_TIMESTAMP - INTERVAL 5 MINUTE)";
		$this->db->select('*');
		$this->db->from('servicios');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows()>0) {
			return $query->result();
		} else {
			return false;
		}*/

		$updateData=array("servicioStatus"=>3);

		$condition = "servicioStatus = 1 and servicioFechaCreacion < (CURRENT_TIMESTAMP - INTERVAL 5 MINUTE)";
		$this->db->where($condition);
		$this->db->update("servicios",$updateData);

		/*$post_data = array('texto' => '2');
		$this->db->insert('pruebas',$post_data);
	    return $this->db->insert_id();*/
	}

	function liberar_servicios()
	{
		$condition = "servicioStatus = 5 and servicioFechaCreacion < (CURRENT_TIMESTAMP - INTERVAL 1 MINUTE)";
		$this->db->select('*');
		$this->db->from('servicios');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows()>0) {
			return $query->result();
		} else {
			return false;
		}

		/*$updateData=array("servicioStatus"=>1);

		$condition = "servicioStatus = 5 and servicioFechaCreacion < (CURRENT_TIMESTAMP - INTERVAL 1 MINUTE)";
		$this->db->where($condition);
		$this->db->update("servicios",$updateData); */
	}

	function cat_estados()
	{
		$this->db->select('*');
	    $this->db->from('c_estado');
	    $query = $this->db->get();

	    if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function cat_municipios($entidad)
	{
		$municipios = $this->session->userdata['loggedin']['municipio'];

		$this->db->select('*');
	    $this->db->from('c_municipio');
	    $this->db->where("c_estado = '".$entidad."' and c_municipio in(".$municipios.")");
	    $query = $this->db->get();

	    if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function cat_localidades($entidad, $municipio)
	{
		$this->db->select('*');
	    $this->db->from('c_localidad');
	    $this->db->where("CVE_ENT = '".$entidad."' and CVE_MUN = '".$municipio."' ");
	    $query = $this->db->get();

	    if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function cat_colonias($entidad, $municipio)
	{
		$this->db->select('*');
	    $this->db->from('c_colonia');
	    $this->db->join('c_cp', 'c_cp = c_CodigoPostal');
	    $this->db->where("c_Estado = '".$entidad."' and c_Municipio = '".$municipio."' ");
	    $this->db->order_by('Nombre');
	    $query = $this->db->get();

	    if ($query->num_rows() != 0) {

			return $query->result();
		} else {
			return false;
		}
	}

	public function serviciosparcial($contar, $fechaDesde = null, $fechaHasta = null, $estatus = null, $idUsuario = null, $limit = null, $start = null)
	{

		$this->db->select('*, CURRENT_TIMESTAMP <= (servicioFechaCreacion + INTERVAL 30 MINUTE) activar ');
		//$this->db->select('*, CURRENT_TIMESTAMP <= (servicioFechaCreacion + INTERVAL 120 MINUTE) activar ');
	    $this->db->from('servicios');

	    if(is_null($fechaDesde))
	    	$condition = "DATE_FORMAT(servicioFechaCreacion, '%d/%m/%Y') = DATE_FORMAT(now(), '%d/%m/%Y') and servicioIdUsuario = '" . $idUsuario . "' ";
	    else
	    {
	    	if($estatus == '*')
	    		$condition = "servicioFechaCreacion BETWEEN '".$fechaDesde."' AND '".$fechaHasta."' and servicioIdUsuario = '" . $idUsuario . "'";
	    	else
	    		$condition = "servicioFechaCreacion BETWEEN '".$fechaDesde."' AND '".$fechaHasta."' and servicioIdUsuario = '" . $idUsuario . "' and servicioStatus = '".$estatus."'";
	    }
	    //$condition = "servicioIdUsuario = '" . $idUsuario . "' ";

	    $this->db->where($condition);

	    if ($contar == 0)
	    	$this->db->limit($limit, $start);

		$this->db->order_by('servicioFechaCreacion', 'desc');
	    $query = $this->db->get();

	    if ($contar == 1)
			return $query->num_rows();
		else
			if ($query->num_rows() > 0 ) {
				return $query->result();
			} else {
				return false;
			}
	}

	public function serviciosparcialimpreso($fechaDesde, $fechaHasta, $estatus, $idUsuario)
	{

		$this->db->select('*, CURRENT_TIMESTAMP <= (servicioFechaCreacion + INTERVAL 30 MINUTE) activar ');

	    $this->db->from('servicios');

	    if($estatus == '*')
    		$condition = "servicioFechaCreacion BETWEEN '".$fechaDesde."' AND '".$fechaHasta."' and servicioIdUsuario = '" . $idUsuario . "'";
    	else
    		$condition = "servicioFechaCreacion BETWEEN '".$fechaDesde."' AND '".$fechaHasta."' and servicioIdUsuario = '" . $idUsuario . "' and servicioStatus = '".$estatus."'";
	    //$condition = "servicioIdUsuario = '" . $idUsuario . "' ";

	    $this->db->where($condition);

	    $this->db->order_by('servicioFechaCreacion', 'desc');
	    $query = $this->db->get();

		if ($query->num_rows() > 0 ) {
			return $query->result();
		} else {
			return false;
		}
	}

    	///////////////////////precio viaje//////////////////////////////

		public function get_txt_serv_vehic($tipoServicio,$tipoVehiculo){
			/* SELECT tipo_auto.tipo AS tipo_servicio_text, tipo_vehiculo.tipo_vehiculo AS tipo_vehiculo_text
				FROM tipo_auto
				INNER JOIN tipo_vehiculo ON tipo_auto.id = tipo_vehiculo.id_tipo_auto
				WHERE tipo_auto.id= $tipoServicio
					AND tipo_vehiculo.id= $tipoVehiculo
			*/
			$this->db->select('tipo_auto.tipo AS tipo_servicio_text, tipo_vehiculo.tipo_vehiculo AS tipo_vehiculo_text');
			$this->db->from('tipo_auto');
			$this->db->join('tipo_vehiculo','tipo_auto.id = tipo_vehiculo.id_tipo_auto');
			$this->db->where('tipo_auto.id',$tipoServicio);
			$this->db->where('tipo_vehiculo.id',$tipoVehiculo);
			$row=$this->db->get()->row();

			return $row;

		}

		public function get_parametros($tipoServicio,$tipoVehiculo){
			/* "SELECT parametros.*, tipo_vehiculo.tipo_vehiculo as tipo_vehiculo_text, tipo_auto.tipo as tipo_servicio_text
				FROM parametros
				INNER JOIN tipo_vehiculo
					ON tipo_vehiculo.id = parametros.id_tipo_vehiculo
				INNER JOIN tipo_auto
					ON tipo_auto.id = parametros.id_tipo_servicio
				WHERE id_tipo_servicio = 1 AND id_tipo_vehiculo = 1 "

				$this->db->select('parametros.*');
				$this->db->from('parametros');
				$this->db->join('tipo_vehiculo','tipo_vehiculo.id = parametros.id_tipo_vehiculo');
				$this->db->join('tipo_auto','tipo_auto.id = parametros.id_tipo_servicio');
				$this->db->where('id_tipo_servicio',$tipoServicio);
				$this->db->where('id_tipo_vehiculo',$tipoVehiculo);
			*/
			/* "SELECT parametros.*
				FROM parametros
				WHERE id_tipo_servicio = 1 AND id_tipo_vehiculo = 1 "
			*/
			$this->db->select('parametros.*');
			$this->db->from('parametros');
			$this->db->where('id_tipo_servicio',$tipoServicio);
			$this->db->where('id_tipo_vehiculo',$tipoVehiculo);
			$row=$this->db->get()->row();

			return $row;
		}

		public function get_tarifa($distancia,$tabla,$tipoVehiculo){

			if($tipoVehiculo == ""){//si NO es servicio de repartidor
				$this->db->where('d_km <=',$distancia);
				$this->db->where('a_km >',$distancia);
			}else{
				$this->db->where('id_tipo_vehiculo',$tipoVehiculo);
			}
			$row = $this->db->get($tabla)->row();
/* 			echo "|||||||||||||||";
			echo $this->db->last_query() */;
			return $row;
		}

		public function get_tarifa_dinamica($numDia,$hora){
			/* "SELECT tarifa_dinamica.*,tasa.nombre,tasa.tarifa
				FROM `tarifa_dinamica`
				INNER JOIN tasa ON tarifa_dinamica.tasa_id=tasa.id
				WHERE dia_id=$numDia AND hora_inicial <= $hora and hora_final >= $hora" */

				$this->db->select("tarifa_dinamica.*,tasa.nombre,tasa.tarifa");
				$this->db->from("tarifa_dinamica");
				$this->db->join('tasa','tarifa_dinamica.tasa_id = tasa.id');
				$this->db->where('dia_id',$numDia);
				$this->db->where('hora_inicial <=',$hora);
				$this->db->where('hora_final >=',$hora);

				$row = $this->db->get()->row();

			return $row;
		}
		//...precio vije
}
