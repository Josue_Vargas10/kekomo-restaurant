<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Restaurantes_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
        $this->load->database();

    }
    private function build_sorter($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($a->$key, $b->$key);
        };
    }
    public function obtenerRestaurantes($lat_cliente,$lng_cliente,$radio_km){
        $latitud1 = floatval($lat_cliente);
        $longitud1 = floatval($lng_cliente);
        $restaurantes_en_radio=array();

        $all_restaurantes=$this->db->where("cat_estatus_id  !=", 2)->get("restaurantes")->result();

        foreach ($all_restaurantes as $restaurante) {
            $latitud2 = floatval($restaurante->servicioLatitudFin);
            $longitud2 = floatval($restaurante->servicioLongitudFin);
            $distancia = (3958*3.1415926*sqrt(($latitud2-$latitud1)*($latitud2-$latitud1) + cos($latitud2/57.29578)*cos($latitud1/57.29578)*($longitud2-$longitud1)*($longitud2-$longitud1))/180)*1.609344;
            $kilometros = $distancia;
            if($distancia<=$radio_km)
            {
                $restaurante->distancia_al_origen = round($distancia, 2, PHP_ROUND_HALF_EVEN);

                array_push($restaurantes_en_radio,$restaurante);
            }
        }
        usort($restaurantes_en_radio, $this->build_sorter('distancia_al_origen'));
        return $restaurantes_en_radio;

    }
    public function get_restaurante_by_place_id($restaurante_place_id){
        $this->db->where("servicioDestinoPlaceId",$restaurante_place_id);
        return $this->db->get('restaurantes')->row();
    }
}//...class