<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_usuario($telefono)
	{
		$query = $this->db->query("SELECT * FROM usuarios where usuarioCelular = '".$telefono."' and usuarioStatus = 1");
	    return $query->result();
	}

	function insert_usuario($post_data) {
		$data = array(
            'usuario' =>$post_data['usuarioNombre'],
            'telefono' => $post_data['usuarioCelular']
        );

        $result = $this->existeUsuario($data);

        if ($result == FALSE) {
	    	$this->db->insert('usuarios',$post_data);
	    	$id = $this->db->insert_id();
	    	return $id;
		}else
		{
			$date = new DateTime("NOW");
        	$dateformatted = $date->format('Y-m-d H:i:s');

			$data = array(
	            'codigo' =>$post_data['codigo'],
	            'usuarioFechaCreacion' => $dateformatted
	        );
			$this->update_usuario_pendiente($data, $post_data['usuarioCelular']);
		}
	}

	function update_usuario($post_data, $telefono) {
		$condition = "usuarioCelular = '".$telefono. "' and usuarioStatus = 1";
		$this->db->where($condition);
		$this->db->update('usuarios', $post_data);
	}

	function update_usuario_pendiente($post_data, $telefono) {
		$condition = "usuarioCelular = '".$telefono. "' and usuarioStatus = 2";
		$this->db->where($condition);
		$this->db->update('usuarios', $post_data);
	}

	function update_insert_usuario($post_data) {
		$this->db->trans_begin();

		$data = array(
            'usuario' =>$post_data['usuarioNombre'],
			'telefono' => $post_data['usuarioCelular'],
        );

        //$result = $this->existeUsuario($data);
		$result = $this->existeNumero($data,$status="2");
        if ($result == FALSE) {
	    	$this->db->insert('usuarios',$post_data);

	    	if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			    return false;
			}
			else
			{
			    $this->db->trans_commit();
			    return true;
			}
		}else
		{
			$condition = "usuarioCelular = '" . $post_data['usuarioCelular'] . "' and usuarioStatus = 2";
			$this->db->where($condition);
			$this->db->update('usuarios', $post_data);

			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			    return false;
			}
			else
			{
			    $this->db->trans_commit();
			    return true;
			}
		}
	}

	public function existe($data) {
		//$condition = "usuarioNombre = '" . $data['usuario'] . "' AND usuarioCelular = '" . $data['telefono'] . "' and usuarioStatus = 1";
		//$condition = "usuarioCelular = '" . $data['telefono'] . "' and usuarioStatus = 1";
		$condition = "usuarioNombre = '" . $data['usuario'] . "' and usuarioStatus = 1";
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function existeNumero($data,$status="1") {
		//$condition = "usuarioNombre = '" . $data['usuario'] . "' AND usuarioCelular = '" . $data['telefono'] . "' and usuarioStatus = 1";
		//$condition = "usuarioCelular = '" . $data['telefono'] . "' and usuarioStatus = 1";
		$condition = "usuarioCelular = '" . $data['telefono'] . "' and usuarioStatus = $status";
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function existeUsuario($data) {
		$condition = "usuarioNombre = '" . $data['usuario'] . "' AND usuarioCelular = '" . $data['telefono'] . "' and usuarioStatus = 2";
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function login($data) {
		$condition = "usuarioCelular = '" . $data['celular'] . "' AND usuarioPassword = '" . $data['password'] . "' and usuarioStatus = 1";
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function leer_informacion_usuario($userCel) {
		$condition = "usuarioCelular = '" . $userCel . "' and usuarioStatus = 1";
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function contrasena($texto, $tipo) {
		if($tipo == 1)
			$condition = "usuarioCelular = '" . $texto  . "' and usuarioStatus = 1";
		else
			$condition = "email = '" . $texto  . "' and usuarioStatus = 1";

		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			$registros = $query->result();

			$inc = $registros[0]->recuperado+1;

			$post_data = array(
                'recuperado'   =>  $inc
            );

			$this->db->where($condition);
			$this->db->update('usuarios', $post_data);
			return $registros;
		} else {
			return false;
		}
	}

	public function informacion_usuario($usuario) {
		$condition = "usuarioNombre like '" . $usuario . "%'  and tipoUsuario = 2";
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function obtiene_zona($entidad, $municipios)
	{
		$this->db->select('c_estado.nombre estado, c_municipio.nombre municipio');
	    $this->db->from('c_municipio');
	    $this->db->join('c_estado', "c_estado.c_estado = '".$entidad."'");
	    $this->db->where("c_municipio.c_estado = '".$entidad."' and c_municipio in(".$municipios.")");
	    $query = $this->db->get();

	    if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	public function getDatosChofer(){
		$this->db->where('s.servicioId',$this->session->userdata('idservicio'));
		return $this->db->join('operador o','s.servicioIdoperadorAuto = o.operadorId','left')
					  ->select('o.*,s.servicioStatus')
					  ->limit(1)
					  ->get('servicios s')
					  ->row();

	}
	//Información del auto
	public function getInfoAuto(){
		$q =  $this->db->select('a.*,s.precio_min_aprox as precio_min,s.precio_max_aprox as precio_max' )
						->where('s.servicioId',$this->session->userdata('idservicio'))
						->join('operador_autos o','s.servicioIdoperadorAuto = o.OPIdOperador')
						->join('autos a','a.autosId = o.OPIdAuto')
						->get('servicios s');
		if($q->num_rows()==1){
			return $q->row();
		}else{
			return '';
		}


	}

	public function rest_validation($code){
		// extract($data);

		$this->db->select('restaurantes_url');
		$this->db->from('restaurantes');
		$this->db->where('restaurantes_clave', $code);
		$this->db->where('cat_estatus_id', 1);
		// $this->db->like("restaurantes_clave", $code, 'after');
		
	    $query = $this->db->get();

	    if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}

}
