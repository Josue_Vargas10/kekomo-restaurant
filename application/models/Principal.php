
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	//Funcion general para obtener por Id
	public function getById($tabla='',$where=array()) {
		$q = $this->db->where($where)->get($tabla);
	    if($q->num_rows()==1){
	      return $q->row();
	    }else{
	      return '';
	    }
	}
	//guardar en una tabla
	public function save($tabla='',$data=''){
		return $this->db->insert($tabla,$data);
	}
	//update en una tabla
	public function update($tabla='',$where=array(),$data=''){
		return $this->db->where($where)->update($tabla,$data);
	}
	//Obtener todos los registros de una tabla
	public function getAll($tabla=''){
		return $this->db->get($tabla)->result();
	}
	//saber si existe el registro
	public function existeRegistro($tabla='',$where=array()) {
		$q = $this->db->where($where)->get($tabla);
	    if($q->num_rows()==1){
	      return true;
	    }else{
	      return false;
	    }
	}
}
?>