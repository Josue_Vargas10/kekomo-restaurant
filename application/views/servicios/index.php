<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);

} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>
<div class="limiter">

      <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
            <br/>
            <a href="tel:+911" class="js_enviar_correo">
            <img height="50" src="<?php echo base_url()?>sos.png" >
          </a>
          </div>
          <?php 
        }
        ?>
      <form id="solicitarform" class="login100-form" role="form" action="<?php echo site_url('/usuarios/finalizar') ?>" method="post">


        <?php if(isset($this->session->userdata['loggedin']['id'])){ ?>
            <input type="hidden" name="hdSIniciado" id="hdSIniciado" value="1">
            <?php }else{ ?>
            <input type="hidden" name="hdSIniciado" id="hdSIniciado" value="0">
            <?php } ?>

            <input type="hidden" name="hdServicioFinalizado" id="hdServicioFinalizado" value="0">

            <input type="hidden" id="hdLatitud" name="hdLatitud" value="<?php if (isset($latitud)) echo $latitud ?>" />
            <input type="hidden" id="hdLongitud" name="hdLongitud" value="<?php if (isset($longitud)) echo $longitud ?>" />
            <input type="hidden" id="hdDomicilio" name="hdDomicilio" value="<?php if (isset($domicilio)) echo $domicilio ?>" />
            <input type="hidden" id="hdFormatedAdress" name="hdFormatedAdress" value="<?php if (isset($hdFormatedAdress)) echo $hdFormatedAdress ?>" />
            <input type="hidden" id="hdnMensaje" value="<?php if (isset($mensaje)) { echo $mensaje; }
            ?>"/>
            <?php
            if (isset($encontrado) && $encontrado = 1) {
            ?>
            <div class="input-group">
              <h3><b><span class="btn-cancelar" style="color:red; cursor:default">Tienes un servicio en espera</span></b></h3>
                <input type="hidden" id="hdEncontrado" value="1"/>
                <input type="hidden" id="hdServicio" value="<?php if (isset($idServicio)) echo $idServicio ?>"/>
                
            </div>

          <b><p><span>Puedes seleccionar el vehículo más cercano.</span></p></b>
            <?php
            } ?>
            
          <!-- Google Map -->
        <div class="google-map">
          <div id="map-canvas"></div>
        </div>  
      </form>         

</div>

<form id="cancelarform" class="form-horizontal" role="form" action="<?php echo site_url('/usuarios/cancelar') ?>" method="post">
  <input type="hidden" id="hdServicioCancelar" name="hdServicioCancelar" value="<?php if (isset($idServicio)) echo $idServicio ?>"/>
</form>

<!-- Modal -->
<div class="modal wrap-modal100" id="myModal" role="dialog">
  <h3 id="chofer_sitio" class="login100-form-title"></h3>

  <div class="container-login50-form-btn">
    <img class="img-responsive" src="" alt="" id="foto"/>
  </div>

  <div id="chofer_nombre" class="text-justify p-b-5 p-t-10"></div>
  <div id="chofer_telefono" class="text-justify p-b-5"></div>
  <div id="chofer_vehiculo" class="text-justify p-b-5"></div>
  <div id="chofer_tipo" class="text-justify p-b-5"></div>
  <div id="chofer_placas" class="text-justify p-b-5"></div>
  <div id="chofer_color" class="text-justify p-b-5"></div>
  
  <input type="hidden" id="hdOperador" value=""/>

  <div class="container-50-form-btn">
    <a id="fototarjeton" class="registrar50-form-btn" >Ver tarjetón</a>
  </div>

  <div class="container-login50-form-btn">
    <input type="button" id="btn-contactar" class="login50-form-btn" value="Contactar"/>
    <input type="button" id="btn-cerrar" class="login50-form-btn login50-form-btn-cancel" value="Cerrar"/>
  </div>
</div>


<div class="modal wrap-modalmensaje" id="myModal6" role="dialog">
  <div class="text-justify p-b-10">
    <span>Domicilio aproximado: </span>
  </div>
  <div class="text-justify p-b-10">
    <span id="con_domicilio"></span>
  </div>
  <div class="text-justify p-b-10">
    <span>¿Es correcta tu ubicación?</span>
  </div>
  <div class="container-login50-form-btn">
    <input type="button" id="btn-confirmar" class="login50-form-btn" value="SI"/>
    <input type="button" id="btnRegresar" class="login50-form-btn login50-form-btn-cancel" value="NO"/>
  </div>
</div>

<!-- Modal -->
<div class="modal wrap-modal80" id="myModal2" role="dialog">
  <div class="container-login50-form-btn">
    <img src="" alt="" id="chofer_tarjeton"//>
  </div>
  <div class="container-login50-form-btn">
    <input type="button" id="btn-cerrarfoto" class="login50-form-btn login50-form-btn-cancel" value="Cerrar"/>
  </div>
</div>

<div class="modal wrap-modal80" id="myModalChoferUltimo" role="dialog">
  <div class="container-login100-form-btn">
    <img src="" alt="" id="chofer_tarjeton_ultimo"//>
  </div>
  <div class="container-login50-form-btn">
    <input type="button" id="btn-cerrarfotoultimo" class="login50-form-btn login50-form-btn-cancel" value="Cerrar"/>
  </div>
</div>

<div class="modal wrap-modalmensaje" id="myModal3" role="dialog">
  <div class="text-justify p-b-10">
    <span>Al parecer el operador ya se comunicó contigo. Indícanos si tu servicio fue Aceptado o Rechazado?</span>
  </div>

  <input type="hidden" id="hdIdOperador" value=""/>
  
  <div class="container-login50-form-btn">
    <input type="button" id="btnAceptar" class="login50-form-btn" value="ACEPTADO"/>
    <input type="button" id="btnRechazar" class="login50-form-btn login50-form-btn-cancel" value="RECHAZADO"/>
  </div>
</div>

<div class="modal wrap-modalmensaje" id="myModal4" role="dialog">
  <div class="text-justify p-b-10">
    <span>Tu servicio no puede ser atendido por el chofer que elegiste. Por favor contacta a otro o espera a que Central de Bases se comunique contigo.</span>
  </div>

  <div class="container-login50-form-btn">
    <input type="button" id="btnRechazado" class="login50-form-btn login50-form-btn-cancel" value="ACEPTAR"/>
  </div>
</div>

<div class="modal wrap-modalmensaje" id="myModal7" role="dialog">
  <div class="text-justify p-b-10">
    <span>Tu servicio no puede ser atendido por el chofer que elegiste. Por favor contacta a otro o espera a que Central de Bases se comunique contigo.</span>
  </div>

  <div class="container-login50-form-btn">
    <input type="button" id="btnPendiente" class="login50-form-btn login50-form-btn-cancel" value="ACEPTAR"/>
  </div>
</div>

<div class="modal wrap-modalmensaje" id="myModal5" role="dialog">
  <div class="text-justify p-b-10">
    <span>Tu servicio no ha recibido respuesta por algún operador y se ha cancelado. Por favor vuelve a solicitar otro servicio.</span>
  </div>

  <div class="container-login50-form-btn">
    <input type="button" id="btnAceptarCancelacion" class="login50-form-btn login50-form-btn-cancel" value="ACEPTAR"/>
  </div>
</div>

<div class="modal wrap-modal100" id="myModalUltimo" role="dialog">
  <h3 id="chofer_sitio_ultimo" class="login100-form-title"></h3>

  <div class="container-login50-form-btn">
    <img src="" alt="" id="foto_ultimo"/>
  </div>

  <div id="chofer_nombre_ultimo" class="text-justify p-b-5 p-t-10"></div>
  <div id="chofer_telefono_ultimo" class="text-justify p-b-5"></div>
  <div id="chofer_vehiculo_ultimo" class="text-justify p-b-5"></div>
  <div id="chofer_tipo_ultimo" class="text-justify p-b-5"></div>
  <div id="chofer_placas_ultimo" class="text-justify p-b-5"></div>
  <div id="chofer_color_ultimo" class="text-justify p-b-5"></div>
  
  <input type="hidden" id="hdOperador_ultimo" value=""/>
  <input type="hidden" id="hdIdServicio_ultimo" value=""/>

  <div class="container-50-form-btn">
    <a id="fototarjeton_ultimo" class="registrar50-form-btn" >Ver tarjetón</a>
  </div>

  <div class="container-login50-form-btn">
    <input type="button" id="btn-contactar_ultimo" class="login50-form-btn" value="Contactar"/>
    <input type="button" id="btn-cerrarultimo" class="login50-form-btn login50-form-btn-cancel" value="Cerrar"/>
  </div>
</div>

  <script>
    var delay;
    var delay2;
    var delay3;

     $('#btn-cerrarultimo').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModalUltimo').modal('hide');
    });
     $('#btn-cerrar').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModal').modal('hide');
    });

    $('#fototarjeton').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModal2').modal({backdrop: "static"});
    });

    $('#btn-cerrarfoto').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModal2').modal('hide');
    });
    $('#btn-cerrarfotoultimo').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModalChoferUltimo').modal('hide');
    });
    

    $('#fototarjeton_ultimo').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModalUltimo').modal('hide');
      $('#myModalChoferUltimo').modal({backdrop: "static"});
    });
/*
    $('#scerrar').click(function(e) {
      $('#myModal2').modal('hide');
    });*/

    $('#btnRegresar').click(function(e) {

      $('#myModal6').modal('hide');
      alert("Mueve el 'monito' a tu ubicación exacta")
    });

    $('#btn-confirmar').click(function(e) {
      solicitarform.submit();
    });
    
    $('#btnRechazar').click(function(e) {
      $('#myModal3').modal('hide');
      activarstatus();
      operadorServicio(4);
    });

    $('#btnRechazado').click(function(e) {
      $('#myModal4').modal('hide');
      clearInterval(delay3);
      operadorServicio(4);
      activarstatus();
    });

    $('#btnPendiente').click(function(e) {
      $('#myModal7').modal('hide');
      clearInterval(delay3);
      activarstatus();
    });


    $('#btnAceptarCancelacion').click(function(e) {
        clearInterval(delay2);
        $("#cancelarform").submit();
    });

    $('#btnAceptar').click(function(e) {
      $('#myModal3').modal('hide');
      operadorServicio(2);
      window.location="<?php echo base_url() ?>";
    });

    function operadorServicio(estatus)
    {
      var servicio = $('#hdServicio').val();
      var operador = $('#hdIdOperador').val();

      $.ajax({
        type: "POST", 
        dataType: "json",
        data: { 'idServicio' : servicio, 'idOperador' : operador, 'estatus' : estatus }, 
        url: "<?php echo base_url() ?>index.php/usuarios/operador_Servicio/"+servicio,
        success: function (data) {

        },
        error: function(xhr, ajaxOptions, throwError){
            alert(throwError);
        }
      });
    }

    function activarstatus ()
    {
      var servicio = $('#hdServicio').val();
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "<?php echo base_url() ?>index.php/usuarios/activarServicio/"+servicio,
          success: function (data) {
              if(data['return'] == 1) 
                window.location="<?php echo base_url() ?>";
          },
          error: function(xhr, ajaxOptions, throwError){
              alert(throwError);
          }
        });
    }

    var mensaje = "";
    $( document ).ready(function() {
        

        mensaje = $('#hdnMensaje').val().toString();
        if ( mensaje != ""){
          alert(mensaje);
        }

        if($("#hdEncontrado").val() == "1")
        {
          var servicio = $('#hdServicio').val();

          values = $.ajax({
          url : "<?php echo base_url()?>index.php/usuarios/status_servicio/"+servicio,
          type : "GET",
          dataType: "json",
          async : true,
          success: function(result){
              $.each( result, function( key, value ) {
                if(value.servicioStatus == 5) //Notificado
                {
                  delay3 = window.setInterval(function() {
                        get_notificado();
                        console.log("get_notificado");
                    }, 60000);
                }

              }); 
            }
          }).responseText;
        }
    });

  
    function imageExists(exists, url, nombre) 
    { 
    
      $(nombre).attr("src", url);
    } 

    $('#btn-contactar').click(function(e) {
        var operador = $('#hdOperador').val();
        var servicio = $('#hdServicio').val();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url() ?>index.php/usuarios/enviar_notificacion/"+operador+"/"+servicio+"/1",
          dataType: "json",
          success: function (data) {
              if(data['return'] == 1){
                  $("#hdIdOperador").val(operador);
                  if($("#hdEncontrado").val() == "1"){
                    delay3 = window.setInterval(function() {
                      get_notificado();
                      console.log("get_notificado");
                    }, 60000);
                    get_marcas(servicio);
                  }

                  alert(data['codigo']);
              }
              else{
                  alert('Ocurrió un error. Por favor intentalo de nuevo.');
              }
              $('#myModal').modal('hide');
          },
          error: function(xhr, ajaxOptions, throwError){
              alert(throwError);
          }
        });
    });

    $('#btn-contactar_ultimo').click(function(e) {
        var operador = $('#hdOperador_ultimo').val();
        var servicio = $('#hdIdServicio_ultimo').val();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url() ?>index.php/usuarios/enviar_notificacion/"+operador+"/"+servicio+"/0",
          dataType: "json",
          success: function (data) {
              if(data['return'] == 1){
                  alert(data['codigo']);
              }
              else{
                  alert('Ocurrió un error. Por favor intentalo de nuevo.');
              }
              $('#myModal_ultimo').modal('hide');
          },
          error: function(xhr, ajaxOptions, throwError){
              alert(throwError);
          }
        });
    });

   $('#btn-cancelar').click(function(e) {
        /*$.ajax({
          type: "POST",
          url: "cancelar",
          success: function (data) {
            location.reload();
          },
          error: function(xhr, ajaxOptions, throwError){
            alert(throwError);
          }
        });*/

         $("#cancelarform").submit();
    });
    
    function getLocation() {
      if($("#hdEncontrado").val() == "1")
      {
        initMap(parseFloat($("#hdLatitud").val()),parseFloat($("#hdLongitud").val()));
      }else{
        if (location.protocol == 'https:'){
          // Try HTML5 geolocation.
          if (navigator.geolocation) {
            console.log("navigator.geolocation");
            navigator.geolocation.getCurrentPosition(showPosition, function(error){
                console.log("error");
                console.log(error.code);
              // El segundo parámetro es la función de error
                  switch(error.code) {
                    case 1:
                        alert('Por favor activa los permisos de ubicación (GPS) de manera manual en la configuración de tu dispositivo, para que se cargue el mapa correctamente y tu ubicación más exacta.');
                        break;
                    case 2:
                        alert('Por favor activa los permisos de ubicación (GPS) de manera manual en la configuración de tu dispositivo, para que se cargue el mapa correctamente y tu ubicación más exacta.');
                        break;
                    case 3:
                        alert('Por favor activa los permisos de ubicación (GPS) de manera manual en la configuración de tu dispositivo, para que se cargue el mapa correctamente y tu ubicación más exacta.');
                        break;
                  }
                  initMap(parseFloat("19.25566608394677"),parseFloat("-103.7515141069153"));
                  
                }
              );
                //initMap(position.coords.latitude,position.coords.longitude); 
          } else {
            console.log("geolocate");
            //navigator.geolocation.getCurrentPosition(showPosition);
            $.ajax({
              type : 'POST',
              data: '', 
              url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE", 
              success: function(result){
                  initMap(result['location']['lat'],result['location']['lng']); 
                        
              }});
            }
        }else {
          console.log("geolocate");
          //navigator.geolocation.getCurrentPosition(showPosition);
          $.ajax({
            type : 'POST',
            data: '', 
            url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE", 
            success: function(result){
                initMap(result['location']['lat'],result['location']['lng']); 
                console.log(result['location']['lat']);
                console.log(result['location']['lng']);
            }});
          }
      }
    }
    function showPosition(position) {
        console.log(position.coords.latitude);
        console.log(position.coords.longitude);
        initMap(position.coords.latitude,position.coords.longitude); 
    }

    
    function CenterControl(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#57cbcc';
        controlUI.style.border = '2px solid #57cbcc';
        controlUI.style.borderRadius = '20px';
        controlUI.style.boxShadow = '0 6px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.marginTop = '15px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click para solicitar tu taxi'; 
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(255, 255, 255)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '18px';
        controlText.style.padding = '14px';
        controlText.innerHTML = 'SOLICITA TU SERVICIO AQUÍ';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          confirmar();
        });
    }

    function CenterControlCancel(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#4e595f';
        controlUI.style.border = '2px solid #4e595f';
        controlUI.style.borderRadius = '20px';
        controlUI.style.boxShadow = '0 6px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.marginTop = '15px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click para solicitar tu taxi'; 
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(255, 255, 255)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '18px';
        controlText.style.padding = '14px';
        controlText.innerHTML = 'CANCELA TU SERVICIO AQUÍ';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          cancelarform.submit();
        });

    }

    function RightControl(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#4f82bc';
        controlUI.style.border = '2px solid #4f82bc';
        controlUI.style.borderRadius = '20px';
        controlUI.style.boxShadow = '0 6px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '17px';
        controlUI.style.marginTop = '10px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click para ver tú último servicio'; 
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(255, 255, 255)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '18px';
        controlText.style.padding = '8px';
        controlText.innerHTML = 'ÚLTIMO SERVICIO';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          $('#myModalUltimo').modal({backdrop: "static"});
        });

    }

    function LeftControl(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#8d9093';
        controlUI.style.border = '2px solid #8d9093';
        controlUI.style.borderRadius = '20px';
        controlUI.style.boxShadow = '0 6px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '17px';
        controlUI.style.marginTop = '10px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Regresar a ubicación actual'; 
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(255, 255, 255)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '18px';
        controlText.style.padding = '8px';
        controlText.innerHTML = '<img class="img-responsive" src="<?php echo base_url() ?>assets/images/ubicacion.png " style="width: 30px; height: 30px" id="ubicacion"/>';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          getLocation();
        });

    }

    var markers = [];
    var map;
    var markeruser;
    var infowindow;

    function initMap(nlat, nlng) {
      $("#hdLatitud").val(nlat);
      $("#hdLongitud").val(nlng);
      // Create a map object and specify the DOM element for display.
      map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: nlat, lng: nlng},
          zoom: 15, 
          gestureHandling: 'greedy', 
          disableDefaultUI: true
        });

        var iconBase = '<?php echo base_url() ?>assets/images/logos/';
        var icons = {
          library: {
            icon: iconBase + 'user.png'
          }
        };

        var userLatLng = new google.maps.LatLng(nlat, nlng);

        var features = [
        {
          position: userLatLng,
          type: 'library'
        } ];

        writeAddressName(userLatLng);

          // Create markers.
         features.forEach(function(feature) {
            if($("#hdEncontrado").val() == "1"){
              markeruser = new google.maps.Marker({
                  position: feature.position,
                  icon: icons[feature.type].icon,
                  map: map
                });
            }
             else{
               markeruser = new google.maps.Marker({
                  position: feature.position,
                  icon: icons[feature.type].icon,
                  map: map, 
                  draggable:true 
                });
            }
            
            google.maps.event.addListener(markeruser, "dragstart", closeMapInfoWindow );
             google.maps.event.addListener(markeruser, 'dragend', function (event){
                  $("#hdLatitud").val(this.getPosition().lat());
                  $("#hdLongitud").val(this.getPosition().lng());
                  writeAddressName(new google.maps.LatLng(this.getPosition().lat(), this.getPosition().lng()));
                  
                })
          });

         // Create the DIV to hold the control and call the CenterControl()
        // constructor passing in this DIV.
        var centerControlDiv = document.createElement('div');
        

         if($("#hdEncontrado").val() == "1")
         {
            var centerControl = new CenterControlCancel(centerControlDiv, map);

            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

            /*if($("#hdVez").val() == "1")
              delay3 = window.setInterval(function() {
                notifica_siguiente();
                console.log("notifica_siguiente");
              }, 30000);
            else*/
              get_marcas($("#hdServicio").val());
         }else
         {
            var centerControl = new CenterControl(centerControlDiv, map);

            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
         }

        if($("#hdSIniciado").val() == "1")
        {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/usuarios/obtenerServicios",
            dataType: "json",
            success: function (data) {
              if(data['servicios'].length > 0)
                { 
                  $('#hdServicioFinalizado').val("1");

                  if(data['servicios'][0]['autosTipo'] == 1)
                    sTipo = 'AUTOMÓVIL';
                  else 
                    sTipo = 'CAMIONETA';

                   //infowindow.open(map, marker);
                  $("#chofer_placas_ultimo").text("Placas: " + data['servicios'][0]['autosPlacas']);
                  $("#chofer_nombre_ultimo").text("Chofer: " + data['servicios'][0]['OperadorNombreCompleto']);
                  $("#chofer_telefono_ultimo").text("Telefono: " + data['servicios'][0]['operadorTelefono']);
                  $("#chofer_vehiculo_ultimo").text("Vehículo: " + data['servicios'][0]['autosDescripcion']);
                  $("#chofer_color_ultimo").text("Color: " + data['servicios'][0]['autosColor']);
                  $("#chofer_sitio_ultimo").text("Sitio: " + data['servicios'][0]['autosSitio']+' '+data['servicios'][0]['autosNick']);
                  $("#chofer_tipo_ultimo").text("Tipo: " + sTipo);
                  

                  /*var imageFile = "<?php //echo base_url() ?>central_admin/"+data['servicios'][0]['OperadorImagen'];

                  var img = new Image() 
                  img.onload = function(){imageExists(true, "<?php echo base_url() ?>central_admin/"+data['servicios'][0]['OperadorImagen'], '#foto_ultimo')}
                  img.onerror = function(){imageExists(false, "<?php echo base_url() ?>assets/images/logos/logo_10b.png", '#foto_ultimo')}
                  img.src = imageFile*/
                  
                  $('#foto_ultimo').attr("src", "<?php echo base_url() ?>central_admin/"+data['servicios'][0]['OperadorImagen']);
                  
                  

                  $('#chofer_tarjeton_ultimo').attr("src", "<?php echo base_url() ?>central_admin/"+data['servicios'][0]['OperadorImagen2']);

                  cargarImagenes();
                  //$('#chofer_tarjeton').val("<?php //echo base_url() ?>central_admin/"+foto2);
                  $('#hdIdServicio_ultimo').val(data['servicios'][0]['servicioId']);
                  $('#hdOperador_ultimo').val(data['servicios'][0]['OPIdOperador']);
                  
                  var rightControlDiv = document.createElement('div');
                   var rightControl = new RightControl(rightControlDiv, map);

                    rightControlDiv.index = 1;
                    map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(rightControlDiv);
                }else
                  $('#hdServicioFinalizado').val("0");
            },
            error: function(xhr, ajaxOptions, throwError){
                alert(throwError);
            }
          });
        }

        var leftControlDiv = document.createElement('div');
        var leftControl = new LeftControl(leftControlDiv, map);

        leftControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.LEFT_CENTER].push(leftControlDiv);
         
         google.maps.event.trigger(map, 'resize');
      }//...initmap

      function cargarImagenes(){
        var img1 = document.getElementById('chofer_tarjeton_ultimo');
        img1.onerror = cargarImagenPorDefecto;
        var img2 = document.getElementById('foto_ultimo');
        img2.onerror = cargarImagenPorDefecto;
        var img3 = document.getElementById('chofer_tarjeton');
        img3.onerror = cargarImagenPorDefecto;
        var img4 = document.getElementById('foto');
        img4.onerror = cargarImagenPorDefecto;
      }

      function cargarImagenPorDefecto(e){
        e.target.src= "<?php echo base_url() ?>assets/images/logos/logo_10.png";
      }
      

      function closeMapInfoWindow() {infowindow.close(); }
    function writeAddressName(latLng) {

      $.ajax({
            type : 'POST',
            data: '', 
            url: "https://maps.googleapis.com/maps/api/geocode/json?latlng="+parseFloat($("#hdLatitud").val())+","+parseFloat($("#hdLongitud").val())+"&key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE", 
            success: function(result){
                

                var address = result['results'][0]['formatted_address'];
                $("#hdDomicilio").val(address);      
                console.log(address);
                //document.getElementById("address").innerHTML = address;
                var contentString = '<div id="content" class="contentMap">'+address+'</div>';

                infowindow = new google.maps.InfoWindow({
                  content: contentString
                });
                infowindow.open(map, markeruser);
            }});

       /* var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
          "location": latLng
        },
        function(results, status) {
          if (status == google.maps.GeocoderStatus.OK){
            var dom = "";
            dom = results[0].formatted_address.toString();
            document.getElementById("address").innerHTML = results[0].formatted_address;
            document.getElementById("hdDomicilio").innerHTML = results[0].formatted_address;
            //return results[0].formatted_address;
          }
          else
            document.getElementById("error").innerHTML += "No se puede recuperar su dirección" + "<br />";
          $("#hdDomicilio").val("");
            //return "Unable to retrieve your address";
        });*/
    }

    function get_marcas(idServicio){
        clearMarkers() ;
        //alert('test');
        values = $.ajax({
          url : "<?php echo base_url()?>index.php/usuarios/operadores/"+idServicio,
          type : "GET",
          dataType: "json",
          async : true,
          success: function(result){
            $.each( result, function( key, value ) {
              //alert("{lat: "+value.latitud+", lng:"+value.longitud+" }" );
              var coordenadas = {lat: parseFloat(value.OPLatitud), lng:parseFloat(value.OPLongitud) };
              addMarkerWithTimeout(coordenadas, key * 200,value.autosPlacas,value.OperadorNombreCompleto,value.operadorTelefono,value.autosDescripcion,value.autosColor,value.autosSitio+' '+value.autosNick,value.OperadorImagen,value.OPIdOperador,value.OperadorImagen2,value.autosTipo);
                }); 
            }
      }).responseText;
    }

    function addMarkerWithTimeout(position,timeout,placas,nombre,telefono,vehiculo,color,sitio,foto,idOperador,foto2,tipo) {
        var iconBase = '<?php echo base_url() ?>assets/images/';

        if(tipo == 1)
          iconBase = iconBase + 'taxi.png';
        else
          iconBase = iconBase + 'pickup_10.png';

        var marker = new google.maps.Marker({
          position: position,
          map: map,
          animation: google.maps.Animation.DROP,
          icon: iconBase
        })
        markers.push(marker);

       
        marker.addListener('click', function() {
          if(tipo == 1)
            sTipo = 'AUTOMÓVIL';
          else 
            sTipo = 'CAMIONETA';

           //infowindow.open(map, marker);
          $("#chofer_placas").text("Placas: " + placas);
          $("#chofer_nombre").text("Chofer: " + nombre);
          $("#chofer_telefono").text("Telefono: " + telefono);
          $("#chofer_vehiculo").text("Vehículo: " + vehiculo);
          $("#chofer_color").text("Color: " + color);
          $("#chofer_sitio").text("Sitio: " + sitio);
          $("#chofer_tipo").text("Tipo: " + sTipo);
          
          $('#foto').attr("src", "<?php echo base_url() ?>central_admin/"+foto);
          $('#chofer_tarjeton').attr("src", "<?php echo base_url() ?>central_admin/"+foto2);
          //$('#chofer_tarjeton').val("<?php //echo base_url() ?>central_admin/"+foto2);
          $('#hdOperador').val(idOperador);
          $('#myModal').modal({backdrop: "static"});
        });
    }

     delay = window.setInterval(function() {
        if($("#hdEncontrado").val() == "1"){
          get_marcas($("#hdServicio").val());
          get_status();
          console.log("get_status");
        }
      }, 30000);

     delay2 = window.setInterval(function() {
        if($("#hdEncontrado").val() == "1"){
          get_pendiente();
          console.log("get_pendiente");
        }
      }, 300000);

    function clearMarkers() {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
      }
      markers = [];
    }


    function get_status(){
        var servicio = $('#hdServicio').val();

        values = $.ajax({
        url : "<?php echo base_url()?>index.php/usuarios/status_servicio/"+servicio,
        type : "GET",
        dataType: "json",
        async : true,
        success: function(result){
            $.each( result, function( key, value ) {
              if(value.servicioStatus == 2) {//Aceptado
                $("#myModal3").modal({backdrop: "static"});
                $("#hdIdOperador").val(value.servicioIdoperadorAuto);
              }

              if(value.servicioStatus == 4) {//Rechazado
                $("#myModal4").modal({backdrop: "static"});
                $("#hdIdOperador").val(value.servicioIdoperadorAuto);
              }
            }); 
          }
        }).responseText;
    }
    function get_pendiente(){
        var servicio = $('#hdServicio').val();

        values = $.ajax({
        url : "<?php echo base_url()?>index.php/usuarios/status_servicio/"+servicio,
        type : "GET",
        dataType: "json",
        async : true,
        success: function(result){
            $.each( result, function( key, value ) {
              if(value.servicioStatus == 1) //Pendiente
                $("#myModal5").modal({backdrop: "static"});
            }); 
          }
        }).responseText;
    }

    function get_notificado(){
        var servicio = $('#hdServicio').val();

        values = $.ajax({
        url : "<?php echo base_url()?>index.php/usuarios/status_servicio/"+servicio,
        type : "GET",
        dataType: "json",
        async : true,
        success: function(result){
            $.each( result, function( key, value ) {
              if(value.servicioStatus == 5) //Notificado
                $("#myModal7").modal({backdrop: "static"});

            }); 
          }
        }).responseText;
    }

    function notifica_siguiente()
    {

    }

    function confirmar()
    {
      var domicilio = $("#hdDomicilio").val();
      var latitud = $("#hdLatitud").val();
      var longitud = $("#hdLongitud").val();

      if(domicilio != '')
      {
        $("#con_domicilio").text(domicilio);
        $("#myModal6").modal({backdrop: "static"});
      }
      else
      {
        alert('Por favor activa los permisos de ubicación (GPS) de manera manual en la configuración de tu dispositivo, para que se cargue el mapa correctamente y tu ubicación más exacta.');
      }
    }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE&callback=getLocation">
  </script>
  </body>

</html>