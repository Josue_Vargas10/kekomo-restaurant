<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <div class="input-group">
        <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
          </div>
          <?php 
        }
        ?>
      </div>
      <form id="contact-form" class="login100-form" role="form" action="<?php echo site_url('/usuarios/grabarComentario') ?>" method="post">
          <!-- section title -->
          <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
              <h2><span class="color">Buzón</span></h2>
              <div class="border"></div>
          </div>
          <!-- /section title -->

          <div class="input-group">
              <?php echo form_error('usuario', '<div class="text-center p-t-12 txterror">', '</div>'); 
              if (isset($message_display)) { ?>
                  <div class="text-center p-t-12">
                    <span class="txterror"><?php echo $message_display;?>
                    </span>
                  </div>
              <?php }?>
          </div>

          <div class="wrap-input80">
            <textarea class='textarea input100' rows='4' id='comentario' name='comentario' placeholder='Deja tu comentario aquí'></textarea>
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-edit" aria-hidden="true"></i>
            </span>
          </div>

          <div  class="input-group">
              <?php echo form_error('comentario', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
          </div>

          <div class="container-login50-form-btn">
            <input type="submit" id="btnAceptar" class="login50-form-btn" value="Enviar"/>
            <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel" value="Cancelar"/>
          </div>
        </form>                   
      </div>  
  </div>
</div>

<script type="text/javascript">
  $('#btn-cancelar').click(function(e) 
  {
      window.location="<?php echo base_url() ?>index.php/usuarios/index";
  });
</script>