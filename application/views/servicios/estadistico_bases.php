
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
          </div>
          <?php 
        }
        ?>
      <form id="estadisticoform" class="login100-form" role="form" method="post">
        <!-- section title -->
        <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
            <h2>Estadístico de <span class="color">servicios</span></h2>
            <div class="border"></div>
        </div>
        <!-- /section title -->

        <div class="input-group">
            <?php 
            if (isset($message_display)) { ?>
                <div class="text-center p-t-12">
                  <span class="txterror"><?php echo $message_display;?>
                  </span>
                </div>
            <?php }?>
        </div>

        <div class="p-b-10">
          <h5>SELECCIONA FECHAS Y ESTATUS </h5>
        </div>

        <div class="container-50-form-btn">
            <input class="input input25 datepicker" id='fecha_inicio' name='fecha_inicio' placeholder='Desde'>
            <span class="focus-input25"></span>
            <input class="input input25 datepicker" id='fecha_fin' name='fecha_fin' placeholder='Hasta'>
            <span class="focus-input25"></span>
        </div>

        <div class="wrap-input50 p-t-12 p-b-12">
          <select class="input input80" id="cmbEstatus" name = "cmbEstatus" onchange="limpiar()">
              <option value="0" disabled selected hidden>Estatus</option>
              <option value="1">PENDIENTE</option>
              <option value="2">ACEPTADO</option>
              <option value="3">CANCELADO</option>
              <option value="4">RECHAZADO</option>
              <option value="5">NOTIFICADO</option>
              <option value="*">TODOS</option>
          </select>
          <a class="input100" href="javascript:buscar(1);"><i class="fa fa-search"></i></a>
          <a class="input100" id="imprimir" ><i class='fa fa-print'></i></a>
        </div>

        <div class="wrap-input100 table-responsive" id="servicios_table"></div>
        <div align="right" id="pagination_link"></div>
        <div align="right" id="totales"></div>
      </form>  
    </div>
  </div> <!-- /container -->
</div>

<form id="TheForm" method="post" action="<?php echo site_url('/usuarios/impreso') ?>" target="TheWindow">
  <input type="hidden" name="hdfechaDesde" id="hdfechaDesde" />
  <input type="hidden" name="hdfechaHasta" id="hdfechaHasta" />
  <input type="hidden" name="hdusuario" id="hdusuario" />
  <input type="hidden" name="hdestatus" id="hdestatus" />
  <input type="hidden" name="hdbase" id="hdbase" />
</form>
 

<script type="text/javascript">
    function buscar(page)
    {
      fechaDesde = $('#fecha_inicio').val();
      fechaHasta = $('#fecha_fin').val();
      usuario = "<?php echo $this->session->userdata['loggedin']['id'];?>";

      if(fechaDesde == '')
      {
        alert("Debe seleccionar un rango de fechas");
        $('#fecha_inicio').focus();
        return;
      }

      if(fechaHasta == '')
      {
        alert("Debe seleccionar un rango de fechas")
        $('#fecha_fin').focus();
        return;
      }


      var combo = document.getElementById("cmbEstatus");

      if (combo.value == '0')
      {
        alert("Debe seleccionar un estatus");
        $('#cmbEstatus').focus();
        return;
      }

      $('#hdfechaDesde').val(fechaDesde);
      $('#hdfechaHasta').val(fechaHasta);
      $('#hdusuario').val(usuario);
      $('#hdestatus').val(combo.value);
      $('#hdbase').val("1");

      $.ajax({
        type: "post",
        data: { 'fechaDesde' : fechaDesde, 'fechaHasta' : fechaHasta, 'estatus' : combo.value, 'usuario' : usuario }, 
        url: "<?php echo base_url() ?>index.php/Usuarios/serviciosparcialfechas/"+page,
        dataType: "json",
        success: function (data) {
          $('#servicios_table').html(data.servicios_table);
          $('#pagination_link').html(data.pagination_link);
          $('#totales').html("<h6><b>TOTAL DE REGISTROS: "+data.total+"</b></h6>");
          /*
          if(data.total > 0)
            $('#imprimir').attr("href", "javascript:imprimir(1);");
          */
        },
        error: function(xhr, ajaxOptions, throwError){
          $("#codigo_error").css({"visibility": ""});
          //alert(throwError);
        }
      });
    }

    function limpiar()
    {
      $('#servicios_table').html("");
      $('#pagination_link').html("");
      $('#totales').html("");
    }

    $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      var page = $(this).data("ci-pagination-page");
      buscar(page);
     });

    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#fecha_inicio').datepicker({
      uiLibrary: 'bootstrap', 
      format: 'dd/mm/yyyy', 
      maxDate: function () {
        return $('#fecha_fin').val();
      }
    });

    $('#fecha_fin').datepicker({
      uiLibrary: 'bootstrap', 
      format: 'dd/mm/yyyy',
      minDate: function () {
        return $('#fecha_inicio').val();
      }
    });

    function imprimir()
    {
      window.open('', 'TheWindow');
      document.getElementById('TheForm').submit();
    }
</script>   