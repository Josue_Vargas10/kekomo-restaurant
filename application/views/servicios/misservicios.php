<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>
<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">

        <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
          </div>
          <?php 
        }
        ?>
      <form id="registraform" class="login100-form" role="form" action="<?php echo site_url('/usuarios/nuevo_usuario_registro') ?>" method="post">
            <!-- section title -->
            <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
                <h2>Mis <span class="color">viajes</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <div class="input-group">
                <?php echo form_error('usuario', '<div class="text-center p-t-12 txterror">', '</div>'); 
                if (isset($message_display)) { ?>
                    <div class="text-center p-t-12">
                      <span class="txterror"><?php echo $message_display;?>
                      </span>
                    </div>
                <?php }?>
            </div>

            <div class="table-responsive wrap-input100">
              <table class='table table-hover' id='tblOrdenes' name='tblOrdenes'>
                <thead>
                  <tr>
                    <th scope="col" style='width:5%'>SERVICIO</th>
                    <th scope="col" style='width:35%'>OPERADOR</th>
                    <th scope="col" style='width:5%'>FOTO</th>
                    <th scope="col" style='width:35%'>VEHÍCULO</th>
                    <th scope="col" style='width:10%'>FECHA</th>
                    <th scope="col" style='width:10%'>VALORACIÓN</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($finalizados as $row) {
                    $date = new DateTime($row->servicioFechaCreacion);
                  if($row->OperadorNombreCompleto <> '') 
                    $operador = $row->OperadorNombreCompleto; 
                  else 
                    $operador = '&nbsp;';
                  if($row->autosDescripcion <> '') 
                    $auto = $row->autosDescripcion; 
                  else 
                    $auto = '&nbsp;';

                  if($row->OperadorImagen <> '') 
                    $operadorimg = "imgclick('$row->OperadorImagen')"; 
                  else 
                    $operadorimg = '';
                  ?>
                    <tr>
                      <th scope="row"><?php echo $row->servicioId ?></th>
                      <td><?php echo $operador ?></td>
                      <td><img class="img-search" onclick="<?php echo $operadorimg; ?>" id="imgOperador" src="<?php echo base_url() ?>assets/images/search_10.png" style="cursor: pointer;"></td>
                      <td><?php echo $auto ?></td>
                      <td><?php echo $date->format('d/m/Y') ?></td>
                      <?php if($row->rating > 0) {?>
                      <td class="td<?php echo $row->servicioId ?>">
                          <div class="stars-outer">
                            <div class="stars-inner"></div>
                        </div>
                      </td>
                      <?php }else { ?>
                        <td><a style="cursor:pointer;" href="javascript:calificar(<?php echo $row->servicioId ?>);">CALIFICAR</a></td>
                      <?php }?>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
            </div>

            <div class="container-login50-form-btn">
                <input type="button" id="btn-inicio" class="login50-form-btn login50-form-btn-cancel"  value="Ir al inicio"/>
            </div>

            <div style="display: none;" id="codigo_error">
                <small class="help-block text-center p-t-12 txterror" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ha ocurrido un error, por favor intente más tarde</small>   
            </div>
          </form>                   
        </div>  
    </div>
</div>

<form id="myModal2" name="myModal2" class="modal wrap-modal50 validate-form2" method="post">
  <div class=" wrap-input100">
    <img class="img-responsive" src="" alt="" id="foto"/>
  </div>
  
  <div class="container-login50-form-btn">
    <input type="button" id="btn-cerrar" class="login50-form-btn login50-form-btn-cancel" value="Cerrar"/>
  </div>
</form>

<form id="myModal" name="myModal" class="modal wrap-modal50 validate-form2" action="<?php echo base_url() ?>index.php/Usuarios/grabarRating" method="post" onsubmit="return validateFormRating()">
  <span class="login100-form-title">
    Califica tu viaje
  </span>

  <div class=" wrap-input100">
    <div class="star-rating" style="margin-bottom: 25px; text-align: center; cursor: pointer;">
      <span class="fa fa-star-o" data-rating="1"></span>
      <span class="fa fa-star-o" data-rating="2"></span>
      <span class="fa fa-star-o" data-rating="3"></span>
      <span class="fa fa-star-o" data-rating="4"></span>
      <span class="fa fa-star-o" data-rating="5"></span>
      <span>&nbsp;&nbsp;Excelente</span>
      <input type="hidden" name="rating" id="rating" class="rating-value" value="0">
    </div>
  </div>

  <div class="wrap-input80">
    <textarea class='textarea input100' rows='4' id='comentario' name='comentario' placeholder='Deja tu comentario aquí'></textarea>
    <span class="focus-input100"></span>
    <span class="symbol-input100">
      <i class="fa fa-edit" aria-hidden="true"></i>
    </span>
  </div>
  
  <input type="hidden" name="hdIdServicio" id="hdIdServicio">

  <div class="container-login50-form-btn">
    <input type="submit" id="btnAceptar" class="login50-form-btn" value="Enviar" disabled/>
    <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel" value="Cancelar"/>
  </div>
</form>

<script>
  $('#btn-inicio').click(function(e) 
  {
      window.location="<?php echo base_url() ?>index.php/usuarios/index";
  });

  $('#btn-cerrar').click(function(e) 
  {
      $("#myModal2").modal('hide');
  });

	function validateFormRating()
    {
	  	var rating = $("#rating").val();

	  	if (rating == '0')
	  	{
	        alert("Debe indicar al menos una estrella");
	        return false;
		}
    }

	$('#btn-cancelar').click(function(e) {
		$("#myModal").modal('hide');
	});

	function calificar(idServicio)
	{
    $("#rating").val("0");
		$("#hdIdServicio").val(idServicio);
    stars();
		$("#myModal").modal({backdrop: "static"});
	}
	function imgclick(url){
      $('#foto').attr("src", "<?php echo base_url() ?>central_admin/"+url);
      $('#myModal2').modal('show');
    }

    function cargarImagenes(){
        var img1 = document.getElementById('foto');
        img1.onerror = cargarImagenPorDefecto;
        }

      function cargarImagenPorDefecto(e){
        e.target.src= "<?php echo base_url() ?>assets/images/logos/logo_10.png";
      }


    $(document).ready(function(){
    	$.ajax({
          type: "post",
          url: "<?php echo base_url() ?>index.php/Usuarios/obtenerServicios/",
          dataType: "json",
          success: function (data) {
          	if(data['servicios'].length>0)
          	{
          		const starTotal = 5;
	            for (var i = 0, len = data['servicios'].length; i < len; i++) 
	            {
	            	if(data['servicios'][i]['rating']>0)
	            	{
					  const starPercentage = (data['servicios'][i]['rating'] / starTotal) * 100;
					  const starPercentageRounded = `${(Math.round(starPercentage / 10) * 10)}%`;
					  const rating = 'td'+data['servicios'][i]['servicioId'];
					  document.querySelector(`.${rating} .stars-inner`).style.width = starPercentageRounded; 
					}
	            }
            }
          },
          error: function(xhr, ajaxOptions, throwError){
          	alert(throwError);
          }
        });

      cargarImagenes();
    });

    function stars()
    {
      var $star_rating = $('.star-rating .fa');
      var $ranking = 0;
      var SetRatingStar = function() {
        return $star_rating.each(function() {
          if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            $ranking = $(this).data('rating');
            return $(this).removeClass('fa-star-o').addClass('fa-star');
          } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
          }
        });
      };

      $star_rating.on('click', function() {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        $SetRatingStar = SetRatingStar();
        //alert($ranking);
        if($ranking > 0)
        {
        	$('#btnAceptar').prop( "disabled", false );
        }
        else
        {
        	$('#btnAceptar').prop( "disabled", true );
        }
        return $SetRatingStar;
      });

      SetRatingStar();
    }
</script>