<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>             
<div class="limiter">
  <div class="container-login100">
    <div class="wrap-logincomplete">

      <?php 
      if (isset($this->session->userdata['loggedin'])) 
      {
        ?>
        <div class="text-right wrap-input100">
          <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
          </span>
        </div>
        <?php 
      }
      ?>
      <form id="solicitarform" class="login100-form" role="form" method="post">
        <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
            <h2>Mis <span class="color">viajes</span></h2>
            <div class="border"></div>
        </div>

        <div class="wrap-input100 table-responsive" id="servicios_table"></div>
        <div align="right" id="pagination_link"></div>
        
    </form>  
    </div>
  </div> <!-- /container -->
</div>

<script type="text/javascript">
    $(document).ready(function(){
       buscar(1);

    });

    var delay = window.setInterval(function() {
        location.reload();
    }, 30000);

    
    function activar(idServicio)
    {
      $.ajax({
        type: "post",
        url: "<?php echo base_url() ?>index.php/Usuarios/activarServicioBase/"+idServicio,
        dataType: "json",
        success: function (data) {
          if(data['return'] == 1)
            location.reload();
          else
            alert("Ocurrió un error. Intente en un momento");
        },
        error: function(xhr, ajaxOptions, throwError){
          $("#codigo_error").css({"visibility": ""});
        }
      });
    }

    function buscar(page)
    {
        $.ajax({
              type: "post",
              url: "<?php echo base_url() ?>index.php/Usuarios/serviciosparcial/"+page,
              dataType: "json",
              success: function (data) {
                $('#servicios_table').html(data.servicios_table);
                $('#pagination_link').html(data.pagination_link);
              },
              error: function(xhr, ajaxOptions, throwError){
                $("#codigo_error").css({"visibility": ""});
                //alert(throwError);
              }
            });

      //}
         //else
          //window.location="<?php //echo base_url() ?>index.php/Ordenes/Lista";
    }

    $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      var page = $(this).data("ci-pagination-page");
      buscar(page);
     });

</script>