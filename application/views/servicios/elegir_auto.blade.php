<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
  if($username==""){
    ?><script>alert();</script><?php
  }
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <div class="input-group">
        <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
          </div>
          <?php 
        }
        ?>
      </div>
            @if(count($datos_servicio)>0)
              <div class="row" style="width: 100%">
                <div class="col-sm-12">
                   <div class="input-group">
                    <h3><b><span class="btn-cancelar" style="color:red; cursor:default">Tienes un servicio en espera...</span></b></h3>
                  </div>
                </div>
              </div>
              <div class="container-login100-form-btn">
                    <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel" value="Cancelar servicio" data-idservicio="{{(isset($idServicio))? $idServicio:'' }}">
              </div>
                
              @else
              <div class="row" style="width: 100%">
                <div class="col-sm-12 txt1 label-select-auto">
                    <label>Seleccione el tipo de servicio</label>
                    {{$drop_tipo_servicio}}<!--tipo_de_servicio-->
                </div>
              </div>
              <div class="row" style="width: 100%">
                <div class="col-sm-12 txt1 label-select-auto">
                    <label>Seleccione el tipo de vehículo</label>
                    <!-- {{$drop_tipo_vehiculo}} -->
                    <select name="tipo_vehiculo" id="tipo_vehiculo" class="form-control">
                    </select>
                </div>
              </div>
              <div class="container-login100-form-btn">
                    <input type="button" id="btn-continuar" class="login50-form-btn login50-form-btn-cancel" value="Continuar">
              </div>

            @endif        
      </div>  
  </div>
</div>

<script type="text/javascript">
  var site_url = "{{site_url()}}";

/* al seleccionar opcion de 'tipo_servicio' 
obtener las opciones disponibles para select 'tipo_vehiculo' */
$("#tipo_servicio").change(function() { 
    //este es el elemento select
    let select_tip_vhc=$("#tipo_vehiculo");
    //tomar valor seleccionado de tipo_servicio 
    let auto_selected=$("#tipo_servicio option:selected").val();
    let auto_selectext=$("#tipo_servicio option:selected").text();
    //

if(auto_selectext=="-- Selecciona --"){
  ErrorCustom('Debes seleccionar un servicio');
}else{

    $.ajax({
          type: "POST", 
          dataType: "json",
          url: "<?=base_url()?>index.php/usuarios/vehiculos_disponibles/"+auto_selected,
          success: function (data) {

             //alert(data[0].tipo_vehiculo); 
          //actualizo los valores del select tipo_vehiculo
                //limpio valores
                select_tip_vhc.empty();
                //actualizo valores
                $.each(data, function(key,value) {
                  if(this.tipo_vehiculo!="Automovil de Lujo"){
                      select_tip_vhc.append($("<option></option>")
                        .attr("value",this.id).text(this.tipo_vehiculo));}
                
                });
          },
          error: function(xhr, ajaxOptions, throwError){
              alert("err:"+throwError);
          }
        });
      }
})


  $("#btn-cancelar").on('click',function(e){
    e.preventDefault();
    ConfirmCustom("¿Está seguro de cancelar el servicio?", cancelarServicio,"", "Confirmar", "Cancelar");
  });
  $("#btn-continuar").on('click',function(e){//tipo_de_servicio añadir al if
    e.preventDefault();
    var tipo_servicio = $("#tipo_servicio").val();
    var tipo_vehiculo = $("#tipo_vehiculo").val();

    if(tipo_servicio==''){
      ErrorCustom('Es necesario seleccionar el tipo de servicio');
    }else{

        ajaxJson(site_url+"/usuarios/update_session_servicio_temporal",{
            'tipo_servicio':tipo_servicio,
            'tipo_vehiculo':tipo_vehiculo,
            'flag':"tipo_servicio"

        },"POST","async",function(result){

        });
        setTimeout(function(){window.location.href = site_url+'/usuarios/mapa'; }, 500);


      
    }
  });

  function cancelarServicio(){
    
    var idServicio = $(this).data('idservicio');
     var url = site_url+'/usuarios/cancelar_servicio';
      ajaxJson(url,{"idServicio":idServicio},"POST","async",function(result){
        if(result){
          location.reload();
        }
      });  
  }
  

</script>