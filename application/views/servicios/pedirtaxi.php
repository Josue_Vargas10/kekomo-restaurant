<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <form id="registraform" class="login100-form" role="form" method="post">
            <!-- section title -->
            <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
                <h2>¿Cómo pedir <span class="color">un taxi?</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->
            <div class="text-justify p-b-10">
              <span>En Central de Bases pedir un taxi es muy sencillo. Sólo sigue los siguientes pasos y en minutos tendrás tu transporte justo donde lo necesitas!</span>
            </div>
            <div class="text-justify p-b-10">
              <span>1. Primero que nada, habilita los permisos de ubicación. Esto con la finalidad de poder localizarte en el lugar exacto del mapa.</span>
            </div>
            <div class="text-justify p-b-10">
              <span>2. Si tu servicio lo necesitas en un lugar distinto al que te encuentras o al que aparece en el mapa, arrastra el marcador y posiciónalo en el domicilio aproximado.</span>
            </div>
            <div class="text-justify p-b-10">
              <span>3. Da click en el botón "Solicita tu táxi público aquí".</span>
            </div>
            <div class="text-justify p-b-10">
              <span>4. Si aún no inicias sesión, se te pedirá tu Usuario y Contraseña. Ingrésalos y da click en el botón "Confirma tu solicitud".</span>
            </div>
            <div class="text-justify p-b-10">
              <span>4.1 En caso de que no tengas un usuario registrado, date de alta en el link "Regístrate aquí".</span>
            </div>
            <div class="text-justify p-b-10">
              <span>4.2 Llena los datos que se te piden y da click en el botón "Solicitar código". Te llegará un mensaje de texto con un código de 4 dígitos al celular que ahí mismo proporcionaste. Captúralo en donde se te pide y procede a Registrarte. Inicia sesión como se te indicó en el paso 4 de esta lista.</span>
            </div>
            <div class="text-justify p-b-10">
              <span>5. Listo! tu solicitud ha sido recibida. Espera a que uno de los choferes de Central de Bases se comunique contigo o contacta directamente a alguno que esté disponible, de la siguiente manera.</span>
            </div>
            <div class="text-justify p-b-10">
              <span>5.1 Ubica en el mapa el chofer más cercano al domicilio donde necesitas el servicio. </span>
            </div>
            <div class="text-justify p-b-10">
              <span>5.2 Da click sobre el ícono del carro o camioneta para ver la información detallada del chofer y su vehículo. Si es lo que necesitas, presiona el botón "Contactar" y espera a que se comunique contigo.</span>
            </div>

            <div class="container-login50-form-btn">
                <input type="button" id="btn-inicio" class="login50-form-btn login50-form-btn-cancel"  value="Ir al inicio"/>
            </div>
          </form>                   
        </div>  
    </div>
</div>

<script type="text/javascript">
  $('#btn-inicio').click(function(e) 
  {
      window.location="<?php echo base_url() ?>index.php/usuarios/index";
  });
</script>