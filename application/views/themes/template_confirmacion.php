<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Kekomo</title>
    <link rel="icon" sizes="32x32" href="<?php echo base_url() ?>assets/images/kekomo/favicon-32x32.png" type="image/png"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/loading-bar.css"/>
    <script src="<?php echo base_url('assets/vendor/loading-bar.js') ?>"></script>
    <!--===============================================================================================-->
    <link href="<?php echo base_url() ?>assets/css/custom/styles.css" rel="stylesheet">
  </head>
  <body>

      <?php echo $output;?>

    <!-- Footer -->
    <footer>
      <div class="container">
        <p class="m-0 text-center text-black txt3">Vargas Group &copy; <script>document.write(new Date().getFullYear())</script>. Todos los derechos reservados.</p>
        <!-- <b><p class="m-0 text-center text-black">Contacto: vargaslagos@gmail.com</p></b> -->
      </div>
      <!-- /.container -->
    </footer>

    <!--===============================================================================================-->
    <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/popper.js"></script>
    <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  </body>
</html>
