<!DOCTYPE html>
<html lang="en">

  <head>



    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!--link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/logos/logo_5a.ico" /-->

    <title>Kekomo</title>
    <link rel="icon" sizes="32x32" href="<?php echo base_url() ?>assets/images/kekomo/favicon-32x32.png" type="image/png"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- icofont -->
    <link href="<?php echo base_url() ?>assets/css/icofont.css?v=<?php echo time();?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() ?>assets/css/full-width-pics.css" rel="stylesheet">

    <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/datetimepicker/js/gijgo.min.js" type="text/javascript"></script>

    <!--===============================================================================================-->
    <!-- selectpicker CSS-->
    <!--<link href="<?php echo base_url() ?>assets/css/bootstrap-selectpicker.css"> -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> -->


    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/animate/animate.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/util.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/main_custom.css?v=<?php echo time();?>">
    <!--===============================================================================================-->

    <!--===============================================================================================-->

    <link href="<?php echo base_url() ?>assets/datetimepicker/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/homescreen/style/addtohomescreen.css">
    <script src="<?php echo base_url() ?>assets/homescreen/src/addtohomescreen.js"></script>

  </head>

  <body>

    <!-- Navigation -->
    <!-- <nav class="navbar navbar-expand-lg navbar-light  fixed-top"> -->
    <nav class="navbar navbar-light  fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- logo -->
            <?php
          if (isset($this->session->userdata['loggedin']) && $this->session->userdata['loggedin']['tipoUsuario'] == 2)
          {
        ?>
            <a class="logo fixed-top" href="<?php echo base_url() ?>index.php/usuarios/index_base">
              <img src="<?php echo base_url() ?>assets/images/logos/logo_10b.png" class="logo-rounded" alt="logo"/>
            </a>
            <?php
          }else{
            ?>
            <a class="logo fixed-top" href="<?php echo base_url() ?>index.php/usuarios/index">
              <img src="<?php echo base_url() ?>assets/images/kekomo/kekomo.png" width="90px" class="logo-rounded" alt="logo"/>
            </a>
          <?php }?>
        <!-- /logo -->

        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <?php
              if (isset($this->session->userdata['loggedin']) && $this->session->userdata['loggedin']['tipoUsuario'] == 2)
              {
            ?>
              <li>
                <a class="nav-link" href="<?php echo base_url() ?>index.php/usuarios/index_base">Inicio</a>
              </li>
              <li>
                <a class="nav-link" href="<?php echo base_url() ?>index.php/usuarios/estadistico_bases">Estadístico</a>
              </li>
              <li>
                <a class="nav-link" href="<?php echo base_url() ?>index.php/usuarios/vermapa">Mapa</a>
              </li>
            <?php
            }
            else
            {
            ?>
              <li>
                <a class="nav-link" href="<?php echo base_url() ?>index.php/usuarios/index">Inicio</a>
              </li>
              <!-- <li>
                <a class="nav-link" href="<?php //echo base_url() ?>index.php/usuarios/misservicios">Mis viajes</a>
              </li>
              <li>
                <a class="nav-link" href="<?php //echo base_url() ?>index.php/usuarios/mis_correos">Mis correos</a>
              </li>
              <li>
                <a class="nav-link" href="<?php// echo base_url() ?>index.php/usuarios/mapa">Mapa servicios</a>
              </li>
              <li>
                <a class="nav-link" href="<?php //echo base_url() ?>index.php/usuarios/pedirtaxi">¿Cómo pedir un taxi?</a>
              </li>
              <li>
                <a class="nav-link" href="javascript:addToHomescreen({detectHomescreen: true});">Acceso Directo</a>
              </li>

                <li>
                  <a class="nav-link" href="<?php //echo base_url() ?>index.php/usuarios/mostrar_registro_operador">Registro</a>
                </li>
                <li>
                  <a class="nav-link" href="http://centraldebases.com/operador/operador_v_1_0.apk
">App Chofer</a>
                </li> -->

              <?php
            } ?>

            <li>
              <a class="nav-link" href="<?php //echo base_url() ?>index.php/usuarios/comentarios">Buzón</a>
            </li>
            <?php
            if (isset($this->session->userdata['loggedin']))
            {
              if($this->session->userdata['loggedin']['usuario'] == 'adolfo' || $this->session->userdata['loggedin']['usuario'] == 'Brenda' || $this->session->userdata['loggedin']['usuario'] == 'Prueba')
              {
              ?>
<!--                 <li>
                  <a class="nav-link" href="<?php //echo base_url() ?>index.php/usuarios/nuevo_operador_vehiculo">Asignar vehículo</a>
                </li>
                <li>
                  <a class="nav-link" href="<?php //echo base_url() ?>index.php/usuarios/estadistico">Estadístico Bases</a>
                </li> -->
              <?php
              } ?>

              <li>
                <a class="nav-link" href="<?php echo base_url() ?>index.php/usuarios/logout">Cerrar sesión</a>
              </li>

            <?php
            }else{
            ?>
              <li>
                <a class="nav-link" href="<?php echo base_url() ?>index.php/usuarios/login">Iniciar sesión</a>
              </li>
          <?php
          }
          ?>

          </ul>
        </div>
      </div>
    </nav>

    <?php if($this->load->get_section('text_header') != '') { ?>
      <h1><?php echo $this->load->get_section('text_header');?></h1>
    <?php }?>
      <?php echo $output;?>

    <!-- Footer -->
    <footer class="py-4">
      <div class="container">
        <p class="m-0 text-center text-black txt3">Diseñado por Vargas Group &copy; <script>document.write(new Date().getFullYear())</script>. Todos los derechos reservados.</p>
        <b><p class="m-0 text-center text-black">Contacto: vargaslagos@gmail.com</p></b>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->

    <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Boostrap Selectpicker JS-->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/js/bootstrap-selectpicker.js"></script> -->
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-es_ES.js"></script> -->

    <!--===============================================================================================-->
    <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/popper.js"></script>
    <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?php echo base_url() ?>assets/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?php echo base_url() ?>assets/vendor/tilt/tilt.jquery.min.js"></script>

    <script src="<?php echo base_url('assets/js/custom/bootbox.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/custom/general.js?v=').time() ?>"></script>
    <script src="<?php echo base_url('assets/js/custom/isloading.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/custom/moment.js') ?>"></script>
    <script >
      $('.js-tilt').tilt({
        scale: 1.1
      })
      var site_url = "<?php echo site_url() ?>";
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
          $("body").on('click','.js_enviar_correo',enviar_correos_sos)
          function enviar_correos_sos(){
             var url = site_url+'/usuarios/sendMails';
              ajaxJson(url,{},"POST","async",function(result){
            });

          }
          $("#ver-presupuesto").on('click',function(e){
            e.preventDefault();
            console.log(traits);
          });

      });

    </script>

    <!--===============================================================================================-->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <?php
      if (!isset($this->session->userdata['loggedin']) OR $this->session->userdata['loggedin']['tipoUsuario'] != 2)
      { ?>
        <!--script>(function(v,p){
        var s=document.createElement('script');
        s.src='https://app.toky.co/resources/widgets/toky-widget.min.js?v='+v;
        s.onload=function(){Toky.load(p);};
        document.head.appendChild(s);
        })('d091d7db', {"$username":"TAXISCOLIMA","$bubble_title":"Hola!","$bubble_message":"Haz clic en el botón de abajo para llamar directamente a nuestra base.","$text":"Llamar a la base"})
    </script-->
    <?php
    }?>
  </body>

</html>
