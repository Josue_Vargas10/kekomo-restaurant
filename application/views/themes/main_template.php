<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="El buscador de los mejores restaurantes">         
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    
    <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!-- <link rel="icon" sizes="32x32" href="<?php echo base_url() ?>assets/images/kekomo/favicon-32x32.png" type="image/png"/> -->
    
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#ff7b00">
    
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/sliderman.1.3.8.js"></script>
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sliderman.css" />
    
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/custom/aside.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/custom/kekomo.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fontawesome-free/css/all.min.css">    
    <!-- <link rel="manifest" href="/manifest.json"> -->

    <title>KeKomo</title>      
  </head>
  <body>
    <div class="wrapper">
      <!-- Sidebar -->
      <div id="mySidenav" class="sidenav">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-11">

                 </div>
                 <div class="col text-right">
                      <a class="d-inline" href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                 </div>
             </div>
             <div class="row">
                 <div class="col">
                      <a id="home" href="<?php echo base_url(); ?>" >
                          <i class="fas fa-home"></i>
                          <?php echo $this->lang->line('home'); ?>
                      </a>
                 </div>
             </div>
             <div class="row">
                 <div class="col">
                     <a href="#">
                          <img src="<?php echo base_url();?>assets/images/kekomo/tenedor3.png" alt="logo" width=25px height=25px>    
                          <?php echo ucfirst($this->lang->line('about')); ?>
                     </a>
                 </div>
             </div>
             <div class="row">
                 <div class="col">
                      <a href="#"><i class="fas fa-file" aria-hidden="true"></i><?php echo ucfirst($this->lang->line('billing')); ?></a>
                 </div>
             </div>
             <div class="row">
                 <div class="col ">
                      <a  href="#" ><i class="fas fa-handshake" aria-hidden="true"></i> <?php echo ucfirst($this->lang->line('partners')); ?></a>
                 </div>
             </div>
             <div class="row">
                 <div class="col">
                      <a id="afiliate" href="#"><i class="fas fa-sign-in-alt" aria-hidden="true"></i> <?php echo ucfirst($this->lang->line('register')); ?></a>
                 </div>
             </div>

             

         </div>

      </div>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Afiliate a KeKomo</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>                
            <div class="modal-footer">
              <div id="myZadarmaCallmeWidget7329"></div>                    
            </div>
          </div>
        </div>
      </div>


      <!-- Modal PWA INSTALLATION -->
      <div class="modal fade" id="installation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Instala nuestra App</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>    
            <div class="modal-body" style="text-align:center">
              
             
              <div class="popup">
                <span class="popuptext" id="myPopup">Para proceder, haga click sobre este recuadro y siga las instrucciones.</span>
              </div>
              <h6 id="unknow-browser" style="display:none;">Para instalar la app busca la opción agregar al inicio o instalar en tu navegador."</h6>
              
             
              

              <div id="wrapper">
                <div id="examples_outer">
                  <div id="slider_container_2">
                    <div id="SliderName_2" class="SliderName_2">
                      <img src="<?php echo base_url();?>assets/images/firefox/step1.jpeg" alt="Demo2 first" title="Demo2 first" usemap="#img1map" />
                   
                      <div class="SliderName_2Description">1.- Has click en más opciones.</div>
                      <img src="<?php echo base_url();?>assets/images/firefox/step2.jpeg" title="Demo2 second" />
                      <div class="SliderName_2Description">2.- Click en instalar.</div>                      
                      <img src="<?php echo base_url();?>assets/images/firefox/step3.jpeg" alt="Demo2 fourth" title="Demo2 fourth" />
                      <div class="SliderName_2Description">3.- Busca el icono en tu pantalla de  inicio.</div>
                    </div>
                    <div class="c"></div>
                    <div id="SliderNameNavigation_2"></div>
                    <div class="c"></div>
                    <script type="text/javascript">
                     
                    </script>
                    <div class="c"></div>
                  </div>
                  <div class="c"></div>
                </div>                              
                <div class="c"></div>
              </div>
            </div>
            <div class="modal-footer" style="display:none">
              <button type="button"  class="btn-round" >
                Regresar</button>
               <!-- <button class="add-button btn-round" >Instalar</button>  -->
            </div>
          </div>
        </div>
      </div>            
      <div id="content">
          <header class="main-header text-center py-2">
            <div id="nav" class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <nav>
                    <div id="menu"  onclick="openNav()">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </div>

                    <div id="lang">



                       <a id="link" href="<?php echo  base_url();?>index.php/usuarios/index/<?php echo $this->lang->line('language'); ?>">
                         <i class="fas fa-globe" aria-hidden="true"></i><label><?php echo ucfirst($this->lang->line('language')); ?></label></a>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </header>
          <section class="main_container">
              <div  class="container">
                  <?php echo $output;?>
              </div>
          </section>
      </div>
    </div>
                   
                      
    <footer class="install-popup">
      <h6>¡Instala nuestra App!</h6>
      <button type="button"  class="btn-round" id="reject">
                Ahora no</button>
      <button id="accept" class="add-button btn-round" ><strong>Instalar</strong></button>
    </footer>                      
    <!-- SCRIPTS -->
      <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/popper.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/fontawesome-free/js/all.js"></script>    
      <script type="text/javascript">
          /* Set the width of the side navigation to 250px */
            function openNav() {

                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
                {
                    document.getElementById("mySidenav").style.width = "200px";
                }else{
                    document.getElementById("mySidenav").style.width = "350px";
                }


            }

          /* Set the width of the side navigation to 0 */
          function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
          }
      </script>
      <script type="text/javascript">
          $(document).ready( ()=> {
              let lang = "<?php echo $this->lang->line('language'); ?>";
              let url = window.location.href;

              if(! (url.includes("index.php")) ){
                  let index = url.lastIndexOf("/");
                  let segment = url.slice( index + 1);
                  url = url.slice(0,index + 1);
                  url = url + "index.php/" + segment;
              }else{
                  if( ! url.endsWith("/")){
                      url = url + "/";
                  }
              }
              if( ! (url.includes("english") ) && ! (url.includes("spanish") ) ){
                  let index = url.lastIndexOf("/");
                  let segment = url.slice( index + 1);
                  url = url.slice(0,index + 1);
                  url = url + lang + "/" + segment
                  $("#link").attr("href", url);
              }
              else{
                  // console.log("TRUE");
                  if( url.includes("english")){
                      url = url.replace("english","spanish");
                  }
                  else if( url.includes("spanish")){
                      url= url.replace("spanish","english");
                  }
                  // console.log(url);
                  $("#link").attr("href", url);
              }
          });
      </script>    
      <div id="zadarmaScripts"></div>
      <script>
        (function() {
          var script = document.createElement('script');
          script.src = 'https://my.zadarma.com/callmewidget/v2.0.8/loader.js';
          document.getElementById('zadarmaScripts').appendChild(script);
        }());
      </script>
      <script>
        var myZadarmaCallmeWidget7329;
        var myZadarmaCallmeWidgetFn7329 = function() {
          myZadarmaCallmeWidget7329 = new ZadarmaCallmeWidget("myZadarmaCallmeWidget7329");
          myZadarmaCallmeWidget7329.create({
            "widgetId": "zU7c374rjCba6ZuBU6m8gjPbyu49h3zBh9Dz1G51xNp6zbkL6R3ygmPcUT7Ssvn4RvnzdyrGtTaN35Mjjjh5yYhhJ32gjxvA1213b9ed2de1aa81a8e3046151a09e01", "sipId":"249642_101", "domElement":"myZadarmaCallmeWidget7329" }, { "shape":"square", "language":"es", "width":"0", "dtmf":false, "font": "'Trebuchet MS','Helvetica CY',sans-serif", "color_call": "rgb(255, 255, 255)", "color_bg_call": "rgb(240,175,12)", "color_border_call": "rgb(153,100,35)", "color_connection": "rgb(255, 255, 255)", "color_bg_connection": "rgb(59,30,112)", "color_border_connection": "rgb(187,240,236)", "color_calling": "rgb(255, 255, 255)", "color_border_calling": "rgb(255, 218, 128)", "color_bg_calling": "rgb(255, 181, 0)", "color_ended": "rgb(255, 255, 255)", "color_bg_ended": "rgb(164,164,164)", "color_border_ended": "rgb(210, 210, 210)"
          });
        }

        if (window.addEventListener) {
          window.addEventListener('load', myZadarmaCallmeWidgetFn7329, false);
        } else if (window.attachEvent) {
          window.attachEvent('onload', myZadarmaCallmeWidgetFn7329);
        }

        $("#afiliate").click(()=>{
                // console.log("Afialarte");

                // console.log($("#exampleModal"));
                $("#exampleModal").modal();
            });

        $("#btn-drive-thru").click( ()=>{
          window.location.href = "<?=base_url("/index.php/usuarios/login/drive_thru")?>"
        });
        $("#btn-recoger").click( ()=>{
          window.location.href = "<?=base_url("/index.php/usuarios/login/recoger")?>"
        });
        $("#btn-domicilio").click( ()=>{
          window.location.href = "<?=base_url("/index.php/usuarios/login/domicilio")?>"
        });
        $("#btn-resto").click( ()=>{
          window.location.href = "<?=base_url("/index.php/".$this->lang->line('current_lang')."/restaurante")?>"
        });
      </script>
    
      <script>        
        slider = undefined;
        
        // browser = "slsl";

        
        
        if( isMobile() ){
          let browser = $("#userAgent").val();
          
          
          if(browser == 'Safari')
            setImages('safari');                                  
          else  if(browser == 'Firefox')              
            setImages('firefox');         
                       
            slider = reloadSlider()
          if( (window.matchMedia('(display-mode: standalone)').matches) ){
            // $("#install-popup").hide();
          }else{
            // console.log(browser);
            $(".install-popup").attr("style", 'display: flex;');
              
            $("#reject").click(()=>{
              $(".install-popup").hide();
            });             
          }
          
          
          if(! ('beforeinstallprompt' in navigator) && browser != 'Chrome'){
            // alert("why mother fucker " + browser);
            
            $("#accept").click(()=>{
             
              // console.log(slider)
              $(".install-popup").hide();

              $("#installation").modal(); 
              $("#installation .modal-dialag").hide()

              // console.log(browser);
              var popup = document.getElementById("myPopup");

              setTimeout(() => {
                    popup.classList.toggle("show");
                  }, 600);
                  
              $("#myPopup").click(()=>{
                $("#myPopup").removeClass('show');
                      
                if(browser != 'Safari' && browser != 'Firefox'){               
                  $("#unknow-browser").show();
                }else{
                  slider.start();     
                  $("#slider_container_2").show();
                }
              });
            });

         
          }
        }
        
        $("#installation .close").click(()=>{
          $("#unknow-browser").hide();
                  
            // $("#SliderNameNavigation_2").empty();
            $(".install-popup").attr("style", 'display: flex;');
            slider.stop();  
            $("#slider_container_2").hide();
            
          
        })
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', () => {
            navigator.serviceWorker.register('/service-worker.js')
                .then((reg) => {
                  console.log('Service worker registered.', reg);
                });
            
            let deferredPrompt;
            const addBtn = document.querySelector('.add-button');      
            // addBtn.style.display = 'none';
            // $('.add-button').unbind('click');
            window.addEventListener('beforeinstallprompt', (e) => {                      

              // Prevent Chrome 67 and earlier from automatically showing the prompt
              e.preventDefault();
              // Stash the event so it can be triggered later.
              deferredPrompt = e;        
              // Update UI to notify the user they can add to home screen
              addBtn.style.display = 'block';

              addBtn.addEventListener('click', (e) => {
                //  hide our user interface that shows our A2HS button
                $(".install-popup").hide();
                addBtn.style.display = 'none';
                // Show the prompt
                deferredPrompt.prompt();
                // Wait for the user to respond to the prompt
                deferredPrompt.userChoice.then((choiceResult) => {
                  if (choiceResult.outcome === 'accepted') {
                    console.log('User accepted the A2HS prompt');
                  } else {
                    console.log('User dismissed the A2HS prompt');
                  }
                  deferredPrompt = null;
                });
              });
            });
           
        });
        }                       
        function isMobile(){
          return (
              (navigator.userAgent.match(/Android/i)) ||
              (navigator.userAgent.match(/webOS/i)) ||
              (navigator.userAgent.match(/iPhone/i)) ||
              (navigator.userAgent.match(/iPod/i)) ||
              (navigator.userAgent.match(/iPad/i)) ||
              (navigator.userAgent.match(/BlackBerry/i ))
          );
        } 

        function setImages(browser){
          let default_browser = '';
          let images  = $("#SliderName_2").children('img');
        
          if(browser == 'safari')
            default_browser = 'firefox';          
          else if (browser == 'firefox')
            default_browser = 'safari';

          // console.log("cambiando imgs")
          images.each((index, img )=>{
            let url = $(img).attr('src');
            $(img).attr('src', url.replace(default_browser, browser) );
            
          });
          

        }
      
      
      function reloadSlider(){
        // $("#SliderNameNavigation_2").empty();
      // $("#SliderName_2 div:not(.SliderName_2Description)").remove()    
        effectsDemo2 = 'rain,stairs,fade';
                      var demoSlider_2 = Sliderman.slider({container: 'SliderName_2', width: 275, height: 450, effects: effectsDemo2,
                        display: {
                          autoplay: 4000,
                          autostart: true,
                          first_slide: true,
                          loading: {background: '#000000', opacity: 0.5, image: '<?php echo base_url();?>assets/img/loading.gif'},
                          buttons: {hide: false, opacity: 1, prev: {className: 'SliderNamePrev_2', label: ''}, next: {className: 'SliderNameNext_2', label: ''}},
                          description: {hide: false, background: '#000000', opacity: 0.4, height: 50, position: 'top'},
                          navigation: {container: 'SliderNameNavigation_2', label: '<img src="<?php echo base_url();?>assets/img/clear.gif" />'}
                          
                        },                      

                      });

                      // building navigation manually
        // var demoSlider_2TotalSlides = demoSlider_2.get('length');
        // // alert(demoSlider_2TotalSlides);
        // var demoSlider_2Navigation = $("#SliderName_2_navigation");
      

        // // adding pages to the navigation bar
        // for (var i = 0; i < demoSlider_2TotalSlides; i++)
        // {
        //   demoSlider_2Navigation.append('<a href="javascript:void()" class="SliderName_2_page" id="SliderName_2_page_' + i + '">' + (i+1) + '</a>');
        // }
        return demoSlider_2;
      }
      </script>
    <!--SCRIPTS -->
  </body>
</html>

