<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
          </div>
          <?php 
        }
        ?>
      <form id="registraform" class="login100-form" role="form" action="<?php echo site_url('/usuarios/grabarAsignacion') ?>" method="post">
            <!-- section title -->
            <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
                <h2>Asignar <span class="color">vehículo</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <div class="input-group">
                <?php 
                if (isset($message_display)) { ?>
                    <div class="text-center p-t-12">
                      <span class="txterror"><?php echo $message_display;?>
                      </span>
                    </div>
                <?php }?>
            </div>

            <div class="wrap-input80">
              <input class="input input100" type="text" id="operador" name="operador" placeholder="Nombre del Operador" onkeyup="buscar(1)">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-search" aria-hidden="true"></i>
              </span>
            </div>
            
            <div class="table-responsive wrap-input100" id="country_table">
              <table class='table table-hover' id='tblOperadores' name='tblOperadores'>
                <thead>
                  <tr>
                    <th scope='col'>Id</th>
                      <th scope='col'>Nombre</th>
                      <th scope='col'>Seleccionar</th>
                    </tr>
                </thead>
              </table>
            </div>
            <input type="hidden" name="hdIdOperador" id="hdIdOperador" value="0">
            <div align="right-auto" id="pagination_link"></div>

            <div class="wrap-input80">
              <input class="input input100" type="text" id="placa" name="placa" placeholder="Número de placa" onkeyup="buscarveh()">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-search" aria-hidden="true"></i>
              </span>
            </div>

            <div class="table-responsive wrap-input100" id="country_table_veh">
              <table class='table table-hover' id='tblVehiculos' name='tblOpetblVehiculosradores'>
                <thead>
                  <tr>
                    <th scope='col'>Placa</th>
                    <th scope='col'>Descripción</th>
                    <th scope='col'>Seleccionar</th>
                  </tr>
                </thead>
              </table>
            </div>

            <input type="hidden" name="hdIdVehiculo" id="hdIdVehiculo" value="0">

            <div class="container-login50-form-btn">
                <button class="login50-form-btn">
                    Aceptar
                </button>
                <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel"  value="Cancelar"/>
            </div>

            <div style="display: none;" id="codigo_error">
                <small class="help-block text-center p-t-12 txterror" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ha ocurrido un error, por favor intente más tarde</small>   
            </div>
          </form>                   
        </div>  
    </div>
</div>

<script>
  $('#btn-cancelar').click(function(e) 
  {
      window.location="<?php echo base_url() ?>index.php/usuarios/index";
  });

    function valida(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla ==8 || tecla==9){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        //alert(tecla);
        tecla_final = String.fromCharCode(tecla);
        //alert(tecla_final);
        return patron.test(tecla_final);
        
        /*var key = evt.keyCode;
        alert(key);
        return (key >= 48 && key <= 57);*/

    }

    $('#btn-cancelarAuto').click(function(e) 
    {
      window.location="<?php echo base_url() ?>index.php/usuarios/nuevo_operador_vehiculo";
    });
    
    function nuevovehiculo()
    {
      window.location="<?php echo base_url() ?>index.php/usuarios/nuevovehiculo";
    }

    function buscar(page)
    {
        var texto = $("#operador").val();

        if(texto == '')
            texto = '*';

        $.ajax({
          type: "post",
          data: { 'texto' : texto }, 
          url: "<?php echo base_url() ?>index.php/usuarios/buscarparcial/"+page,
          dataType: "json",
          success: function (data) {
            $('#country_table').html(data.country_table);
            $('#pagination_link').html(data.pagination_link);
          },
          error: function(xhr, ajaxOptions, throwError){
            $("#codigo_error").css({"visibility": ""});
            //alert(throwError);
          }
        });
    }

    $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      var page = $(this).data("ci-pagination-page");
      buscar(page);
     });

    function selectoperador(idOperador)
    {
        $('#hdIdOperador').val(idOperador);
        //$('#categoria').focus();
    }


    function buscarveh()
    {
        var texto = $("#placa").val();

        if(texto.length > 3){
            if(texto == '')
                texto = '*';

            $.ajax({
              type: "post",
              data: { 'texto' : texto }, 
              url: "<?php echo base_url() ?>index.php/usuarios/buscarparcialveh/",
              dataType: "json",
              success: function (data) {
                $('#country_table_veh').html(data.country_table_veh);
              },
              error: function(xhr, ajaxOptions, throwError){
                $("#codigo_error").css({"visibility": ""});
                //alert(throwError);
              }
            });
        }
    }

    function selectvehiculo(idVehiculo)
    {
        $('#hdIdVehiculo').val(idVehiculo);
        //$('#categoria').focus();
    }
</script>
