        <div class="qr">
            <div class="row">
                <div class="col"></div>
                <div class="col-12 col-md-6 col-lg-5 text-center">
                    <label ><?php echo ucfirst($this->lang->line('label_1')); ?></label>
                    <div class="row">
                        <div class="col-8 col-md-8 col-lg-8">
                            <input id="code" type="text"  class="form-control"> 
                        </div>
                        <div class="col-4 col-md-4 col-lg-4">
                            <button id="send" type="button" class="btn btn-round btn-block">
                                Enviar
                            </button>                            
                        </div>
                    </div>
                </div>
                <div class="col"></div>
            </div>
            <!-- </div> -->
            <div class="row">
                <div class="col"></div>
                <div class="col text-center">
                    <label for=""><?php echo ucfirst($this->lang->line('label_2')); ?></label>
                </div>
                <div class="col"></div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col-12 col-md-6 col-lg-5 text-center">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <a id="scanner" href="<?php base_url() ?>/index.php/scanner">
                                <button class="btn btn-round btn-block"><?php echo $this->lang->line('link_1'); ?></button>
                            </a>
                        </div>
                    </div>                            
                </div>
                <div class="col"></div>    
            </div>
        </div>
        <!-- /.container -->
        
    

<div class="modal" tabindex="-1" role="dialog" id="myModal"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php  echo $this->lang->line('modal_title'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-left">
        <p id="text"><?php echo $this->lang->line('modal_text'); ?></p>
      </div>
      
    </div>
  </div>
</div>
<footer>
    <strong><label for=""><?php echo ucfirst($this->lang->line('footer')); ?></label></strong>
</footer>    
<!-- /section -->
<!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
<script src="<?php echo base_url() ?>assets/js/axios.min.js"></script>

<script>
    $(document).ready(()=> {
    
        let $id = null;
        // $("#code").change(()=>{
        //     validate()
        // });
       
        $("#send").click(()=>{
            // alert("Nice");
            validate();
        });

        function validate(){    
            let input = $("#code").val();
            // console.log(input)
                if(input != ''){        
                    axios({
                        method: 'get',
                        url: '/usuarios/rest_validation/'+input,
                        baseURL: '<?php base_url(); ?>/index.php',
                        headers: {'Content-Type': 'multipart/form-data' },           
                    }).then(function (response) {
                        obj = response.data;

                        // console.log(obj.warning)
                        if(response.data.warning){                            
                            $("#myModal").modal();
                        }
                        else{
                            // console.log(obj.data[0].restaurantes_url);
                            // console.log(obj);
                            if($id == null){
                                get_order(input,obj.data[0].restaurantes_url);
                            }
                            
                            // window.location.href = obj.data[0].restaurantes_url;
                        }
                        // console.log(obj);
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });                 
                }    
        }

        async function get_order(code,URL){
          return await axios({
              method: 'get',
              url: 'api/restaurant/'+code+'/'+1,
              baseURL: URL,              
            //   baseURL: 'https://www.elositopolar.com/index.php/',
              headers: {
                'Content-Type': 'application/json',                                
              }                         
            }).then(function (response) {
            // console.log(true)
            // console.log(response.data.id);
            // console.log(response.data.status);

                $id = response.data.id;

                if(response.data.status){
                    console.log(URL);
                    window.location.href = URL+'/restaurante_inicio/access_control/'+response.data.id;
                }else{
                    
                }
                          
          })
          .catch(function (error) {
              console.log(error);              
              document.getElementById("text").innerHTML = "<?php echo $this->lang->line('modal_text2'); ?>";
              $("#myModal").modal();                
          }); 

          // await console.log(response);
        }
    });
</script>
