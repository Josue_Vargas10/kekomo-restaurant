<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
        <form id="contact-form" class="login100-form" role="form" action="<?php echo site_url('/usuarios/nuevo_operador_registro') ?>" method="post" enctype="multipart/form-data">
            <!-- section title -->
            <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
                <h2>Registrar <span class="color">operador</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <div class="input-group">
                <?php 
                if (isset($message_display)) { ?>
                    <div class="text-center p-t-12">
                      <span class="txterror"><?php echo $message_display;?>
                      </span>
                    </div>
                <?php }?>
            </div>

            <div class="wrap-input80">
              <input class="input input100" id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre completo" value="<?php if (isset($nombre)) {echo $nombre;}?>">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-user" aria-hidden="true"></i>
              </span>
            </div>
            <div  class="input-group">
                <?php echo form_error('nombre', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>

            <div class="wrap-input80">
              <input class="input input100" type="text" id="usuario" name="usuario" placeholder="Usuario" onkeydown="return validaletras(event)" onblur="validausuario();" value="<?php if (isset($usuario)) {echo $usuario;}?>">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-user" aria-hidden="true"></i>
              </span>
            </div>
             <div  class="input-group">
                <?php echo form_error('usuario', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>
            <div style="display: none" id="erroruser">
                <small class="help-block" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ya existe un registro con ese usuario</small>   
            </div>
            <div style="display: none" id="erroruserempty">
                <small class="help-block" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Debe ingresar un nombre de usuario</small>   
            </div>

            <div style="display: none" id="erroruserblanco">
                <small class="help-block" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">El usuario no debe contener espacios en blanco</small>   
            </div>

            <div class="wrap-input80">
                <input class="input input100" type="password" id="password" name="password" placeholder="Contraseña" value="<?php if (isset($password)) {echo $password;}?>">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
            </div>
            <div  class="input-group">
                <?php echo form_error('password', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>

            <div class="wrap-input80">
                <input class="input input100" type="password" id="confirmacion" name="confirmacion" placeholder="Confirma tu contraseña">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
            </div>
            <div class="input-group">
                <?php echo form_error('confirmacion', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>

            <div class=" wrap-input80">
                <input class="input input100" type="text" id="telefono" name="telefono" name="telefono" placeholder="Celular" maxlength="10" onkeydown="return valida(event)" value="<?php if (isset($telefono)) {echo $telefono;}?>">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </span>
            </div>
            
            <div class="input-group">
                <?php echo form_error('telefono', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>

            <h5 style="color:#fff;padding-bottom: .7vw; text-align: center;">Subir gafete</h5>

             <div class="wrap-input80 input-group">
                <label class="login20-form-btn login20-form-btn-cancel input-group-btn">
                    <span  >
                        Buscar&hellip; <input type="file" style="display: none;" id="archivo" name="archivo">
                    </span>
                </label>
                <input type="text" class="input input80" id="narchivo" name="narchivo" placeholder="Ruta..." disabled>
            </div>

            <div class="input-group">
                <?php echo form_error('archivo', '<div style="color:red">', '</div>'); ?>
            </div>

            <h5 style="color:#fff;padding-bottom: .7vw; text-align: center;">Datos de la unidad</h5>

            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <input class="input input100" type="text" id="placas" name="placas" placeholder="Placas" value="<?php if (isset($placas)) {echo $placas;}?>" onblur="validaplaca();">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-car" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="wrap-input40">
                    <input class="input input100" type="text" id="modelo" name="modelo" placeholder="Modelo" maxlength="4" onkeydown="return valida(event)" value="<?php if (isset($modelo)) {echo $modelo;}?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-car" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <?php echo form_error('placas', '<div style="color:red">', '</div>'); ?>
                </div>
                <div class="wrap-input40">
                    <?php echo form_error('modelo', '<div style="color:red">', '</div>'); ?>
                </div>
            </div>

            <input type="hidden" name="hdExiste" id="hdExiste" value="<?php if (isset($hdExiste)) {echo $hdExiste;}?>" onblur="validaplaca();">

            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <input class="input input100" type="text" id="sitio" name="sitio" placeholder="Sitio" value="<?php if (isset($sitio)) {echo $sitio;}?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-car" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="wrap-input40">
                    <input class="input input100" type="text" id="numero" name="numero" placeholder="Número" value="<?php if (isset($numero)) {echo $numero;}?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-car" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <?php echo form_error('sitio', '<div style="color:red">', '</div>'); ?>
                </div>
                <div class="wrap-input40">
                    <?php echo form_error('numero', '<div style="color:red">', '</div>'); ?>
                </div>
            </div>

            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <select class="input input100" id="cmbTipo" name = "cmbTipo">
                        <option value="0" disabled selected hidden>-- Tipo de Servicio --</option>
                        <option value="1">Taxi Privado</option>
                        <option value="2">Repartidor</option>
                        <option value="3">Carga</option>
                        <option value="4">Construcción</option>
                        <option value="5">Viajes</option>
                        <option value="6">Mudanza</option>
                    </select>

                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="wrap-input40">
                    <div class="txt1 label-select-auto">
                        <select class="input input100" id="tipo_vehiculo" name="tipo_vehiculo">
                        <option value="0" >--Tipo de vehículo--</option>
                        </select>
                    </div>
                    <span class="symbol-input100">
                        <i class="fa fa-car" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <?php echo form_error('cmbTipo', '<div style="color:red">', '</div>'); ?>
                </div>
                <div class="wrap-input40">
                    <?php echo form_error('color', '<div style="color:red">', '</div>'); ?>
                </div>
            </div>

            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <input class="input input100" type="text" id="marca" name="marca" placeholder="Marca" value="<?php if (isset($marca)) {echo $marca;}?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-car" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="wrap-input40">
                    <input class="input input100" type="text" id="linea" name="linea" placeholder="Línea" value="<?php if (isset($linea)) {echo $linea;}?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-car" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="wrap-input40">
                    <input class="input input100" type="text" id="color" name="color" placeholder="Color" value="<?php if (isset($color)) {echo $color;}?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-paint-brush" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
            <div class="container-login100-form-input">
                <div class="wrap-input40">
                    <?php echo form_error('marca', '<div style="color:red">', '</div>'); ?>
                </div>
                <div class="wrap-input40">
                    <?php echo form_error('linea', '<div style="color:red">', '</div>'); ?>
                </div>
            </div>

            <div class="container-login50-form-btn">
                <button class="login50-form-btn" id="btn-entrar">
                    Registrarse
                </button>
                <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel"  value="Cancelar"/>
            </div>

          </form>                   
        </div>  
    </div>
</div>

<script>
/* al seleccionar opcion de 'cmbTipo' 
obtener las opciones disponibles para select 'tipo_vehiculo' */
$("#cmbTipo").change(function() { 
    //este es el elemento select
    let select_tip_vhc=$("#tipo_vehiculo");
    //tomar valor seleccionado de cmbTipo 
    let auto_selected=$("#cmbTipo option:selected").val();
    let auto_selectext=$("#cmbTipo option:selected").text();

if(auto_selectext=="-- Tipo de Servicio --"){
  ErrorCustom('Debes seleccionar un servicio');
}else{
    $.ajax({
          type: "POST", 
          dataType: "json",
          url: "<?=base_url()?>index.php/usuarios/vehiculos_disponibles/"+auto_selected,
          success: function (data) {

          //actualizo los valores del select tipo_vehiculo
                //limpio valores
                select_tip_vhc.empty();
                //actualizo valores
                $.each(data, function(key,value) {
                select_tip_vhc.append($("<option></option>")
                  .attr("value",this.id).text(this.tipo_vehiculo));
                });
          },
          error: function(xhr, ajaxOptions, throwError){
              alert("err:"+throwError);
          }
        });
      }
})






    $('#btn-cancelar').click(function(e) 
    {
        window.location="<?php echo base_url() ?>index.php/usuarios/index";
    });

    $(function() {
      // We can attach the `fileselect` event to all file inputs on the page
      $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/');
        input.trigger('fileselect', [numFiles, label]);
      });
    });
    $(document).ready(function(){
         $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.val(log);
              } else {
                  if( log ) alert(log);
              }
          });

         if($('#hdExiste').val() == "1")
         {
            $('#modelo').prop( "disabled", true );
            $('#sitio').prop( "disabled", true );
            $('#numero').prop( "disabled", true );
            $('#color').prop( "disabled", true );
            $('#marca').prop( "disabled", true );
            $('#linea').prop( "disabled", true );
            $('#cmbTipo').prop( "disabled", true );
        }
        else
        {
            $('#modelo').prop( "disabled", false );
            $('#sitio').prop( "disabled", false );
            $('#numero').prop( "disabled", false );
            $('#color').prop( "disabled", false );
            $('#marca').prop( "disabled", false );
            $('#linea').prop( "disabled", false );
            $('#cmbTipo').prop( "disabled", false );
        }
    });
        
    function valida(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla ==8 || tecla==9){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        //alert(tecla);
        tecla_final = String.fromCharCode(tecla);
        //alert(tecla_final);
        return patron.test(tecla_final);
        
        /*var key = evt.keyCode;
        alert(key);
        return (key >= 48 && key <= 57);*/

    }

    function validaletras(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
       if (tecla ==8 || tecla==9){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);

        /*var key = evt.keyCode;
        alert(key);
        return ((key >= 48 && key <= 57) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122));*/
    }

    function validausuario(){
        
        var nombre = $('#usuario').val().toString();
        $("#erroruserempty").prop( "style", "display:none");

        if(numero != '' && nombre != '')
        {
            $.ajax({
              type: "POST",
              url: "<?php echo base_url() ?>index.php/usuarios/existe_operador/"+nombre,
              dataType: "json",
              success: function (data) {
                //alert("SUCCESS:");
                //alert(data['return']);
                
                if(data['return'] == 0){
                    $('#btn-entrar').prop( "disabled", false );
                    $("#erroruser").prop( "style", "display:none");
                }
                else{
                    $('#btn-entrar').prop( "disabled", true );
                    $("#erroruser").prop( "style", "display:block");
                    $("#usuario").focus();
                    return false;
                }
              },
              error: function(xhr, ajaxOptions, throwError){

                //alert(throwError);
              }
            });
        }            
    }
    function validaplaca(){
        
        var placa = $('#placas').val().toString();
       
        if(placa != '')
        {
            $.ajax({
              type: "POST",
              url: "<?php echo base_url() ?>index.php/usuarios/existe_vehiculo/"+placa,
              dataType: "json",
              success: function (data) {

                if(data['encontrado'] == 0){
                    $('#modelo').val("");
                    $('#sitio').val("");
                    $('#numero').val("");
                    $('#color').val("");
                    $('#marca').val("");
                    $('#linea').val("");
                    $('#hdExiste').val("0");
                    document.getElementById('cmbTipo').value='0';
                    document.getElementById('tipo_vehiculo').value='0';

                    $('#modelo').prop( "disabled", false );
                    $('#sitio').prop( "disabled", false );
                    $('#numero').prop( "disabled", false );
                    $('#color').prop( "disabled", false );
                    $('#marca').prop( "disabled", false );
                    $('#linea').prop( "disabled", false );
                    $('#cmbTipo').prop( "disabled", false );
                    $('#tipo_vehiculo').prop( "disabled", false );
                }
                else{
                    descripcion = data['vehiculo'][0]['autosDescripcion'];
                    modelo = descripcion.substring(descripcion.length-4);
                    primerespacio = descripcion.indexOf(" ",0);
                    marca = descripcion.substring(0,primerespacio);
                    linea = descripcion.substring(primerespacio+1,descripcion.length-5);

                    $('#modelo').val(modelo);
                    $('#sitio').val(data['vehiculo'][0]['autosSitio']);
                    $('#numero').val(data['vehiculo'][0]['autosNick']);
                    $('#color').val(data['vehiculo'][0]['autosColor']);
                    $('#marca').val(marca);
                    $('#linea').val(linea);
                    $('#hdExiste').val("1");
                    document.getElementById('cmbTipo').value=data['vehiculo'][0]['autosTipo'];
                    document.getElementById('cmbTipo').value=data['vehiculo'][0]['tipo_auto'];

                    $('#modelo').prop( "disabled", true );
                    $('#sitio').prop( "disabled", true );
                    $('#numero').prop( "disabled", true );
                    $('#color').prop( "disabled", true );
                    $('#marca').prop( "disabled", true );
                    $('#linea').prop( "disabled", true );
                    $('#cmbTipo').prop( "disabled", true );
                    $('#tipo_vehiculo').prop( "disabled", true );
                }
              },
              error: function(xhr, ajaxOptions, throwError){

                //alert(throwError);
              }
            });
        }
        else
        {
            $('#modelo').val("");
            $('#sitio').val("");
            $('#numero').val("");
            $('#color').val("");
            $('#marca').val("");
            $('#linea').val("");
            $('#hdExiste').val("0");
            document.getElementById('cmbTipo').value='0';
            document.getElementById('tipo_vehiculo').value='0';
        }            
    }
</script>
