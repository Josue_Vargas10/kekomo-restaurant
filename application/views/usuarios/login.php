<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">

        <p class="text-right"><?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          //echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];
        }
        ?>
        </p>  
        <form id="contact-form" method="post" action="<?php echo site_url('/index.php/usuarios/proceso_login_usuario') ?>" role="form" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 login100-form"><!-- col-xs-12 col-sm-12 col-md-6 col-lg-8 col-xl-8 -->

            <!-- section title -->
            <div class="title title-login text-center wow fadeInUp" data-wow-duration="500ms">
              <h2>Inicia <span >sesión</span></h2>
              <div class="border"></div>
            </div>
            <!-- /section title -->

      	
            <?php
            if (isset($logout_message)) { ?>
            <div class="text-center p-t-12">
              <span class="txterror"><?php echo $logout_message;?>
              </span>
            </div>
            <?php }
            if (isset($message_display)) { ?>
            <div class="text-center p-t-12">
              <span class="txtsuccess"><?php echo $message_display;?>
              </span>
            </div>
            <?php } 
            if (isset($error_message)) { ?>
            <div class="text-center p-t-12">
              <span class="txterror"><?php echo $error_message;?>
              </span>
            </div>
            <?php }
            ?>

            <input type="hidden" name="hdMensajeError" id="hdMensajeError" value="<?php if (isset($error_message)) echo $error_message; ?>">

            <div class="wrap-input40-login">
              <input class="input input100" type="text" id="celular" name="celular" placeholder="Número de celular">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-mobile" aria-hidden="true"></i>
              </span>
            </div>
            <div class="input-group">
                 <?php echo form_error('celular', '<div class="text-center txterror">', '</div>'); ?>
            </div>
            <div class="wrap-input40-login">
              <input class="input input100" type="password" id="password" name="password" placeholder="Contraseña">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-lock" aria-hidden="true"></i>
              </span>
            </div>
            <div class="input-group">
                 <?php echo form_error('password', '<div class="text-center txterror">', '</div>'); ?>
            </div>
            <div style="display: none" id="codigo_error">
                <small class="text-center txterror" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ha ocurrido un error, por favor intente más tarde</small>   
            </div>

            <div id="cf-submit">
                <input type="hidden" id="hdLatitud" name="hdLatitud" value="<?php if (isset($latitud)) echo $latitud ?>" />
                <input type="hidden" id="hdLongitud" name="hdLongitud" value="<?php if (isset($longitud)) echo $longitud ?>" />
                <input type="hidden" id="hdDomicilio" name="hdDomicilio" value="<?php if (isset($domicilio)) echo $domicilio ?>" />
                <input type="hidden" id="hdOpcion" name="hdOpcion" value="<?php if (isset($opcion)) echo $opcion ?>" />
                <input type="hidden" id="hdOneSignalId" name="hdOneSignalId" value="<?php if (isset($hdOneSignalId)) {echo $hdOneSignalId;}?>" />
            </div> 
            
            <div class="row login-row-form-btn">

              <div class="container-login25-form-btn col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <button class="login50-form-btn" id="btn-entrar">
                  Entrar
                </button>
              </div>
              
              <div class="container-login25-form-btn col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <input type="button" class="registrar50-form-btn" id="btn-registrar" value="Regístrate aquí" style="cursor: pointer;"/>
              </div> 

            </div>


            
            <div class="text-center forgot-pass">
              <span class="txt1">
                Olvidaste tu 
              </span>
              <a class="txt3" id="olvidaste" style="cursor: pointer;">
                Contraseña?
              </a>
            </div>
                        
        </form>

        <!--<div class="cardslogin col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
           <h5 class="cardslogin-title">Nuestros Servicios:</h5>
        
          <div class="cardslogin col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
             
                <div class="row ">
                
                    <div class="cardlogin col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                        <div class="cardlogin-body">
                            <h5 class="cardlogin-title">
                                <div id="car-icon"><i class="icofont-car megaicon"></i></div>
                            </h5>
                            <p class="cardlogin-text">Taxi Privado</p>
                        </div>
                    </div>
                    
                    <div class="cardlogin col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                        <div class="cardlogin-body">
                            <h5 class="cardlogin-title">
                                <div id="moto-icon"><i class="icofont-moto-jaimee megaicon"></i></div>
                            </h5>
                            <p class="cardlogin-text">Repartidores</p>
                        </div>
                    </div>
                    
                    <div class="cardlogin col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                        <div class="cardlogin-body">
                            <h5 class="cardlogin-title">
                                <div id="pickup-icon"><i class="icofont-pickup megaicon"></i></div>
                            </h5>
                            <p class="cardlogin-text">Carga</p>
                        </div>
                    </div>

                    <div class="cardlogin col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                      <div class="cardlogin-body">
                          <h5 class="cardlogin-title">
                              <div id="truckalt-icon"><i class="icofont-truck-alt megaicon"></i></div>
                          </h5>
                          <p class="cardlogin-text">Mudanza</p>
                      </div>
                    </div>

                    <div class="cardlogin col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                      <div class="cardlogin-body">
                          <h5 class="cardlogin-title">
                              <div id="articulated-truck-icon"><i class="icofont-articulated-truck megaicon"></i></div>
                          </h5>
                          <p class="cardlogin-text">Construcción</p>
                      </div>
                    </div>

                    <div class="cardlogin col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                        <div class="cardlogin-body">
                            <h5 class="cardlogin-title">
                                <div id="van-reverse-icon"><i class="icofont-van-back megaicon"></i></div>
                            </h5>
                            <p class="cardlogin-text">Viajes</p>
                        </div>
                    </div>

              </div>
          </div>

        </div> cardslogin -->

      </div><!-- wrap-login100 -->
  </div> <!-- /container -->
</div><!-- limiter -->

            <!-- Modal -->
<form id="myModal" name="myModal" class="modal wrap-modal50 validate-form2" action="<?php echo base_url() ?>index.php/Login/contrasena" method="post">
  <span class="login100-form-title">
    Indica tu número de celular
  </span>

  <div class=" wrap-input100">
    <input class="input input100" type="text" id="telefono" name="telefono" placeholder="Teléfono" maxlength="10" onkeyup="validanumero();" onkeydown="return valida(event)">
    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-phone" aria-hidden="true"></i>
    </span>
  </div>
  
  <div class="container-login50-form-btn">
    <input type="button" id="btnAceptar" class="login50-form-btn" value="Aceptar" disabled/>
    <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel" value="Cancelar"/>
  </div>
</form>


          </section>       
            <!--</div>                     
        </div>  
    </div>-->

    <div id="customalert-container">
      <div id="customalert"></div>
    </div>
    <script>

          /* title-login responsive */
          if (screen.width>=600) {
            var titleSize="6vw"
          }else{
            var titleSize="8vw"}
          var x = document.getElementsByClassName("title title-login text-center wow fadeInUp");
            for(let i of x){
              i.style.fontSize=titleSize;
              /* alert(i.className);  */
            }
          /* icons responsive */
          if (screen.width>=900) {
            var iconSize="7vw"
            
          }else{
            var iconSize="13vw"}
          var x = document.getElementsByClassName("megaicon");
            for(let i of x){
              i.style.fontSize=iconSize;
              /* alert(i.className); */
            }

          function valida(e){
            tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla ==8 || tecla==9){
                return true;
            }
                
            // Patron de entrada, en este caso solo acepta numeros
            patron =/[0-9]/;
            //alert(tecla);
            tecla_final = String.fromCharCode(tecla);
            //alert(tecla_final);
            return patron.test(tecla_final);
            
            /*var key = evt.keyCode;
            alert(key);
            return (key >= 48 && key <= 57);*/

          }

          function validanumero()
          {
              if ($('#telefono').val().length < 10)
              {
                  $('#btnAceptar').prop( "disabled", true );
                  return false;
              }else
              {   
                  $('#btnAceptar').prop( "disabled", false );
                  $('#btnAceptar').focus();
                      
              }
          }

            $('#olvidaste').click(function(e) 
            {
                $("#myModal").modal({backdrop: "static"});
            });


            $('#btnAceptar').click(function(e) 
            {
              var numero = $('#telefono').val().toString();
              $("#myModal").modal('hide');
              $.ajax({
                  type: "POST", 
                  data: { 'texto' : numero }, 
                  url: "<?php echo base_url() ?>index.php/usuarios/contrasena/",
                  dataType: "json",
                  success: function (data) {
                    if(data['return'] == 1){
                        //$("#codigo_error").css({"visibility": "disabled"});
                        
                        $("#codigo_error").prop( "style", "display:none");
                        alert('Se ha enviado un mensaje al número: '+data['codigo']);
                    }
                    else
                    {
                        //$("#codigo_error").css({"visibility": "","color":"#FF0000"});
                        $("#codigo_error").prop( "style", "display:block");
                        //alert('return: '+data['return'])
                        alert(data['codigo']);
                    }
                  },
                  error: function(xhr, ajaxOptions, throwError){
                    //$("#codigo_error").css({"visibility": "","color":"#FF0000"});
                    $("#codigo_error").prop( "style", "display:block");
                    alert(throwError);
                  }
                });
            });

            $('#btn-registrar').click(function(e) 
            { 
              //ExitoCustom("¡Próximamente podrás disfrutar de todos nuestros servicios!","","Ok");
              //alert2("¡Próximamente podrás disfrutar de todos nuestros servicios!","¡Hola!","Ok");
              window.location="<?php echo base_url() ?>index.php/usuarios/nuevo_usuario_registro";
            });

            $('#btn-cancelar').click(function(e) 
            {
              window.location="<?php echo base_url() ?>index.php/usuarios/index";
            });


            function alert2(message, title, buttonText) {
                buttonText = (buttonText == undefined) ? "Ok" : buttonText;
                title = (title == undefined) ? "The page says:" : title;

                let container=$('#customalert-container');
                container.append(jQuery.parseHTML("<div id='customalert'></div>")); 
                let div = $("#customalert");
                div.html(message);
                div.attr('title', title);
                div.dialog({
                    autoOpen: true,
                    modal: true,
                    draggable: false,
                    resizable: false,
                    buttons: [{
                        text: buttonText,
                        click: function () {
                            $(this).dialog("close");
                            container.remove( "#customalert" );
                            container.remove( ".gj-modal" );
                        }
                    }]
                });
                div.dialog();
            }

    </script>
