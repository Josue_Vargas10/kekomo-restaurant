<form id="form_agregar_restaurante">
<ul>
    <li class="li-mdl-rest">
        <span>Nombre Restaurante:</span>
        <input type="text" class="form-control" name="restaurantes_nombre" id="restaurantes_nombre" value="">
    </li>

    <li class="li-mdl-rest">
        <span>Telefono:</span>
        <input type="text" class="form-control" name="restaurantes_telefono" id="restaurantes_telefono" value="">
    </li>

    <li class="li-mdl-rest">
        <span>Contacto:</span>
        <input type="text" class="form-control" name="restaurantes_contacto" id="restaurantes_contacto" value="">
    </li>

    <li class="li-mdl-rest">
        <span>Telefono del contacto:</span>
        <input type="text" class="form-control" name="restaurantes_tel_contacto" id="restaurantes_tel_contacto" value="">
    </li>

    <li class="li-mdl-rest">
        <span>URL:</span> 
        <input type="text" class="form-control" name="restaurantes_URL" id="restaurantes_URL" value="https://www.elositopolar.online/index.php">
    </li>
    
    <li class="li-mdl-rest">
        <span>Palabras clave:</span> 
        <input type="text" class="form-control" name="restaurantes_palabras_clave" id="restaurantes_palabras_clave" value="">
    </li>

    <li class="li-mdl-rest">
        <span>Nombre Google maps:</span> {{$restaurante['place_name']}}
    </li>
    
    <li class="li-mdl-rest">
        <span>Latitud:           </span> {{$restaurante['lat']}}
    </li>
    
    <li class="li-mdl-rest">
        <span>Longitud:          </span> {{$restaurante['lng']}}
    </li>
    
    <li class="li-mdl-rest">
        <span>Dirección completa:</span> {{$restaurante['formatted_address']}}
    </li>
    
    <li class="li-mdl-rest">
        <span>Calle y numero:    </span> {{$restaurante['calle_numero']}}
    </li>
    
    <li class="li-mdl-rest">
        <span>Place ID:          </span> {{$restaurante['place_id']}}
    </li>
    
</ul>
    <input type="hidden" name="cat_estatus_id" id="cat_estatus_id" value="1">
    <input type="hidden" name="servicioPlaceName" id="servicioPlaceName" value="{{$restaurante['place_name']}}">
    <input type="hidden" name="servicioLatitudFin" id="servicioLatitudFin" value="{{$restaurante['lat']}}">  
    <input type="hidden" name="servicioLongitudFin" id="servicioLongitudFin" value="{{$restaurante['lng']}}">
    <input type="hidden" name="restaurantes_domicilio" id="restaurantes_domicilio" value="{{$restaurante['formatted_address']}}">
    <input type="hidden" name="servicioDestinoText" id="servicioDestinoText" value="{{$restaurante['formatted_address']}}">
    <input type="hidden" name="servicioDestinoCalleNumero" id="servicioDestinoCalleNumero" value="{{$restaurante['calle_numero']}}">
    <input type="hidden" name="servicioDestinoPlaceId" id="servicioDestinoPlaceId" value="{{$restaurante['place_id']}}">

    <button type="button" id="btn_guardar_restaurante" class="btn btn btn-primary">Guardar</button>
</form>

<script>
setTimeout(() => {
    $(".add_rest_modal").css("overflow-y","hidden");
}, 200);

$("#btn_guardar_restaurante").click(function(){
    let formdata=$("#form_agregar_restaurante").serialize();
    var validar_campos=["restaurantes_nombre","restaurantes_telefono","restaurantes_contacto","restaurantes_tel_contacto","restaurantes_URL","restaurantes_palabras_clave"];
    var cont=0;
    $.each(validar_campos,function(index,value){

        let inputval=$("#"+value).val();
        if(inputval.length<=0){
            cont++
        }
    });
  
    if(cont>0){
        ErrorCustom("Por favor ingrese todos los campos","");
    }else{

        ajaxJson(
            site_url+"index.php/usuarios/agregar_restaurante",
            formdata, 
            "POST", 
            true, 
            function(result){

        });
    }

});
</script>



