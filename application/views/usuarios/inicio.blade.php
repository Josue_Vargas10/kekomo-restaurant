        <div class="row">
            <div class="col-12">
                <h1 id="main_label" class=" text-center"><?php echo $this->lang->line('main_label'); ?></h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="row option mt-4">
            <div class="col"></div>
            <div id="restaurante" class="col"></div>
            <div class="col-8 col-md-7 col-lg-6">
                <button type="button" id="btn-resto" class="btn btn-round btn-block">
                  <label><?php echo $this->lang->line('button_1'); ?></label>
                </button>
            </div>
            <div class="col"></div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col"></div>
            <div class="col-10">
                <hr>
            </div>
            <div class="col"></div>
        </div>
        <!-- /.row -->
        <div class="row option">
            <div class="col"></div>
            <div id="car" class="col"></div>
            <div class="col-8 col-md-7 col-lg-6">
                <button type="button"  id="btn-domicilio"  class="btn btn-round btn-block">
                   <label><?php echo $this->lang->line('button_2'); ?></label>
                </button>
            </div>
            <div class="col"></div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col"></div>
            <div class="col-10">
                <hr>
            </div>
            <div class="col"></div>
        </div>
        <!-- /.row -->
        <div class="row option">
              <div class="col"></div>
              <div id="bag" class="col"></div>
              <div class="col-8 col-md-7 col-lg-6">
                  <button  id="btn-recoger" type="button" class="btn btn-round btn-block">
                    <label><?php echo $this->lang->line('button_3'); ?></label>
                  </button>
              </div>
              <div class="col"></div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col"></div>
            <div class="col-10">
                <hr>
            </div>
            <div class="col"></div>
        </div>
        <!-- /.row -->
        <!-- /.row -->
        <div class="row option">
              <div class="col"></div>
              <div id="dry-thru" class="col"><i class="fa fa-car"></i> </div>
              <div class="col-8 col-md-7 col-lg-6">
                  <button id="btn-drive-thru" type="button" class="btn btn-round btn-block">
                   <label><?php echo $this->lang->line('button_4'); ?></label>
                  </button>
              </div>
              <div class="col"></div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<!-- /section -->

<input id="userAgent" type="text" value="<?php echo $browser['name'] ?>" hidden />
<script>
    var browser = "<?php echo $browser['name'] ?>";
    agent = "<?php echo $agent ?>";
    // const installedApps = navigator.getInstalledRelatedApps();
    // const nativeApp = installedApps.find(app => app.id === 'kekomo.restaurant');
    // var request = window.navigator.mozApps.getInstalled();
    // alert(browser +"\n---\n" +    
    //       agent   +"\n---\n" 
    //       );
   

    
</script>
