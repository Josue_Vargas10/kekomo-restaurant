  <script src="<?php echo base_url(); ?>assets/js/jsQR.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">

  <div id="loadingMessage">🎥 Unable to access video stream (please make sure you have a webcam enabled)</div>
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h3 id="instruction">Escanea tu codigo QR..</h3>
        </div>
    </div>
    <div class="row">
      <div class="col-12 text-center">
        <canvas id="canvas" hidden></canvas>
        </div>
    </div>
    <div class="row">
      <div class="col-12 text-center">
        <div id="output" hidden>
          <div class="d-block" id="outputMessage">No QR code detected.</div>
          <div hidden><b>Data:</b> <span id="outputData"></span></div>
        </div>
        </div>
    </div>
      </div>
    </div>
  </div>
  
  <div class="modal" tabindex="-1" role="dialog" id="myModal"> 
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><?php  echo $this->lang->line('modal_title'); ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
          <p id="text"><?php echo $this->lang->line('modal_text'); ?></p>
        </div>      
      </div>
    </div>
  </div>  
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>  
  <script>
    // var reqanimationreference;
    var flag = 0;
    let $id = null;
    $(".close").click(()=>{
      flag=0;    
    });

    
    var video = document.createElement("video");
    var canvasElement = document.getElementById("canvas");
    var canvas = canvasElement.getContext("2d");
    var loadingMessage = document.getElementById("loadingMessage");
    var outputContainer = document.getElementById("output");
    var outputMessage = document.getElementById("outputMessage");
    var outputData = document.getElementById("outputData");

    function drawLine(begin, end, color) {
      canvas.beginPath();
      canvas.moveTo(begin.x, begin.y);
      canvas.lineTo(end.x, end.y);
      canvas.lineWidth = 4;
      canvas.strokeStyle = color;
      canvas.stroke();
    }


    // Use facingMode: environment to attemt to get the front camera on phones
    navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function(stream) {
      video.srcObject = stream;
      video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
      video.play();
      requestAnimationFrame(tick);
      // console.log(reqanimationreference);
    });

    function tick() {
      loadingMessage.innerText = "⌛ Loading video..."
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        loadingMessage.hidden = true;
        canvasElement.hidden = false;
        outputContainer.hidden = false;

        // canvasElement.height = video.videoHeight;
        // canvasElement.width = video.videoWidth;
        canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
        var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
        var code = jsQR(imageData.data, imageData.width, imageData.height);
        if (code) {                    
          // console.log(flag);
          drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
          drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
          drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
          drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");
          outputMessage.hidden = true;
          // return code.data;
          // cancelAnimationFrame(reqanimationreference);
          if(flag == 0){
            qr_validation(code.data);  
            flag =1;
          }          
        } else {
          outputMessage.hidden = false;
          // outputData.parentElement.hidden = true;
        }        
      }
      requestAnimationFrame(tick);
    }

    async function qr_validation(code){
      await axios({
            method: 'get',  
            url: '/usuarios/rest_validation/'+code,
            baseURL: '<?php base_url(); ?>/index.php',
            headers: {'Content-Type': 'multipart/form-data' },           
        }).then(function (response) {
                        
          if(response.data.warning){                            
              $("#myModal").modal();                        
          }
          else{
              obj = response.data;
              if($id == null){
                get_order(code,obj.data[0].restaurantes_url);                                   
              }
          }              
      })
      .catch(function (error) {
          console.log(error);
      })
      .then(function () {
          // always executed
      });      


          // console.log(r);
    }

              
    async function get_order(code,URL){
      await axios({
          method: 'get',
          url: 'api/restaurant/'+code+'/'+1,            
          baseURL: URL,
          headers: {
            'Content-Type': 'application/json',                                                
          }                         
        }).then(function (response) {
          // console.log();
          $id = response.data.id;
          if(response.data.status){
            // alert("buena esa");
            window.location.href = URL+'/restaurante_inicio/access_control/'+response.data.id;
          }            
      })
      .catch(function (error) {
          console.log(error);                      
          document.getElementById("text").innerHTML = "<?php echo $this->lang->line('modal_text2'); ?>";              
          $("#myModal").modal(); 
      }); 
      
    }
    
  </script>  