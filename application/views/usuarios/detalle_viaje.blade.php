<?php
$coma=($datos['origin_texto']!="" && isset($datos['origin_texto']))?", ":"";
$origen=$datos['origin_texto'].$coma.$datos['origin_calle_numero'];
?>

<h6 style="font-size: 15px; color: #723F19;font-weight: 1000;">Origen:</h6>  <span style="font-size:12px"><?=$origen?></span>
<h6 style="font-size: 15px; color: #723F19;font-weight: 1000;">Destino:</h6> <span style="font-size:12px">{{$datos['destination_name']}} , {{$datos['destination_calle_numero']}}</span>
<br>
<span style="font-size:12px">{{$datos['palabras_clave']}}</span>
<br>
<!--<span style="text-align: right !important;font-size: 20px;font-weight: bold"></span> Costo aprox.-->



<?php if(!isset($datos['servicio_kekomo'])){

	$_SESSION["tipo_servicio"]["servicio_kekomo"]="domicilio";
	$datos['servicio_kekomo']="domicilio";
}

	?>
@if($datos['tipo_ticket']=="Taxi Privado" || $datos['tipo_ticket']=="Repartidores")
	<?php
		$span_precio="$".round($datos['precio_viaje_min'],0,PHP_ROUND_HALF_EVEN)." - $".
						round($datos['precio_viaje_max'],0,PHP_ROUND_HALF_EVEN)." aprox.";
		$precio_minimo=round($datos['precio_viaje_min'],0,PHP_ROUND_HALF_EVEN);
		$tiempo_viaje=",   Tiempo aproximado: ".$datos['duracion_trayecto'];
		$distancia_viaje="Distancia: ".$datos['distancia_trayecto'];
		//$label_pago="*A continuación serás redirigido al menú del restaurante que seleccionaste...";
		$label_pago="";
		$tiempo_y_distancia=$distancia_viaje.$tiempo_viaje;
	?>
	@if($datos['tipo_ticket']=="Taxi Privado")
		<?php
			$label_for_textarea="Indicaciones para el chofer:";
			$ej_for_textarea="ej. casa en la esquina color rojo";
			$icon_class="car";
		?>
	@else
		@if($datos['servicio_kekomo']=="domicilio")
		<?php
			$label_for_textarea="Detalles de la entrega:";
			$ej_for_textarea="ej. Edificio 'C', departamento de informática. pregunte por ing. Alejandro";
			$icon_class="moto-jaimee";
		?>
		@endif
		@if($datos['servicio_kekomo']=="drive_thru")
		<?php
			$label_for_textarea="Detalles de la entrega:";
			$ej_for_textarea="ej. A nombre de  Juan, llego en un nissan sentra rojo, camisa blanca";
			$icon_class="car";
		?>
		@endif
		@if($datos['servicio_kekomo']=="recoger")
		<?php
			$label_for_textarea="Detalles de la entrega:";
			$ej_for_textarea="ej. A nombre de  Juan, llevo camisa blanca";
			$icon_class=" fa fa-shopping-bag";
			$tiempo_y_distancia="";
		?>
		@endif
	@endif
@else
		<?php 
			//$span_precio="$ -Precio por cotizar-";
			$span_precio="";
			$tiempo_viaje="";
			$distancia_viaje="";
			$label_pago=" *Serás contactado por alguno de nuestros proveedores";
		?>
@endif

@if($datos['tipo_ticket']=="Carga")
	<?php
		$label_for_textarea="Describe qué es lo que deseas transportar:";
		$ej_for_textarea="Necesito llevar un ropero";
		$icon_class="pickup";
	?>
@endif

@if($datos['tipo_ticket']=="Construcción")
	<?php
		$label_for_textarea="Describe lo que deseas transportar:";
		$ej_for_textarea="Arena, grava...";
		$icon_class="articulated-truck";
	?>
@endif

@if($datos['tipo_ticket']=="Viajes")
	<?php
		$label_for_textarea="Escribe la fecha de inicio y termino de tu viaje:";
		$ej_for_textarea="Salida: 16 de Agosto de 2020 \n Llegada: 20 de Agosto d 2020";
		$icon_class="van-back";
	?>
@endif
@if($datos['tipo_ticket']=="Mudanza")
	<?php
		$label_for_textarea="Describe lo que deseas transportar:";
		$ej_for_textarea="Muebles del hogar, taller, oficina...";
		$icon_class="truck-alt";
	?>
@endif

<!-- <span style="font-size: 18px;color: blue;font-weight: bold;color:#2F8977">{{$span_precio}}
</span> -->

<span id="dist_y_tiempo" style="font-size: 12px;/* color: #723F19 */;font-weight: 1000"><?=$tiempo_y_distancia?></span>
	<form id="frm-confirmation">
	<select name="select_serv_kekomo" id="select_serv_kekomo" class="form-control col-lg-3 col-md-3 col-sm-6" style="/* max-width:25vw */">
	<?php
		$disponible=true;
		$no_encontrado="<i class='icofont-exclamation-tringle'></i> El servicio que seleccionó (".$datos['servicio_kekomo'].") no está disponible en el restaurante por ahora, por favor intente mas tarde o seleccione entre los dispibles en el desplegable"; 
		foreach ($datos['servicios_disponibles'] as $kkmo_serv) {
			$selected="";
			switch ($kkmo_serv) {
				case 'domicilio':
					$text_serv="A domicilio";
					break;
				case 'recoger':
					$text_serv="Por recoger";
					break;
				case 'drive_thru':
					$text_serv="Servicio en tu auto";
					break;
				default:
					$kkmo_serv=-1;
					$text_serv="el restaurante no tiene servicio en este momento";
					$selected="selected='selected'";
					$disponible=false;
					break;
			}
			if($kkmo_serv==$datos['servicio_kekomo']){
				$selected="selected='selected'";
			}
			echo '<option value="'.$kkmo_serv.'" '.$selected.'>'.$text_serv.'</option>';
		}
	
	?>

	</select>
	<span id="err_no_disponible" style="font-size: 12px;color: navy;font-weight: 1200;width:60%"><?=(!$disponible)?$no_encontrado:""?></span>

			<!-- icono -->
			<!--<div class="cardlogin col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6"> card mudanza -->
			<div class="cardlogin">
				<div class="cardlogin-body">
					<h1 class="cardlogin-title">
						<div id="megaicon_container"><i class="<?=($icon_class==" fa fa-shopping-bag")?$icon_class:"icofont-".$icon_class?> megaicon"></i></div>
					</h1>
				</div>
			</div>


	<div class="row">
		<div class="col-sm-12">
			<label id="txt_area_label" style="font-size: 13px;" >{{$label_for_textarea}}</label>
			<textarea id="txt_area_ej" style="resize:none" name="observaciones" class="form-control" placeholder="{{$ej_for_textarea}}"></textarea>
		</div>
	</div>

	<span style="font-size: 13px;" >{{$label_pago}}</span>
	<input type="hidden" id="tipo_ticket" value="{{$datos['tipo_ticket']}}">
	<input type="hidden" id="cobertura_restaurante" value="{{$datos['cobertura_restaurante']}}">
	<input type="hidden" name="servicio_kekomo" value="{{$datos['servicio_kekomo']}}">
	<input type="hidden" name="origin_lat" value="{{$datos['origin_lat']}}">
	<input type="hidden" name="origin_lng" value="{{$datos['origin_lng']}}">
	<input type="hidden" name="destination_lat" value="{{$datos['destination_lat']}}">
	<input type="hidden" name="destination_lng" value="{{$datos['destination_lng']}}">
	<input type="hidden" name="duracion" 		value="{{$datos['duracion_trayecto']}}">
	<input type="hidden" name="distancia" id="distancia" 		value="{{$datos['distancia_trayecto']}}">
	
	<input type="hidden" id="span_precio_var" value="$<?=$precio_minimo?>">
	<input type="hidden" name="tel_contacto_modal" id="tel_contacto_modal">
	<!-- <div id="myZadarmaCallmeWidget7517"></div> -->

</form>

<script>
	
		/* icons responsive */
		if (screen.width>=900) {//pantalla grande
			var iconSize="8vw";
			var titleSize="20px";
		
		}else{//pantalla chica
			var iconSize="14vw"
			var titleSize="15px";
			$(".modal-dialog").css("padding-right","5.5vw");
			$(".modalConfirm").css("overflow-y","hidden");
			$(".modal-header").css("padding","12px");
		}
		var icons = document.getElementsByClassName("megaicon");
		var modal_title = $(".modal-title");
		

		modal_title.css("font-size",titleSize);
		for(let icon of icons){
			icon.style.fontSize=iconSize;
			/* alert(i.className); */
		}
$("#select_serv_kekomo").val("<?=$datos['servicio_kekomo']?>").trigger('change');
$("#select_serv_kekomo").on("change",function(){
	let this_val=$(this).val();
	let new_dist_tiempo="<?=$distancia_viaje.$tiempo_viaje?>";
	if(this_val!=null){
		//{{$distancia_viaje}}{{$tiempo_viaje}}
		let new_ej="";
		if( this_val=="recoger"){
			new_ej="ej. A nombre de Juan, llevo camisa blanca";
			new_icon="fa fa-shopping-bag";
			new_dist_tiempo="";
		}
		if(this_val=="drive_thru"){
			new_ej="ej. A nombre de Juan, llego en un nissan sentra rojo, camisa blanca";
			new_icon="car";
		}
		if(this_val=="domicilio"){

			new_ej="ej. Edificio 'C', departamento de informática. pregunte por ing. Alejandro";
			new_icon="moto-jaimee";
		}
		let new_label="Detalle de la entrega:";

		//$("#txt_area_ej").removeAttr("placeholder");
		$("#dist_y_tiempo").text(new_dist_tiempo);
		$(".megaicon").removeAttr("id")
						.removeClass()
						.addClass("megaicon");

		if(new_icon!="fa fa-shopping-bag"){
			new_icon="icofont-"+new_icon;

		}

		$(".megaicon").addClass(new_icon);



		$("#txt_area_label").text(new_label);
		$("#txt_area_ej").attr("placeholder",new_ej);

		$("#err_no_disponible").text("");
		ajaxJson(
			site_url+"index.php/usuarios/update_session_servicio_kekomo/"+this_val,
			{}, 
			"POST", 
			true, 
			function(result){

		});//ajaxJson
	}
});

$(".btn-ticket-old").click(function(){
      <?php if(isset($this->session->ticket))
      {
      ?>
		  //console.log("||||||ticket|||||");
		  objTicket=<?=json_encode($this->session->ticket)?>;
			//alert(JSON.stringify(objTicket,0,4));
			customModal(site_url+"/index.php/usuarios/ticket",{objTicket},"POST","lg","","","","","Cerrar","","","modal");
      <?php
    }//end if
    ?>
});
</script>


<script>	
	// agregamos el id que se cargara dinamicamente
	
	widgetId = "<?php echo $datos['zadarmabtn']['widgetId'] ?>";
	sipId  = "<?php echo $datos['zadarmabtn']['sipId'] ?>";
	domElement = "<?php echo $datos['zadarmabtn']['domElement'] ?>";

	if(widgetId.length == 0 || sipId.length == 0 || 
		domElement.length ==  0){
			$(".btn-ticket").hide();		
		}else{
			$(".btn-ticket").attr("id",domElement);

			window[domElement] = null;
			// console.log(domElement + ' = ' + myZadarmaCallmeWidget7525);
			// var myZadarmaCallmeWidget7525;
			var myZadarmaCallmeWidgetFn = function() {
				window[domElement] =  new ZadarmaCallmeWidget(domElement);
				window[domElement].create({
					"widgetId": widgetId, "sipId": sipId, "domElement": domElement }, { "shape":"square", "language":"es", "width":"0", "dtmf":false, "font": "'Trebuchet MS','Helvetica CY',sans-serif", "color_call": "rgb(255, 255, 255)", "color_bg_call": "rgb(126, 211, 33)", "color_border_call": "rgb(191, 233, 144)", "color_connection": "rgb(255, 255, 255)", "color_bg_connection": "rgb(33, 211, 166)", "color_border_connection": "rgb(144, 233, 211)", "color_calling": "rgb(255, 255, 255)", "color_border_calling": "rgb(255, 218, 128)", "color_bg_calling": "rgb(255, 181, 0)", "color_ended": "rgb(255, 255, 255)", "color_bg_ended": "rgb(164,164,164)", "color_border_ended": "rgb(210, 210, 210)"
				});
			}
								
			myZadarmaCallmeWidgetFn();
		}			
  </script>
	
<script>


</script>



  