<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>
<style type="text/css">
	.loader {
	  border: 16px solid #f3f3f3; /* Light grey */
	  border-top: 16px solid #3498db; /* Blue */
	  border-radius: 50%;
	  width: 120px;
	  height: 120px;
    animation: spin 2s linear infinite;
    margin:auto
	}
.loader-center{
  margin:auto;
}
	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
  .img-chofer{
    max-width: 200px;
    max-height: 200px;
    border-radius: 10px;
  }
  .mensaje{
    color:red;
    font-size: 40px;
    text-align: center !important;
  }
</style>
<style type="text/css">
  html {
  box-sizing: border-box;
}

*, *::before, *::after {
  box-sizing: inherit;
  padding: 0;
  margin: 0;
}

body {
  font-family: -apple-system,BlinkMacSystemFont,San Francisco,Helvetica Neue,Helvetica,Ubuntu,Roboto,Noto,Segoe UI,Arial,sans-serif;
}

.hidden {
  display: none;
}

svg {
  width: 20px;
  height: 20px;
  margin-right: 7px;
}

button, .button {
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: auto;
  padding-top: 8px;
  padding-bottom: 8px;
  color: #777;
  text-align: center;
  font-size: 14px;
  font-weight: 500;
  line-height: 1.1;
  letter-spacing: 2px;
  text-transform: capitalize;
  text-decoration: none;
  white-space: nowrap;
  border-radius: 4px;
  border: 1px solid #ddd;
  cursor: pointer;
}

button:hover, .button:hover {
  border-color: #cdd;
}

.share-button, .copy-link {
  padding-left: 30px;
  padding-right: 30px;
}

.share-button, .share-dialog {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

.share-dialog {
  display: none;
  width: 95%;
  max-width: 500px;
  box-shadow: 0 8px 16px rgba(0,0,0,.15);
  z-index: -1;
  border: 1px solid #ddd;
  padding: 20px;
  border-radius: 4px;
  background-color: #fff;
}

.share-dialog.is-open {
  display: block;
  z-index: 2;
}

.datos_chofer{
  color:white;
}
header {
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
}

.targets {
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
  margin-bottom: 20px;
}

.close-button {
  background-color: transparent;
  border: none;
  padding: 0;
}

.close-button svg {
  margin-right: 0;
}

.link {
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
  border-radius: 4px;
  background-color: #eee;
}

.pen-url {
  margin-right: 15px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.titulo{
    color: #4F9CFD !important;
    font-weight: bold;
    background-color: white;
    padding: 2px;
}
.wrap-input100{
  margin-bottom: 0px !important;
}
</style>
<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <div class="input-group">
        <?php
        if (isset($this->session->userdata['loggedin']))
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
          </span>
        </div>
        <?php
      }
      ?>
    </div>
    @if(!$existe_chofer)
    <div id="lbl-espere" class="row" style="margin-bottom: 20px;">
      <div class="col-sm-12">
        <h3 class="text-center" style="color: #4abea7">La asignación del servicio puede tardar hasta 5 minutos, por favor espere…</h3>
      </div>
    </div>
    @endif

    @if(!$existe_chofer)
<div style="width:100%;">
      <div id="div_loader loader-center" >
        <div class="loader"></div>
      <h3 id="sin-chofer">Asignando un chofer...  </h3>
    </div>
</div>
    @endif
    <div id="div_info" class="{{(!$existe_chofer)?'invisible':''}}" style="width: 100%;">
      <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
              <h2><span id="message" class="color">Jaimee!!! ya va en camino</span></h2>
              <div class="border"></div>
      </div>

      <br>
      <div class="row datos_chofer">
        <div class="col-sm-4">
          <div class="row">
            <div class="col-sm-6">
              <img id="chofer_imagen" class="img-chofer" src="<?php echo base_url(); ?>/central_admin/{{($existe_chofer)?$datos_chofer->OperadorImagen:''}}" alt="">
            </div>
          </div>
        </div>
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-6">
              <label>Nombre del chofer:</label> <strong id="chofer_name">
                {{($existe_chofer)?$datos_chofer->operadorNombreCompleto:''}}
              </strong>
            </div>
            <div class="col-sm-6">
              <label>Llamar al chofer: </label> <strong >
                <?php $telefono = 'tel:'.$datos_chofer->operadorTelefono ?>
                <a style="font-size: 20px" id="chofer_telefono" href="{{($existe_chofer)?$telefono:''}}"><i style="font-size: 40px;margin-left:10px;margin-top: 5px" class="fa fa-phone"></i></a>
              </strong>
            </div>
          </div>
        </div>
      </div>
      <h5 style="color: red" id="precio_servicio" class="text-right">{{($total_servicio!='')?'Costo final $:'.number_format($total_servicio,2) : '' }} </h5>
      <div class="container-login100-form-btn">
           <input style="background-color: #57cbcc" type="button" id="btn-ver-auto" class="login100-form-btn" value="Ver vehículo"/>
      </div>
      <div class="container-login25-form-btn">
              <input type="button" class="registrar50-form-btn" id="btn-inicio" value="Ir al inicio" style="cursor: pointer;">
            </div>


      <div class="row">
        <div class="col-sm-12">
          <div class="share-dialog">
            <header>
              <h3 class="dialog-title">Share this pen</h3>
              <button class="close-button"><svg><use href="#close"></use></svg></button>
            </header>
            <div class="targets">
              <a class="button">
                <svg>
                  <use href="#facebook"></use>
                </svg>
                <span>Facebook</span>
              </a>

              <a class="button">
                <svg>
                  <use href="#twitter"></use>
                </svg>
                <span>Twitter</span>
              </a>

              <a class="button">
                <svg>
                  <use href="#linkedin"></use>
                </svg>
                <span>LinkedIn</span>
              </a>

              <a class="button">
                <svg>
                  <use href="#email"></use>
                </svg>
                <span>Email</span>
              </a>
            </div>
            <div class="link">
              <div class="pen-url">https://codepen.io/ayoisaiah/pen/YbNazJ</div>
              <button class="copy-link">Copy Link</button>
            </div>
          </div>
          <br>
          <br>
          <br>
          <button class="share-button hidden" type="button" title="Compartir">
            <svg>
              <use href="#share-icon"></use>
            </svg>
            <span>Compartir información</span>
          </button>

          <svg class="hidden">
            <defs>
              <symbol id="share-icon" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></symbol>

              <symbol id="facebook" viewBox="0 0 24 24" fill="#3b5998" stroke="#3b5998" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></symbol>

              <symbol id="twitter" viewBox="0 0 24 24" fill="#1da1f2" stroke="#1da1f2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></symbol>

              <symbol id="email" viewBox="0 0 24 24" fill="#777" stroke="#fafafa" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></symbol>

              <symbol id="linkedin" viewBox="0 0 24 24" fill="#0077B5" stroke="#0077B5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-linkedin"><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></symbol>

              <symbol id="close" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-square"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="9" x2="15" y2="15"></line><line x1="15" y1="9" x2="9" y2="15"></line></symbol>
            </defs>
          </svg>
        </div>
      </div>
    </div>

  </div>
</div>
</div>



<input type="hidden" name="texto_compartir" id="texto_compartir" value="{{$info_share}}">
<script type="text/javascript">
  var site_url = "{{site_url()}}";
  var base_url = "{{base_url()}}";
  var nombre_usuario = "{{$this->session->userdata['loggedin']['usuario']}}";
  var existe_chofer = "{{$existe_chofer}}"
  var url_gps = "{{$url_gps}}"

function validarChofer(){
  var url = site_url+'/usuarios/validarChofer';
  ajaxJson(url,$("#frm-confirmation").serialize(),"POST","async",function(result){
  	result = JSON.parse( result );
    //1.-pendiente 3.-cancelado 2.-aceptado 4.-rechazado 5.-notificado
      console.log(result);
    console.log(result.estatus);
  	if(result.estatus==2){
  		$("#div_loader").remove();
  		$("#div_info").removeClass('invisible');
  		$("#chofer_name").text(result.chofer.operadorNombreCompleto)

      $("#chofer_telefono").prop('href','tel:'+result.chofer.operadorTelefono);
  		//$("#chofer_telefono").text(result.chofer.operadorTelefono)
  		$("#chofer_imagen").attr('src',base_url+"/central_admin/"+result.chofer.OperadorImagen)
      $("#lbl-espere").empty();

      url_gps = result.url_gps;
      $("#precio_servicio").empty().append('Costo final: $'+ Math.round(result.total_servicio).toFixed(2));
      const datos_compartir = `Hola soy ${nombre_usuario}, quiero compartir los datos de mi viaje: Nombre Chofer: ${result.chofer.operadorNombreCompleto}, Llamar al chofer: ${result.chofer.operadorTelefono}

      `
      $("#texto_compartir").val(datos_compartir);
      $("#sin-chofer").text('');
      setTimeout(validarChofer, 10000);
      // console.log(result.estatus);
  	}else if(result.estatus==1){
      // console.log(result.estatus);
      setTimeout(validarChofer, 10000);
    }else if(result.estatus==3){
      //cancelado
      $("#div_loader").remove();
      $("#div_info").removeClass('invisible').empty().append('Tu servicio fue cancelado por que no se asignó un chofer, por favor inténtalo más tarde').addClass('mensaje');
      $("#chofer_name").text('')
      $("#chofer_telefono").text('')
      $("#chofer_imagen").attr('src','')
      $("#lbl-espere").empty();

    }
    else if(result.estatus==5){
      // console.log(result.estatus);
      // alert("Viaje Iniciado con exito");
      message = "Viaje iniciado con exito.";
      console.log($("#message").text());
      if($("#message").text() != message){
        $("#message").text(message);
      }
      setTimeout(validarChofer, 10000);
    }
    else if(result.estatus==6){
      // console.log(result.estatus);
      message = "Viaje finalizado.Gracias por su preferencia.";
      $("#message").text(message);
      setTimeout(redirigir, 10000 );
    }
  });
}

if(!existe_chofer){
  setTimeout(validarChofer, 10000);
}

$("body").on('click',"#btn-ver-auto",function(){
  if(url_gps!=''){
    window.open(url_gps, '_blank');
  }else{
    window.open("https://jaimee.com.mx/", '_blank');
  }
})
$("body").on('click',"#btn-inicio",function(){
  ConfirmCustom("¿Está seguro de regresar al inicio?", confirmarInicio,"", "Confirmar", "Cancelar");
});
function confirmarInicio(){
  window.location.href = base_url;
}

function redirigir(){
  window.location.replace("https://jaimee.com.mx/");
}


</script>
<script type="text/javascript">
const shareButton = document.querySelector('.share-button');
const shareDialog = document.querySelector('.share-dialog');
const closeButton = document.querySelector('.close-button');

shareButton.addEventListener('click', event => {
  if (navigator.share) {
   navigator.share({
      title: 'WebShare API Demo',
      url: 'https://jaimee.com.mx/',
      text: $("#texto_compartir").val()

    }).then(() => {
      console.log('Thanks for sharing!');
    })
    .catch(console.error);
    } else {
        shareDialog.classList.add('is-open');
    }
});

closeButton.addEventListener('click', event => {
  shareDialog.classList.remove('is-open');
});



var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if( isMobile.any() ) {
  $(".share-button").removeClass('hidden');
}
</script>
