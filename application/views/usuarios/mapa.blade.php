<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
  $userid = ($this->session->userdata['loggedin']['id']);

} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>
<style type="text/css">
      html, body { height: 100%; margin: 0; padding-top: 0; }
      #map { 
        height: 100%;
        margin-top:5%; 
      }

      .panel-default>.panel-heading{
        color: rgba(0, 0, 0, 0.80);
        border-color: none;
        margin-bottom: 10px;
      }
      div #form.panel.panel-default{
        /* top: 10px !important; */
        width: 70%;
        background-color: rgba(241, 133, 54, 0.90);
        /* margin-top:6%; */
        padding: 10px;
        
      }

      .panel{
        border: none;
        border-radius: 0;
      }

      .panel p{
        color: #7e8080;
      }

      .panel-heading{
        border: none;
        border-radius: 0;
      }

      .modal-content{
        color: #ffffff;
        /* background-color: rgba(0, 0, 0, 0.90); */
        background-color: rgba(241, 133, 54, 0.90);
      }
      .padding-left{
        padding-left: 5px;
      }

      .btn-primary{
        color: #ffffff;
        background-color: #4abea7;
        border-color: #4abea7;
      }
  
      .btn-primary:hover{
        background-color: #3ca28e !important;
        border-color: #4abea7 !important;
      }

      .btn-primary:active{
        background-color: #2f8977 !important;
        border-color: #4abea7 !important;
      }
      .btn-secundario{
        background-color: #908D87;
      }
      .panel-default{
        left:15vw !important;
      }
      .panel-title{
      color:#ffffff;
      }
      .panel-body .row{
        width:100%;
        margin-right: 0px; 
        margin-left: 0px;
      }
      .form-inline{
        width:100%
      }
      .form-inline .form-group{
        display: block;
      }

      .form-inline .form-group input{
        width: 100%;
      }
      .form-control:focus{
        background-color: #c9c9c9 !important
      }
      .input_mapa{
        padding-left: .5vw;
        padding-right: .5vw;
      }
      .input_mapa input{
        height: 35px !important;
      }
      .input_price{
          padding-left: 0vw;
        }
      .btnSiguiente{
        padding-right:.8vw;
        padding-left: 0vw;
        padding-top: 1.5vh;
      }
      .input_mapa .form-group{
      padding-top:.8vw;
      }
      .iconContainer{
        padding-left:0vw;
        padding-right:0vw;
        padding-top: 1vh;
      }
      .btnV_FREC{
        padding-bottom: 25px;
        color:#000;
        font-family: Poppins-Medium;
        font-size: 15px;
        line-height: 1.5;
        padding: 5px 10px;
        border-radius: 20px;
        background-color: #e9ecef;
        height: 30px;
        box-shadow: 0px 3px 0px #5aacad;
        transition:all ease 0.3s 0s;
      }
      .btnV_FREC:hover{
        box-shadow: 0 1px 0px #fff;
        padding-top:3px;
        width:57px;
        height:28px;
        padding: 7px 10px;
      }
      .btnV_FREC:focus{
        color:black;
      }
      option{
        background-color: #f2f1f0;
      }
      .myIcofont{
        font-size: 2em;
        color: rgba(0,0,0,.7);
        position: absolute;
        padding-top: .7vh;
        padding-left: .3vw;
      }
      #v_frec_container a{
          color: rgba(0,0,0,.7);
          text-decoration: none;
          position:absolute;
      }
      .select2-container {
        margin-left: 1.5%;
        padding-bottom: 25px;
        color: #000;
        font-family: Poppins-Medium;
        font-size: 15px;
        line-height: 1.5;
        padding: 5px 10px;
        border-radius: 20px;
        background-color: #e9ecef;
        height: 30px;
        box-shadow: 0px 3px 0px #5aacad;
        transition:all ease 0.3s 0s;
      }
      .select2-container:hover{
        box-shadow: 0 1px 0px #fff;
        padding-top:3px;
        width:57px;
        height:28px;
        padding: 7px 10px;
      }
      .select2-container--default .select2-selection--single {
        background-color: transparent;
        border: unset;
        border-radius: unset;
      }
      .select2-container--default .select2-selection--single:focus {
        outline: unset
      }
      .select2-container--default .select2-selection--single .select2-selection__rendered {
        font-family: Poppins-Medium;
        font-size: 15px;
        color: #444;
        line-height: unset;
      }
      .select2-container--default .select2-selection--single .select2-selection__rendered:focus {
        outline: unset;
      }

      input[type=number]::-webkit-inner-spin-button{
        -webkit-appearance: button-arrow-down!important; 
      } 
      input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: button-arrow-down!important; 
      }
      .modal-nuevo-restaurante .modal-content .modal-body{
          max-height:70vh;
          overflow-y:auto;
      }
      .li-mdl-rest{
        /* list-style: inside;
        margin: 0px;
        list-style-type: disc; */
        margin: 0px;
        list-style-type: none;
      }
      .li-mdl-rest span{
        color: #5aacad;
      }

      @media screen and (max-width: 1024px){

      .btnV_FREC{
      padding-right: 1.3vw;
      }
      .btnSiguiente{
        padding-top: 1vh;
        }
      .myIcofont{
        padding-left: .3vw;
      }
      }

      @media screen and (max-width: 768px){
        div #form.panel.panel-default{
          left: 0% !important;
          /* top: 10px !important; */
          width: 100%;
          z-index: 5000 !important;
        }
        .btnSiguiente{
          padding-top: 1.6vh;
          padding-left: .5vh;
          padding-bottom: 1vh;
        }
        .panel-title{
          color:#ffffff;
          font-size: 20px !important;
          text-align: center !important;
        }
        .form-group {
            margin-bottom: 0rem !important;
        }
        .panel-heading{
          margin-bottom: 0px !important;
        }
        div #form.panel.panel-default {
          margin-top: 2vh !important;
        }
        .input_price{
          padding-left: 1.2vw;
          padding-right: .5vw;
        }
        .btnV_FREC{
          width: 61px;
          padding-left: 4vw;
          padding-top: 1vh;
        }
        .iconContainer{
          padding: 0vh 1.3vw;
          width: 60px;
          height: 30px;
        }
        .myIcofont{
          padding-left: .5vh;
        }
      }/* 768px */
      @media screen and (max-width: 550px){
        #map { 
          margin-top:5%; 
        }
        div #form.panel.panel-default {
          margin-top: 3vh !important;
        }
        .btnV_FREC{
          padding-right: 0vh;
          padding-top: 2vh;
          padding-left: 2vw;
        }
        .iconContainer{
          padding-top:.8vh;
        }
        .input_price .form-group{
          padding-left: .5vw;
        }
        .input_price{
          padding-left: 1vw;
        }
        .btnSiguiente{
          padding-right: 1vw; 
          padding-top: 3vh;
        }
        .myIcofont{
          padding-left: .8vw;
          padding-top: 1.2vh;
          font-size: 1.5em;
        }

      }/* 550 */

</style>

<div id="map"></div>
<?php
if($userid="7"){
    ?>
    <div class="btn" id="add_restaurante" style="cursor:pointer"><i class="icofont-save"></i><input type="hidden" id="nvo_restaurante" value="none"></div>
    <script>
      var arrTxtUser=JSON.stringify(<?=json_encode($this->session->userdata['loggedin'])?>)
      //alert(arrTxtUser);
    </script>
<?php } ?>

  <script type="text/javascript">

    var tipo_servicio = "{{$this->session->userdata['tipo_servicio']['tipo_servicio']}}";//tipo_de_servicio
    var site_url = "{{site_url()}}";
    var global_latitud = '';
    var global_longitud = '';
    var global_formatted_address = '';
    var iconBaseA = '{{base_url()}}assets/images/logos/cliente.png';
    var iconBaseB = '{{base_url()}}assets/images/logos/tenedor.png';
    function number_format(amount, decimals) {

        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }

    //Si no está el tipo de auto mandarlo a que lo elija
    if(tipo_servicio=='' || tipo_servicio==undefined){//tipo_de_servicio
      window.location.href = site_url+'/usuarios/index';
    }
  </script>
  

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_gbAjK7KGkfidjIi0iLFqD39VWbhtXcs&libraries=places"></script>
  <script src="{{site_url()}}assets/js/custom/moment.js"></script>
  <script src="{{site_url()}}assets/js/custom/bundle.js?v=<?php echo time();?>"></script>
  <script>
   $( document ).ready(function() {
    //var userLatLng = new google.maps.LatLng(parseFloat("19.2506)"), parseFloat("-103.7415"));
    <?php 
    unset($this->session->userdata['tripdata']);

    ?>

      $.ajax({//ajax origenes frecuentes
        type: "post",
        url: site_url+"index.php/usuarios/obtenerServiciosFrecuentes/"+"servicioOrigenPlaceId",
        dataType: "json",
        success: function (dataOrigin) {

          let select="";
          let optionsOrigin="";

          select +="<option id='no' value='-1' data-trip='not_set' selected disabled>&nbsp&nbsp&nbsp</option>";
          let myselctOrigin=$("#v_frec_origin");
          let info=jQuery.parseHTML(select);
          myselctOrigin.append(info); 

          $.each(dataOrigin.servicios,function(index,json){
              originOBJ={
                address_components:{
                  0:{
                    long_name:json.servicioOrigenText
                  }
                },
                geometry:{
                  location:{
                      lat:json.servicioLatuitudInicio,
                      lng:json.servicioLongitudInicio
                  }
                },
                name:json.servicioOrigenText,
                place_id:json.servicioOrigenPlaceId
              };

              let datatripOrigin='{'+
                '"origen":"'+originOBJ.name+
                '","latOrig":"'+originOBJ.geometry.location.lat+
                '","lngOrig":"'+originOBJ.geometry.location.lng+
                '","originOBJ":'+JSON.stringify(originOBJ)+
              '}';

              optionsOrigin+="<option value='' "+
              "data-trip='"+datatripOrigin+"'id='v_frec_opt"+index+"' >"+
              originOBJ.name+", "+originOBJ.address_components[0].long_name+"</option>";

              let myOriginOpt=$("#v_frec_origin");
              let html1=jQuery.parseHTML(optionsOrigin);
              myOriginOpt.append(html1);

              optionsOrigin="";

          });//...each origenes frecuentes
        },//...success origenes frecuentes
        error: function(xhr, ajaxOptions, throwError){
        alert("Error:"+throwError);
        }
      });//...ajax origenes frecuentes

      $.ajax({//ajax destinos frecuentes
        type: "post",
        url: site_url+"index.php/usuarios/obtenerServiciosFrecuentes/"+"servicioDestinoPlaceId",
        dataType: "json",
        success: function (dataDestin) {
          let select="";
          var optionsDestin="";
          select +="<option id='no' value='-1' data-trip='not_set' selected disabled>&nbsp&nbsp&nbsp</option>";
          let myselctDestin=$("#v_frec_destination");
          let info=jQuery.parseHTML(select);
          myselctDestin.append(info); 

          $.each(dataDestin.servicios,function(index,json){
              destinOBJ={
                address_components:{
                  0:{
                    long_name:json.servicioDestinoText
                  }
                },
                geometry:{
                  location:{
                      lat:json.servicioLatitudFin,
                      lng:json.servicioLongitudFin
                  }
                },
                name:json.servicioDestinoText,
                place_id:json.servicioDestinoPlaceId
              };

              let datatripDestin='{'+
                '"destino":"'+destinOBJ.name+
                '","latOrig":"'+destinOBJ.geometry.location.lat+
                '","lngOrig":"'+destinOBJ.geometry.location.lng+
                '","destinOBJ":'+JSON.stringify(destinOBJ)+
              '}';
              let zadarma = {"widgetId": json.widgetId,
                              "sipId": json.sipId,
                              "domElement": json.domElement};

              optionsDestin+="<option value='"+json.restaurantes_id+"' "+
              "data-trip='"+datatripDestin+"' id='v_frec_opt"+index+"' "+
              "data-restaurante_url='"+json.restaurantes_url+
              "' data-zadarmabtn='"+JSON.stringify(zadarma)+"' "+
              "' data-palabras_clave='"+json.restaurantes_palabras_clave+"' >"+
              destinOBJ.name+"</option>";

              let myDestinOpt=$("#v_frec_destination");
              let html1=jQuery.parseHTML(optionsDestin);
              myDestinOpt.append(html1);

              optionsDestin="";

          });//...each destinos frecuentes
        },//...success destinos frecuentes
        error: function(xhr, ajaxOptions, throwError){
        alert("Error:"+throwError);
        }
      });//...ajax destinos frecuentes

    //actualizar_select_restaurantes();


    var arrowright="<i class='icofont-arrow-right' style='font-size:1em; color: yellow;'></i>";//ui-next
    var info1=jQuery.parseHTML(arrowright);
    $("#confirm_route").append(info1);

    var star="<i class='icofont-ui-next' style='font-size:1em; color: yellow;'></i>";
    var info2=jQuery.parseHTML(star);
    $("#no").append(info2);


    $("#select_restaurantes").change(function() {
        updateLocation("destination","origin","restaurante");
        //unselectV_Frec("destination");
      });

      $("#add_restaurante").click(function() {
        let data_trip=$("#nvo_restaurante").val();
        if(data_trip!="none"){
          modal_nuevo_restaurante(data_trip);
        }
      });

    $("#range").on("change",function(){
      actualizar_select_restaurantes();
    })

});//document.ready
function modal_nuevo_restaurante(data_trip){
  
    customModal(site_url+"index.php/usuarios/modal_add_restaurante", JSON.parse(data_trip), "POST", "md modal-nuevo-restaurante", "", "","", "", "","", "Agregar restaurante", "add_rest_modal");
  
}
function actualizar_select_restaurantes(origin_lat="",origin_lng=""){
    let val_range=$("#range").val();
    if(origin_lat==""){
      let origin_lat=$("#origin").data("lat");
      let origin_lng=$("#origin").data("lng");
    }
    var val_select_rest=$("#select_restaurantes").val();
    let limit_km=(val_range=="" || val_range==undefined)?7:val_range;
    let lat=(origin_lat=="" || origin_lat==undefined)?"19.2388459":origin_lat;
    let lng=(origin_lng=="" || origin_lng==undefined)?"-103.721443":origin_lng;

    ajaxJson(
        site_url+"index.php/usuarios/obtenerRestaurantes/"+lat+"/"+lng+"/"+limit_km,
        {}, 
        "POST", 
        true, 
        function(result){
          //var div_target=$(".restaurantes_container");
          var restaurantes=JSON.parse(result);
          //div_target.append($("<select id='select_restaurantes' class='btnV_FREC'></select>"));
          var select_restaurantes=$("#select_restaurantes");
          select_restaurantes.empty();
            let opt= $("<option></option>")
              .attr("value",-1)
              .attr("data-trip","not_set")
              .attr("disabled","")
              .text("¿Qué quieres comer?");
            select_restaurantes.append(opt);
          $.each(restaurantes,function(index,json){
            destinOBJ={
                address_components:{
                  0:{
                    long_name:this.servicioDestinoCalleNumero
                  }
                },
                geometry:{
                  location:{
                      lat:this.servicioLatitudFin,
                      lng:this.servicioLongitudFin
                  }
                },
                name:this.servicioPlaceName,
                formatted_address:this.servicioDestinoText,
                place_id:this.servicioDestinoPlaceId
              };

              let datatripDestin='{'+
                '"destino":"'+destinOBJ.name+
                '","latOrig":"'+destinOBJ.geometry.location.lat+
                '","lngOrig":"'+destinOBJ.geometry.location.lng+
                '","destinOBJ":'+JSON.stringify(destinOBJ)+
              '}';

            let zadarma = {"widgetId": this.widgetId,
                              "sipId": this.sipId,
                              "domElement": this.domElement};

            let opt= $("<option class='align-last' ></option>")
                .attr("value",this.restaurantes_palabras_clave)
                .attr("data-text",this.restaurantes_nombre)
                .attr("data-trip",datatripDestin)
                .attr("data-restaurante_url",this.restaurantes_url)
                .attr("data-zadarmabtn", JSON.stringify(zadarma))
                .attr("data-palabras_clave", this.restaurantes_palabras_clave)
                .text(this.restaurantes_nombre +" "+ this.distancia_al_origen + "km");
                //.text(this.restaurantes_nombre+" ("+this.restaurantes_palabras_clave+")");
            select_restaurantes.append(opt);
          });
          setTimeout(() => {
            select_restaurantes.select2();
            select_restaurantes.val(val_select_rest).trigger("change")
          }, 500);
        },
        false,
        ""
      );

  
}//...actualizar select restaurantes
  </script>

<script>

</script>
  <div id="zadarmaScripts"></div>
  <script>
     (function() {
      var script = document.createElement('script');
      script.src = 'https://my.zadarma.com/callmewidget/v2.0.8/loader.js';
      document.getElementById('zadarmaScripts').appendChild(script);
	}());
  </script>
  </body>
  
</html>