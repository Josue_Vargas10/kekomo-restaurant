<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>        
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHHWSkUcqyeERY0E2KCxx_Fggbo1BZN5I&libraries=places&callback=initMap"
            async defer></script>

<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>

    var  pointA,pointB;
    function initMap() {
        pointA = new google.maps.LatLng(19.263315, -103.766136),
        pointB = new google.maps.LatLng(19.070466, -104.301156),
        myOptions = {
            zoom: 7,
            center: pointA
        },
        map = new google.maps.Map(document.getElementById('map'), myOptions),
        // Instantiate a directions service.
        directionsService = new google.maps.DirectionsService,
/*         trafficLayer = new google.maps.TrafficLayer(), */
        directionsDisplay = new google.maps.DirectionsRenderer({
            map: map
        }),
        
        /* trafficLayer.setMap(map); */
        markerA = new google.maps.Marker({
            position: pointA,
            title: "point A",
            label: "A",
            map: map
        }),
        markerB = new google.maps.Marker({
            position: pointB,
            title: "point B",
            label: "B",
            map: map
        });

    // get route from A to B
    calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);

}


function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
    directionsService.route({
        origin: pointA,
        destination: pointB,
        travelMode: 'DRIVING',
        avoidTolls: false,
        avoidHighways: false,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            var pointsArray = response.routes[0].overview_path;

                console.log(response.routes);

                //obtener leg markers(segmentar trayecto por coordenadas)
                for (var route in response.routes) {
                    for (var leg in route.legs) {
                        for (var step in leg.steps) {
                            for (var latlng in step.path) {
                                pointsArray.push(latlng)
                                    
   /*                              markers=[
                                        new google.maps.Marker({
                                        position: pointA,
                                        title: "p",
                                        label: "m",
                                        map: map
                                    }), 
                                ]*/
                            }
                        }
                    }
                }
                alert(response.routes[0].legs[0].distance.text);
                alert(response.routes[0].legs[0].duration.text);
                //alert(JSON.stringify(pointsArray,0,4));
                //console.log(JSON.stringify(pointsArray,0,4));
                //alert(pointsArray.length);
                //alert(getDistance(19.25722,-103.68835000000001,19.256690000000003,-103.68798000000001));
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

initMap();
function getDistance(latitud1,longitud1,latitud2,longitud2){
    let kilometros,distancia;
    distancia = (3958*3.1415926*Math.sqrt((latitud2-latitud1)*(latitud2-latitud1) + Math.cos(latitud2/57.29578) * Math.cos(latitud1/57.29578)*(longitud2-longitud1)*(longitud2-longitud1))/180)*1.609344;
        kilometros = distancia;
        return kilometros;
}

    </script>

  </body>

