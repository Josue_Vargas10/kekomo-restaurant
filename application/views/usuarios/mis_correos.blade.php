<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <div class="input-group">
        <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
          </div>
          <?php 
        }
        ?>
      </div>
      <form id="mails-form" class="login100-form" role="form" method="post">
          <!-- section title -->
          <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
              <h2><span class="color">Mis correos</span></h2>
              <div class="border"></div>
          </div>
          <!-- /section title -->

         <div class="alert alert-info alert-dismissible fade" role="alert">
            <strong>¡Registros actualizados correctamente!</strong> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="wrap-input100">
            {{$input_correo1}}
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-edit" aria-hidden="true"></i>
            </span>
            <div class="error error_correo1"></div>
          </div>

          <div class="wrap-input100">
            {{$input_correo2}}
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-edit" aria-hidden="true"></i>
            </span>
            <div class="error error_correo2"></div>
          </div>

          <div class="wrap-input100">
            {{$input_correo3}}
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-edit" aria-hidden="true"></i>
            </span>
            <div class="error error_correo3"></div>
          </div>

          <div class="container-login50-form-btn">
            <input type="button" id="btnAceptar" class="login50-form-btn" value="Actualizar correos"/>
            <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel" value="Cancelar"/>
          </div>
        </form>                   
      </div>  
  </div>
</div>

<script type="text/javascript">
  var site_url = "{{site_url()}}";
  $("#btnAceptar").on('click',guardar);
  $('#btn-cancelar').click(function(e) 
  {
      window.location="<?php echo base_url() ?>index.php/usuarios/index";
  });

  function guardar(){
     var url = site_url+'/usuarios/mis_correos';
      ajaxJson(url,$("#mails-form").serialize(),"POST","async",function(result){
              if(isNaN(result)){
              data = JSON.parse( result );
              //Se recorre el json y se coloca el error en la div correspondiente
              $.each(data, function(i, item) {
                  $(".error_"+i).empty();
                  $(".error_"+i).append(item);
                  $(".error_"+i).css("color","red");
              });
      }else{
        //location.reload();
        $(".alert").addClass('show');
        setTimeout(esconderAlert, 2000);
      }
    });  
  }
  function esconderAlert(){
      $(".alert").removeClass('show');
  }
  

</script>