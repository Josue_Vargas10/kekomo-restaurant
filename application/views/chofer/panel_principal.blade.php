<section >
    <div class="container">

        <div class="col-sm-12  options">
          <div class="row">
            <div class="col-3">

            </div>

            <div class="col-6 text-center option">

            </div>

          </div>
          <div class="row">
            <div class="col-3">

            </div>
            <div class="col-6 text-center option">
                  <button type="button" id="ticket" class="btn btn-success btn-lg"
                  data-toggle="modal" data-target="#exampleModalCenter">Ver servicio</button>
            </div>
          </div>
          <div class="row">
            <div class="col-3">

            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="background-color:#20375a;">
              <div class="modal-header">
                <a  id="phone" href="tel:600123456">
                  <div class="row">
                    <h5 id="main_label" > <i class="fa fa-phone" aria-hidden="true"></i> Confirmar con cliente. </h5>
                  </div>
                </a>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" disabled="disabled">
                  <i class="fa fa-times" aria-hidden="true"></i>
                </button>


              </div>
              <div class="modal-body">
                <div class="col-12">
                  <a id="link-origen" href="" target="_blank">
                    <span class="highlight">
                      <strong>Origen: </strong><i class="fa fa-map-marker" aria-hidden="true"></i>
                    </span>
                  </a>
                  <p class="details" id="origen"></p>
                  <a id="link-destino" href="" target="_blank">
                  <span class="highlight">
                      <strong>Destino:  </strong><i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                  </a>
                  <p class="details" id="destino"></p>
                  <span class="highlight"><strong>Precio: </span>
                  <p class="details"> <span id="precioMin" >  </span> - <span id="precioMax" ></span> <span class="details"> aprox.</span></p></strong>
                  <!-- <span class="highlight">Distancia:</span>
                  <span class="details" id="distancia"></span> <span class="details"> mins</span>
                  <span class="highlight">Duración:</span>
                  <span class="details" id="duracion"></span><span class="details"> mins</span> -->
                  <span class="highlight"><strong>Observaciones:</strong> </span>
                  <div class="observaciones-usuario">
                    <textarea name="textarea" rows="4" cols="100%" id="observaciones" disabled></textarea>
                  </div>
                </div>
              </div>
              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="button" id="start" class="btn btn-primary"></button>
                <button type="button" id="stop" class="btn btn-success" disabled="disabled"><i class="fa fa-money" aria-hidden="true"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

</section>
<script>
$(document).ready(function(){



   let service =  <?=json_encode($_SESSION['servicio'][0])?>;
   let operador =<?=json_encode($_SESSION['operador'][0])?>;
   let status = false;

   $('#start').attr("disabled", true);
   $("#stop").attr("disabled", false);

    $("#ticket").click(()=>{

      console.log( service);
      // getPlace();

      // $("#link-origen").attr("href", "https://www.google.com/maps/dir/Tu+ubicación/"+ service['servicioLatuitudInicio'] +
      //    ',' +  service['servicioLongitudInicio']);
     $("#link-origen").attr("href", getURL(service['servicioOrigenPlaceId'], service['servicioOrigenCalleNumero']));
     $("#link-destino").attr("href", getURL(service['servicioDestinoPlaceId'], service['servicioDestinoCalleNumero']));
      //CAmbiar estado del servico....
      var coordinates;

      // console.log(service['servicioOrigenCalleNumero']);
      document.getElementById("origen").innerHTML  = service['servicioOrigenText'];
      document.getElementById("destino").innerHTML  = service['servicioDestinoCalleNumero'];
      document.getElementById("precioMin").innerHTML  = service['precio_min_aprox'];
      document.getElementById("precioMax").innerHTML  = service['precio_max_aprox'];
      document.getElementById("phone").attr("href", "tel:" + service['celularSolicitante']) ;

      // console.log(typeof(service['servicioComentario']));
      if(service['observacionesSolicitante'] == "")
      {
        document.getElementById("observaciones").innerHTML  = "Sin observaciones.";
      }
      else {
        document.getElementById("observaciones").innerHTML  =service['observacionesSolicitante'];
      }

      // console.log(service);
      if(status != true){
        aceptarServicio(service['servicioId'], operador['OPId']);

      }
      status = true;

      // console.log("ready");
    });

    $("#stop").click(()=> {

      $(".close").attr("disabled", false);
      clear_modal();
      mostrar_precio();
      finalizarServicio(service["servicioId"]);

    });
    function clear_modal(){
     $(".modal-header").empty();
     $(".modal-body").empty();
   }

    function mostrar_precio(){
      $(".modal-header").append("<h5 style='width: 100%'>Servicio finalizado</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><i class='fa fa-times' aria-hidden='true'></i></button>");
      $("h5:first-of-type").attr("id", "phone");
      $("h5:first-of-type").attr("style", "color:white;font-size:1.25em;padding-left:1em;");
      $(".modal-body").append(" <h6 class='details' style='padding-left: 1em;'>Precio final:</h6>");
      let precio_final = (parseInt(service["precio_max_aprox"]) + parseInt(service["precio_min_aprox"]) ) / 2;
      console.log(parseInt(service["precio_max_aprox"]) + parseInt(service["precio_min_aprox"]));
      console.log(service["precio_min_aprox"]);
      $(".modal-body").append(" <h1 class='highlight price'>$"+precio_final.toFixed(2)+"</h1>");
      // $(".modal-body").append("<div class='observaciones-usuario'><textarea name='textarea' rows='5' cols='100%' id='observaciones' disabled></textarea></div>");
      // $("#observaciones").attr("style", "display :hide;");
    }
    async function finalizarServicio(idService) {
      // alert("GOLA");
      //FALTA VALIDAR QUE NO SE HAYA FINALIZADO EL SERVICIO antes
      // POR LO QUE SERA NECESARIO CAMBIAR EL STATUS DEL SERVICIO UNA VEZ FINALIZADO....
      const params = new URLSearchParams();
      params.append('idServicio', idService);
      const response = await axios.post('<?php echo base_url() ?>usuarios/finalizar_servicio', params)
         .then(function (response) {
           console.log(response);
         })
        .catch(function (error) {
          console.log(error);
        });
    }
    function getURL(place_id, dir_place) {
      url = 'https://www.google.com/maps/dir/?api=1';
      origin =  '&origin=' + operador['OPLatitud'] + ',' + operador['OPLongitud'];
      destination = '&destination=' + getPlace(dir_place);
      place_id = '&destination_place_id=' + place_id;

      url = url + origin + destination + place_id + '&dir_action=navigate';

      return url;
    }

    function getPlace(place) {
      // console.log(place);
      parts = place.split(',');
      // console.log(parts);
      res = '';
      // console.log(parts.length);
      parts.forEach((item, i) => {
          part = item.split(' ');
          // console.log( part.length);
          part.forEach((item, i) => {
            item = item.replace('#','');
            res += item;
            // console.log( i);

            if(i < part.length-1){
              res += '+';
            }
          });
          if( i < parts.length-1)
            res += '%2C';
            // console.log(res);
      });



      // slug = slug.replace(' ', '+');
      return res;

     // return coordinates;
    }

  });
</script>
