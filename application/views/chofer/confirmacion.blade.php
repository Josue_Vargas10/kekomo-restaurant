<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <h1 id="leyenda" class="d-flex justify-content-center">Tienes un servicio nuevo...</h1>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-0 col-md-1 col-lg-3">

        </div>
        <div id="loader" class="col-sm-12 col-md-10 col-lg-6">
            <div class="ldBar label-center" id="myBar" data-preset="circle"
              style="width:100%;height:100%" data-value="0" data-min="0" data-max="30"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-6 text-center">
          <button type="button" id="cancelar"  class="btn btn-danger btn-lg " data-dismiss="modal">Rechazar</button>
        </div>
        <div class="col-6 text-center">
              <button type="button" id="aceptar" class="btn btn-primary btn-lg"  data-toggle="modal" data-target="#exampleModalCenter">Aceptar</button>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content" style="background-color:#20375a;">
            <div class="modal-header">
              <a  id="phone" href="tel:600123456">
                <div class="row">
                  <h5 id="main_label" > <i class="fa fa-phone" aria-hidden="true"></i> Confirmar con cliente. </h5>
                </div>
              </a>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" disabled="disabled">
                <i class="fa fa-times" aria-hidden="true"></i>
              </button>


            </div>
            <div class="modal-body">
              <div class="col-12">
                <a id="link-origen" href="" target="_blank">
                  <span class="highlight">
                    <strong>Origen: </strong><i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                </a>
                <p class="details" id="origen"></p>
                <a id="link-destino" href="" target="_blank">
                <span class="highlight">
                    <strong>Destino:  </strong><i class="fa fa-map-marker" aria-hidden="true"></i>
                </span>
                </a>
                <p class="details" id="destino"></p>
                <span class="highlight"><strong>Precio: </span>
                <p class="details"> <span id="precioMin" >  </span> - <span id="precioMax" ></span> <span class="details"> aprox.</span></p></strong>
                <!-- <span class="highlight">Distancia:</span>
                <span class="details" id="distancia"></span> <span class="details"> mins</span>
                <span class="highlight">Duración:</span>
                <span class="details" id="duracion"></span><span class="details"> mins</span> -->
                <span class="highlight"><strong>Observaciones:</strong> </span>
                <div class="observaciones-usuario">
                  <textarea name="textarea" rows="4" cols="100%" id="observaciones" disabled></textarea>
                </div>
              </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" id="start" class="btn btn-primary"></button>
              <button type="button" id="stop" class="btn btn-success" disabled="disabled"><i class="fa fa-money" aria-hidden="true"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

<script>
   $(document).ready(function(){
      let service =  <?=json_encode($_SESSION['servicio'][0])?>;
      // console.log(service);
      // console.log(service['observacionesSolicitante']);
      let operador =<?=json_encode($_SESSION['operador'][0])?>;
      // console.log(operador);
      let status = false;
      let end = false;

      if(service == false || operador == false){
        $('#aceptar').attr("disabled", true);
        $('#cancelar').attr("disabled", true);
        status = true;
      }

      if(service['servicioStatus'] == 2){
        status = true;
      }

      // if(service['servicioStatus'] == )
      // console.log(service);
      // console.log(operador['OPId']);

      $("#start").click(()=> {
        // alert("Viaje iniciado exitosamente");
        iniciarServicio(service['servicioId']);
          $('#start').attr("disabled", true);
          $("#stop").attr("disabled", false);
      });
      $("#stop").click(()=> {
        if(end != true){
          $(".close").attr("disabled", false);
          clear_modal();
          mostrar_precio();
          finalizarServicio(service["servicioId"]);
          end = true;
        }


      });
      $("#cancelar").click(()=>{
         rechazarServicio(service['servicioId']);
      });
      $("#aceptar").click(()=>{
       // console.log();
       // getPlace();

       // $("#link-origen").attr("href", "https://www.google.com/maps/dir/Tu+ubicación/"+ service['servicioLatuitudInicio'] +
       //    ',' +  service['servicioLongitudInicio']);
       if(end != true){


          $("#link-origen").attr("href", getURL(service['servicioOrigenPlaceId'], service['servicioOrigenCalleNumero']));
          $("#link-destino").attr("href", getURL(service['servicioDestinoPlaceId'], service['servicioDestinoCalleNumero']));
           //CAmbiar estado del servico....
           var coordinates;

           // console.log(service['servicioOrigenCalleNumero']);
           document.getElementById("origen").innerHTML  = service['servicioOrigenText'];
           document.getElementById("destino").innerHTML  = service['servicioDestinoCalleNumero'];
           document.getElementById("precioMin").innerHTML  = service['precio_min_aprox'];
           document.getElementById("precioMax").innerHTML  = service['precio_max_aprox'];
           document.getElementById("phone").attr("href", "tel:" + service['celularSolicitante']) ;

           // console.log(typeof(service['servicioComentario']));
           if(service['observacionesSolicitante'] == "")
           {
             document.getElementById("observaciones").innerHTML  = "Sin observaciones.";
           }
           else {
             document.getElementById("observaciones").innerHTML  =service['observacionesSolicitante'];
           }

           // console.log(service);
           if(status != true){
             aceptarServicio(service['servicioId'], operador['OPId']);

           }
           status = true;
         }

         // console.log("ready");
       });
       $(document).on('click', '.close', function(){
         // alert("HOLA");
         open(window.location.href, '_self').close();

       });

     function mostrar_precio(){
       $(".modal-header").append("<h5 style='width: 100%'>Servicio finalizado</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><i class='fa fa-times' aria-hidden='true'></i></button>");
       $("h5:first-of-type").attr("id", "phone");
       $("h5:first-of-type").attr("style", "color:white;font-size:1.25em;padding-left:1em;");
       $(".modal-body").append(" <h6 class='details' style='padding-left: 1em;'>Precio final:</h6>");
       let precio_final = (parseInt(service["precio_max_aprox"]) + parseInt(service["precio_min_aprox"]) ) / 2;
       console.log(parseInt(service["precio_max_aprox"]) + parseInt(service["precio_min_aprox"]));
       console.log(service["precio_min_aprox"]);
       $(".modal-body").append(" <h1 class='highlight price'>$"+Math.round(precio_final).toFixed(2)+"</h1>");
       // $(".modal-body").append("<div class='observaciones-usuario'><textarea name='textarea' rows='5' cols='100%' id='observaciones' disabled></textarea></div>");
       // $("#observaciones").attr("style", "display :hide;");
     }
     function clear_modal(){
       $(".modal-header").empty();
       $(".modal-body").empty();
      }


     async function rechazarServicio(idService) {
        const params = new URLSearchParams();
        params.append('idServicio', idService);
           const response = await axios.post('<?php echo site_url(); ?>/usuarios/rechazar_servicio', params)
           .then(function (response) {
             console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
      }
     async function finalizarServicio(idService) {
       // alert("GOLA");
       const params = new URLSearchParams();
       params.append('idServicio', idService);
       const response = await axios.post('<?php echo site_url(); ?>/usuarios/finalizar_servicio', params)
          .then(function (response) {
            console.log(response);
          })
         .catch(function (error) {
           console.log(error);
         });
     }
     async function aceptarServicio(idService, idOperador) {
        const params = new URLSearchParams();
        params.append('idServicio', idService);
        params.append('idOperador', idOperador);
           const response = await axios.post('<?php echo site_url(); ?>/usuarios/aceptar_servicio', params)
           .then(function (response) {
             console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
      }

      async function iniciarServicio(idService){
            const params = new URLSearchParams();
            if ("geolocation" in navigator) {
              navigator.geolocation.getCurrentPosition(function(position) {
                // params.append('OPLat', position.coords.latitude);
                // params.append('OPLong', position.coords.longitude);
               //  const response = await axios.post('<?php echo base_url(); ?>usuarios/aceptar_servicio', params)
               //  .then(function (response) {
               //    console.log(response);
               // })
               // .catch(function (error) {
               //   console.log(error);
               // });
               console.log(position.coords.latitude);
               console.log(position.coords.longitude);
              });
            }else{
              alert("Error al capturartus coordenadas");
            }
            params.append("idServicio", service['servicioId']);
            console.log("<?php echo site_url(); ?>");
            const response = await axios.post('<?php echo site_url(); ?>/usuarios/iniciar_servicio', params)
            .then(function (response) {
              console.log(response);
            })
           .catch(function (error) {
             console.log(error);
           });
      }

     var n =0;
     updateLoader();

     function updateLoader(){
       let bar1 = new ldBar('#myBar');
       let bar2 = document.getElementById('myBar').ldBar;
       if(n < 30){

         setTimer();
         bar1.set(n)
         setTimeout(updateLoader,1000);
       }
       else {
         console.log(status);
         if(status != true)
          rechazarServicio(service['servicioId']);
       }

     }
     function setTimer(){
         n = n +1;
       setTimeout(()=> {}, 1000);
     }

     function getURL(place_id, dir_place) {
       url = 'https://www.google.com/maps/dir/?api=1';
       origin =  '&origin=' + operador['OPLatitud'] + ',' + operador['OPLongitud'];
       destination = '&destination=' + getPlace(dir_place);
       place_id = '&destination_place_id=' + place_id;

       url = url + origin + destination + place_id + '&dir_action=navigate';

       return url;
     }

     function getPlace(place) {
      // console.log(place);
      parts = place.split(',');
      // console.log(parts);
      res = '';
      // console.log(parts.length);
      parts.forEach((item, i) => {
          part = item.split(' ');
          // console.log( part.length);
          part.forEach((item, i) => {
            item = item.replace('#','');
            res += item;
            // console.log( i);

            if(i < part.length-1){
              res += '+';
            }
          });
          if( i < parts.length-1)
            res += '%2C';
            // console.log(res);
      });



      // slug = slug.replace(' ', '+');
      return res;

     // return coordinates;
    }
  });
</script>
